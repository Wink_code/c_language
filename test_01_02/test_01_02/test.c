#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	int arr[10] = { 5,3,9,1,4,2,8,6,7,10 };
	for (int i = 1; i < 10; i++)
	{
		int tmp = arr[i];//存储当前待排序元素
		int j = i - 1;//和i下标前面的元素进行比较
		while(j >= 0 && tmp > arr[j])
		{
			arr[j + 1] = arr[j];
			j--;
		}
		//退出循环前j--了，因此为j+1
		arr[j + 1] = tmp;
	}
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}

//n阶魔方阵，n为3-100之间的奇数
//int main()
//{
//	int arr[50][50] = { 0 };
//	int n = 0;
//	int row = 0;
//	int col = 0;
//	int prevRow = 0;
//	int prevCol = 0;
//	while (1)
//	{
//		printf("请输入魔方阵的阶数，阶数属于3-100之间的奇数：\n");
//		scanf("%d", &n);
//		//判断输入的是否符合要求
//		if ((n >= 3 && n <= 100) && (0 != n % 2))
//		{
//			break;
//		}
//	}
//	col = n / 2;
//	arr[row][col] = 1;
//	//生成2-n*n的数
//	for (int i = 2; i <= n * n; i++)
//	{
//		row--;//上一行
//		col++;//下一列
//		//将数组看成回绕的
//		if (row < 0)
//		{
//			row = n - 1;
//		}
//		if (col >= n)
//		{
//			col = 0;
//		}
//
//		if (0 != arr[row][col])
//		{
//			//如果上一行、下一列已经有元素了
//			//那下一个元素存放在 当前列  的下一行
//			row = prevRow + 1;
//			col = prevCol;
//		}
//		//下一个元素存放在当前元素的上一行、下一列
//		arr[row][col] = i;
//		//记录当前行和列
//		prevRow = row;
//		prevCol = col;
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			printf("%5d", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}