﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define INT_VAL(a,b) do{ a=0;b=0; }while(0)
int main()
{
	int x = 10;
	int y = 20;
	if (1)
		INT_VAL(x, y);
	else
		x = 100, y = 200;
	printf("%d %d\n", x, y);
	return 0;
}

//#define MAX(a, b) ((a) > (b) ? (a) : (b))
//int main()
//{
//	int x = 5;
//	int y = 8;
//	int z = MAX(x++, y++);
//	printf("x=%d y=%d z=%d\n", x, y, z);
//	return 0;
//}

//#define SQURE1(x) x*x
//#define SQURE2(x) (x)*(x)
//#define DOUBLE1(x) (x)+(x)
//#define DOUBLE2(x) ((x)+(x))
//
//int main()
//{
//	int a1 = SQURE1(6); //36
//	int a2 = SQURE1(6+1); //49 ??
//	printf("%d\n", a1);
//	printf("%d\n", a2);
//	printf("\n");
//
//	int b1 = SQURE2(6); //36
//	int b2 = SQURE2(6 + 1); //49
//	printf("%d\n", b1);
//	printf("%d\n", b2);
//	printf("\n");
//
//	int c1 = 5 * DOUBLE1(2 + 1);// 30??
//	int c2 = 5 * DOUBLE2(2+1);// 30
//	printf("%d\n", c1);
//	printf("%d\n", c2);
//	return 0;
//}

//#define MAX 100;
//
//int main()
//{
//	int max = 0;
//	if (1)
//		max = MAX;
//	else
//		max = 0;
//	return 0;
//}

//#define CASE break;case //在写case语句的时候⾃动把 break写上。
//int main()
//{
//	int input = 2;
//	switch (input)
//	{
//		case 1:
//			printf("1\n");
//		CASE 2 :
//			printf("2\n");
//		CASE 3 :
//			printf("3\n");
//		CASE 4 :
//			printf("4\n");
//		CASE 5 :
//			printf("5\n");
//	}
//	return 0;
//}

//#define M 1000
//#define reg register  //为 register这个关键字，创建⼀个简短的名字
//int main()
//{
//	int a = M;
//	reg int b = 0;
//}


// 如果定义的 stuff过⻓，可以分成⼏⾏写，除了最后⼀⾏外，每⾏的后⾯都加⼀个反斜杠(续⾏符)。
//int main()
//{
//	printf("data:%s\n", __DATE__);
//	printf("time:%s\n", __TIME__);
//	printf("line:%d\n",__LINE__);
//	return 0;
//}

//#define BSC //
//int main()
//{
//	BSC printf("hello world\n");
//	return 0;
//}

//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	int sum = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			/*if (i == j)
//			{
//				sum += arr[i][j];
//			}*/
//			if (i + j == 2)
//			{
//				sum += arr[i][j];
//			}
//		}
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//int main()
//{
//	int arr[101] = { 0 ,0};
//	for (int i = 2; i <= 100; i++)
//	{
//		arr[i] = i;
//	}
//	for (int i = 2; i < 100; i++)
//	{
//		for (int j = i + 1; j <= 100; j++)
//		{
//			if (arr[j] % i == 0)
//			{
//				arr[j] = 0;
//			}
//		}
//	}
//	int count = 0;
//	for (int i = 0; i < 101; i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d ", arr[i]);
//			count++;
//		}
//		if (count  == 10)
//		{
//			printf("\n");
//			count = 0;
//		}
//	}
//	return 0;
//}

//#include<string.h>
//int main()
//{
//	char arr[3][20] = { 0 };
//	char str[20] = { 0 };
//	for (int i = 0; i < 3; i++)
//	{
//		gets(arr[i]);
//	}
//	if (strcmp(arr[0], arr[1]) > 0)
//		strcpy(str, arr[0]);
//	else
//		strcpy(str, arr[1]);
//	if (strcmp(str, arr[2]) < 0)
//		strcpy(str, arr[2]);
//	printf("%s\n", str);
//	return 0;
//}

//int main()
//{
//	//12a 333 aaa
//	char arr[100] = { 0 };
//	gets(arr);
//	int count = 0;
//	int i = 0;
//	int flag = 0;
//	while (arr[i] != '\0')
//	{
//		if (arr[i] == ' ')
//		{
//			flag = 0;
//		}
//		else if(flag == 0 &&\
//			((arr[i] >= 'a' && arr[i] <= 'z')\
//			|| arr[i] >= 'A' && arr[i] <='Z'))  //不是空格，并且单词未被计算过
//		{
//			flag = 1;//标记该单词已经计算过了
//			count++;
//		}
//		i++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int arr[3][3] = { 2,12,54,33,65,7,98,78,31 };
//	int max = arr[0][0];
//	int row = 0;
//	int col = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			if (max < arr[i][j])
//			{
//				max = arr[i][j];
//				row = i; 
//				col = j;
//			}
//		}
//	}
//	printf("max = %d,row = %d, col = %d\n", max, row, col);
//	return 0;
//}

//int main()
//{
//	int arr[2][3] = { 1,1,1,2,2,2 };
//	int arr2[3][2] = { 0 };
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			arr2[j][i] = arr[i][j];
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			printf("%d ", arr2[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}