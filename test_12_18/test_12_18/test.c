#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//更相减损
/*大数减小数，
将差与小数比较，
直到两数相等，就找到了*/
int func2(int n, int m)
{
	if (n > m)
	{
		return func2(m, n - m);
	}
	else if (m > n)
	{
		return func2(n, m - n);
	}
	else
		return n;
}

//递归
int func(int n, int m)
{
	if (m == 0)
	{
		return n;
	}
	else
	{
		return func(m, n % m);
	}
}
int main()
{
	int n, m;
	scanf("%d %d", &n, &m);
	int mul = n * m;
	//int ret = func(n, m);
	int ret = func2(n, m);
	printf("最大公约数：%d\n", ret);
	printf("最小公倍数：%d\n", mul / ret);
}

//void swap(int* n, int* m)
//{
//	int tmp = *n;
//	*n = *m;
//	*m = tmp;
//}
//int main()
//{
//	int a, b, c;
//	scanf("%d %d %d", &a, &b, &c);
//	if (a < b)
//	{
//		swap(&a, &b);
//	}
//	if (a < c)
//	{
//		swap(&a, &c);
//	}
//	if (b < c)
//	{
//		swap(&b, &c);
//	}
//	printf("%d %d %d\n", a, b, c);
//	return 0;
//}

//#include<math.h>
//int main()
//{
//	double a, b, c;
//	scanf("%lf %lf %lf", &a, &b, &c);
//	double flag = b * b - 4 * a * c;
//	double ret1, ret2;
//	if (flag > 0)
//	{
//		ret1 = (-b + sqrt(flag)) / (2 * a);
//		ret2 = (-b - sqrt(flag)) / (2 * a);
//		printf("ret1= %lf, ret2 = %lf\n", ret1, ret2);
//	}
//	else if (flag == 0)
//	{
//		ret1 = -b / (2 * a);
//		ret2 = -b / (2 * a);
//		printf("ret1=ret2 = %lf\n", ret1);
//	}
//	else
//	{
//		printf("方程无解\n");
//	}
//	return 0;
//}

//暴力求解法
//int main()
//{
//	int n, m;
//	scanf("%d %d", &n, &m);
//	int mul = n * m;
//	int min = n < m ? n : m;
//
//	while (min)
//	{
//		if (n % min == 0 && m % min == 0)
//		{
//			printf("最大公约数：%d\n", min);
//			break;
//		}
//		min--;
//	}
//	printf("最小公倍数：%d\n", mul / min);
//	return 0;
//}

//辗转相除法
//int main()
//{
//	int n, m;
//	scanf("%d %d", &n, &m);
//	int mul = n * m;
//	int tmp = 0;
//	while (tmp = n % m)
//	{
//		n = m;
//		m = tmp;
//	}
//	printf("最大公约数：%d\n", m);
//	printf("最小公倍数：%d\n", mul / m);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	if (n % 3 == 0 && n % 5 == 0)
//	{
//		printf("yes\n");
//	}
//	else
//	{
//		printf("no\n");
//	}
//	return 0;
//}

//int Max(int a, int b, int c)
//{
//	int max = 0;
//	max = a > b ? a : b;
//	max = max > c ? max : c;
//	return max;
//}
//
//int main()
//{
//	int a, b, c;
//	scanf("%d %d %d", &a, &b, &c);
//	int max = Max(a, b, c);
//	/*int max = a;
//	if (a < b)
//	{
//		max = b;
//	}
//	if (max < c)
//	{
//		max = c;
//	}*/
//	printf("%d\n", max);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0; 
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int max = arr[0];
//	for (i = 0; i < 10; i++)
//	{
//		if (max < arr[i])
//		{
//			max = arr[i];
//		}
//	}
//	printf("%d\n", max);
//	return 0;
//}

//int main()
//{
//    int i = 0;
//    for (i = 10000; i <= 99999; i++)
//    {
//        int j = 0;
//        int sum = 0;
//        int tmp = i;
//        for (j = 10; j <= 10000; j *= 10)
//        {
//            sum += (tmp % j) * (tmp / j);
//        }
//        if (sum == i)
//        {
//            printf("%d ", i);
//        }
//    }
//    return 0;
//}