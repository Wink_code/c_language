#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int fun(int* a, int n)
//{
//	if (n >= 1)
//	{
//		return fun(a, n - 1) + a[n - 1];
//	}
//	else
//		return 0;
//}
//
//int main()
//{
//	int a[5] = { 1,2,3,4,5 };
//	int s = fun(a, 5);
//	printf("%d", s);
//	return 0;
//}

void copy(char a[], char b[])
{
	int i = 0, j = 0;
	while (a[i] != '\0')
	{
		if (a[i] == 'a' || a[i] == 'e' || a[i] == 'i' || a[i] == 'o' || a[i] == 'u'
			|| a[i] == 'A' || a[i] == 'E' || a[i] == 'I' || a[i] == 'O' || a[i] == 'U')
			b[j++] = a[i];
		i++;
	}
	b[j] = 0;
}

//int main()
//{
//	char str[20] = { 0 };
//	char dest[20] = { 0 };
//	gets(str);
//	copy(str, dest);
//	printf("%s\n", dest);
//	return 0;
//}

void fun(int a[3][3])
{
	int i, j;
	for (i = 0; i < 3; i++)
	{
		for (j = i; j < 3; j++)
		{
			int tmp = a[i][j];
			a[i][j] = a[j][i];
			a[j][i] = tmp;
		}
	}
}

int isPrime(int n)
{
	for (int i = 2; i < n; i++)
	{
		if (n % i == 0)
			return 0;
	}
	return 1;
}

int main2()
{
	//int a[3][3] = { 1,1,1,2,2,2,3,3,3 };
	////fun(a);
	//for (int i = 0; i < 3; i++)
	//{
	//	for (int j = 0; j < 3; j++)
	//	{
	//		printf("%d ", a[i][j]);
	//	}
	//	printf("\n");
	//}
	int n = 0;
	scanf("%d", &n);
	if (isPrime(n))
	{
		printf("%d������\n",n);
	}
	else
	{
		printf("%d����\n",n);
	}
	return 0;
}

void Average(float a[][4], float b[])
{
	int i, j;
	float sum;
	for (i = 0; i < 5; i++)
	{
		sum = 0;
		for (j = 0; j < 4; j++)
		{
			sum += a[i][j];
		}
		b[i] = sum / 4;
	}
}

void Sort(float a[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (a[j] < a[j + 1])
			{
				float tmp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = tmp;
			}
		}
	}
}

int main()
{
	float score[5][4], ave[5];
	//for (int i = 0; i < 5; i++)
	//{
	//	for (int j = 0; j < 4; j++)
	//	{
	//		scanf("%f", &score[i][j]);
	//	}
	//}

	Average(score, ave);
	for (int i = 0; i < 5; i++)
	{
		printf("%f ", ave[i]);
	}
	printf("\n");
	Sort(ave, 5);
	for (int i = 0; i < 5; i++)
	{
		printf("%f ", ave[i]);
	}
	return 0;
}