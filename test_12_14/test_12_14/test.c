#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>

int main()
{
	int n;
	FILE* pFile;
	char buffer[27];

	pFile = fopen("myfile.txt", "w+");
	//向文件中写。文件指针后移
	for (n = 'A'; n <= 'Z'; n++)
		fputc(n, pFile);
	//使文件指针回到起始位置
	rewind(pFile);
	//从文件指针位置开始读取
	fread(buffer, 1, 26, pFile);
	fclose(pFile);
	buffer[26] = '\0';
	printf(buffer);
	return 0;
}

//int main()
//{
//	FILE* pFile;
//	char str[20] = { 0 };
//	pFile = fopen("example.txt", "wb");
//	fputs("01234567890123456789", pFile);
//	fseek(pFile, 6, SEEK_SET);
//	int ret = ftell(pFile);
//	printf("%d\n", ret);
//	fclose(pFile);
//	return 0;
//}

//struct s
//{
//	char name[20];
//	int age;
//	float score;
//};
//void write()
//{
//	struct s s1 = { "zhangsan", 20, 66.0f };
//	FILE* pf = fopen("text.txt", "w");
//	int ret = fprintf(pf, "%s %d %f", s1.name, s1.age, s1.score);
//	printf("%d\n", ret);//写入字符总数
//	fclose(pf);
//	pf = NULL;
//}
//void read()
//{
//	struct s s2 = { 0 };
//	FILE* pf = fopen("text.txt", "r");
//	int ret = fscanf(pf, "%s %d %f", s2.name, &s2.age, &s2.score);
//	printf("%d\n", ret);//读取了参数列表中的几项
//	//fprintf(stdout, "%s %d %f", s2.name, s2.age, s2.score);
//	fclose(pf);
//	pf = NULL;
//}
//int main()
//{
//	write();
//	read();
//	return 0;
//}
//int main()
//{
//	char str[50] = "22222222";
//	FILE* fp = fopen("source.txt", "a");
//	if (fp == NULL)
//	{
//		return 1;
//	}
//	int ret = fputs(str, fp);
//	printf("%d\n", ret);
//	fclose(fp);
//	fp = NULL;
//
//	return 0;
//}
//int main()
//{
//	FILE* read = fopen("source.txt", "r");
//	FILE* write = fopen("dest.txt", "w");
//	if (read == NULL)
//	{
//		printf("read cannot open this file\n");
//		return 1;
//	}
//	if (write == NULL)
//	{
//		//若写时打开文件失败，那么因该把读的文件也关闭掉
//		fclose(read);
//		read = NULL;
//		printf("write cannot open this file\n");
//		return 1;
//	}
//	char ch = 0;
//	while ((ch = fgetc(read)) != EOF)
//	{
//		fputc(ch, write);
//	}
//	fclose(read);
//	read = NULL;
//	fclose(write);
//	write = NULL;
//	return 0;
//}

//int main()
//{
//	char ch = 'a';
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		printf("cannoy open this file\n");
//		return 1;
//	}
//	char ret = fgetc( pf);
//	printf("%c\n", ret);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//双向冒泡排序
//void d_bubble_sort(int arr[], int num)
//{
//	int left = 0;
//	int right = num - 1;
//	//优化，如果数组已经有序，就跳出循环
//	int flag = 0;
//	while (left < right)
//	{
//		flag = 1;
//		//从左向右找最大
//		for (int i = left; i < right; i++)
//		{
//			//前>后，交换
//			if (arr[i] > arr[i + 1])
//			{
//				flag = 0;
//				int tmp = arr[i];
//				arr[i] = arr[i + 1];
//				arr[i + 1] = tmp;
//			}
//		}
//		//优化，如果遍历了一遍数组，没有发生交换，那就说明数组已经有序了
//		if (flag == 1)
//		{
//			break;
//		}
//		right--;  //最大值已放在右侧
//		//从右向左找最小
//		for (int j = right; j > left; j--)
//		{
//			//前>后，交换
//			if (arr[j] < arr[j - 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j - 1];
//				arr[j - 1] = tmp;
//			}
//		}
//		left++;//最小值放在左侧
//	}
//}
//int main()
//{
//	int arr[10] = { 5,3,7,9,1,2,4,8,6,10 };
//	int arr2[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	d_bubble_sort(arr2,10);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}