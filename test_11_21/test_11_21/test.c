#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//左旋字符串
 
//1. 循环
void left_around1(char* s, int k)
{
	while (k--)
	{
		char tmp = *s; //存储第一个字符
		char* cur = s; //s不能动
		while (*(cur + 1) != '\0')
		{
			*cur = *(cur + 1);
			cur++;
		}
		*cur = tmp;
	}
}

void left_around2(char *str, int time)
{
	int len = strlen(str);
	//例如共5个字符，现旋转11个字符，就相当于旋转1个字符
	time = time % len;
	int i = 0;
	//旋转几个字符
	for (i = 0; i < time; i++)
	{
		int tmp = str[0];
		//后面的往前移
		int j = 0;
		for (j = 0; j <len - 1 ; j++)
		{
			str[j] = str[j + 1];
		}
		str[j] = tmp;
	}
}

//2. 三次逆置
void reverse(char *arr, int start, int end)
{
	while (start < end)
	{
		char tmp = 0;
		tmp = arr[start];
		arr[start] = arr[end];
		arr[end] = tmp;
		start++;
		end--;
	}
}
int main()
{
	char s[] = "ABCDEFG";
	int len = strlen(s) - 1;
	int k = 0;
	scanf("%d", &k);
	//left_around1(s, k);
	//left_around2(s, k);
	reverse(s, 0, k - 1);
	reverse(s, k, len);
	reverse(s, 0, len);
	puts(s);
	return 0;
}

//int main()
//{
//	//二维数组也是数组，之前对数组名理解也是适合
//	int a[3][4] = { 0 };
//	printf("%zd\n", sizeof(a));//12*4 = 48个字节，数组名单独放在sizeof内部
//	printf("%zd\n", sizeof(a[0][0]));//4
//	printf("%zd\n", sizeof(a[0]));//a[0]是第一行这个一维数组的数组名，数组名单独放在sizeof内部了
//	//计算的是第一行的大小，单位是字节，16个字节
//
//	printf("%zd\n", sizeof(a[0] + 1));//a[0]第一行这个一维数组的数组名,这里表示数组首元素
//	//也就是a[0][0]的地址，a[0] + 1是a[0][1]的地址 4/8
//
//	printf("%zd\n", sizeof(*(a[0] + 1)));//a[0][1] - 4个字节
//	printf("%zd\n", sizeof(a + 1));//a是二维数组的数组名，但是没有&，也没有单独放在sizeof内部
//	//所以这里的a是数组收元素的地址，应该是第一行的地址，a+1是第二行的地址
//	//大小也是4/8 个字节
//	printf("%zd\n", sizeof(*(a + 1)));//*(a + 1) ==> a[1] - 第二行的数组名，单独放在sizeof内部，计算的是第二行的大小
//	//16个字节
//	printf("%zd\n", sizeof(&a[0] + 1));//&a[0]是第一行的地址，&a[0]+1就是第二行的地址，4/8
//	printf("%zd\n", sizeof(*(&a[0] + 1)));//访问的是第二行，计算的是第二行的大小，16个字节
//	//int(*p)[4] = &a[0] + 1;
//	//
//	printf("%zd\n", sizeof(*a));//这里的a是第一行的地址，*a就是第一行，sizeof(*a)计算的是第一行的大小-16
//	//*a --> *(a+0) --> a[0]
//	printf("%zd\n", sizeof(a[3]));//这里不存在越界
//	//因为sizeof内部的表达式不会真实计算的
//	//计算的是第四行的大小-16
//
//	return 0;
//}
