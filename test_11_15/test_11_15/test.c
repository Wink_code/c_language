#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
int judge_alpha(char ch)
{
	if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int TheLongestString(char string[], int len)
{
	int i = 0;
	int length = 0;
	int end_len = 0;
	int flag = 1;//假设该位置是一个单词的开始
	int start_point = 0;
	int place = 0;
	for (i = 0; i <= len; i++)
	{
		//是字母，计数
		if (judge_alpha(string[i]))
		{
			//是一个单词的开始，记录开始位置
			if (flag)
			{
				start_point = i;
				flag = 0;
			}
			//记录完位置，记录长度
			//不是一个单词的开始，那就说明属于这个单词，计算该单词的长度
			length++;
		}
		//不是字符:单词结束
		else
		{
			flag = 1;
			if (length > end_len)
			{
				end_len = length;
				length = 0; //置为0，继续记录下一个单词的长度
			}
		}
	}
	return start_point;
}

int main()
{
	char str[20] = { 0 };
	gets(str);
	int len = strlen(str);
	int ret = TheLongestString(str, len);
	int i = 0;
	//此处也可以定义全局变量接最长字符串的长度，就不需要调用judeg_alpha函数了
	for (i = ret; judge_alpha(str[i]); i++)
	{
		printf("%c", str[i]);
	}

	return 0;
}

//void func(char str[])
//{
//	//别忘记\0
//	//0123  4
//	//3689  \0
//	
//	//01234567 8
//	//3 6 8 9 \0 
//	int i = 0;
//	for (i = strlen(str); i > 0; i--)
//	{
//		str[2 * i] = str[i];
//		str[2 * i - 1] = ' ';
//	}
//	printf("%s", str);
//}
//
//int main()
//{
//	char str[10];
//	scanf("%s", &str);
//	func(str);
//	return 0;
//}