#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
int count;
void Move(int n, char a, char b, char c)
{
    /*
        给定n个盘子,移动过程分五步
        一:把n-1个盘子从a移到c,
        二:把最大的盘子从a移到b
        三:把n-1个盘子从c移回a
        四:把最大的盘子从b移到c
        五:把n-1个盘子从a移到c
        如此循环,当每一步都没有盘子移动时就进入下一步
        */
    if (n == 0)
    {
        return;
    }
    Move(n - 1, a, b, c);
    count++; //最大的盘子从a移到b
    Move(n - 1, c, b, a);
    count++;//把最大的盘子从b移到c
    Move(n - 1, a, b, c);
}

int main()
{
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        count = 0;
        Move(n, 'a', 'b', 'c');
        printf("%d\n", count);
    }
    return 0;
}

//牛客BC164
//#include <stdio.h>
//#include<math.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    for (int i = n; i <= m; i++)
//    {
//        int sum = 0;
//        int tmp = i;
//        while (tmp)
//        {
//            sum += pow(tmp % 10, 4);
//            tmp /= 10;
//        }
//        if (sum == i)
//        {
//            printf("%d ", sum);
//        }
//    }
//    return 0;
//}

//牛客BC163
//int main()
//{
//    int n = 0;
//    char a1, a2, a3, a4;
//    scanf("%d %c %c %c %c", &n, &a1, &a2, &a3, &a4);
//    char str[50] = { 0 };
//    getchar();
//    gets(str);
//    for (int i = 0; str[i] != '\0'; i++)
//    {
//        if (str[i] == a1)
//        {
//            str[i] = a2;
//        }
//        if (str[i] == a3)
//        {
//            str[i] = a4;
//        }
//    }
//    puts(str);
//    return 0;
//}

//牛客 BC149
//#include <stdio.h>
//#include<ctype.h>
//int main()
//{
//    char str[1000] = { 0 };
//    char ret[100] = { 0 };
//    gets(str);
//    int flag = 1;
//    int i = 0;
//    int j = 0;
//    while (str[i] != '\0')
//    {
//        //1.不是空格
//        //2.是单词的开头
//        if (str[i] != ' ' && flag == 1)
//        {
//            if (islower(str[i]))
//            {
//                ret[j++] = toupper(str[i]);
//            }
//            else
//            {
//                ret[j++] = str[i];
//            }
//            flag = 0;
//            i++;//向后移
//        }
//        //是空格了，说明一个单词结束了或者开始是单词了
//        else if (str[i] == ' ')
//        {
//            flag = 1;
//            i++;
//        }
//        else
//        {
//            //属于一个已经操作过的单词
//            i++;
//        }
//    }
//    puts(ret);
//    return 0;
//}

//牛客 BC148
//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    char str[101] = { 0 };
//    scanf("%s", str);
//    int left = 0;
//    int right = 0;
//    char c1 = 0;
//    char c2 = 0;
//    for (int i = 0; i < m; i++)
//    {
//        scanf("%d %d %c %c", &left, &right, &c1, &c2);
//        for (int j = left; j <= right; j++)
//        {
//            if (str[j - 1] == c1)
//            {
//                str[j - 1] = c2;
//            }
//        }
//    }
//    puts(str);
//    return 0;
//}

//int main()
//{
//	union
//	{
//		int a;
//		long b;
//		unsigned char c;
//	}m;
//	m.b = 0x12345678;
//	printf("%x\n", m.a);
//	printf("%x\n", m.c);
//	return 0;
//}

//struct s
//{
//	int x;
//	int y;
//}test1 = {12,12}, test2;

//int main()
//{
//	/*test2 = test1;
//	struct s test3 = { 1,2 };*/
//
//	struct s
//	{
//		int n;
//		struct s* next;
//	};
//	struct s arr[3] = { 5, &arr[1], 7,&arr[2], 8,'\0' };
//	struct s* p = arr;
//	//printf("%d\n", p++->n);
//	//printf("%d\n", p->n);
//
//	//p = arr;
//	//printf("%d\n", ++p->n);
//
//	//printf("%d\n", p->n++);
//
//	return 0;
//}

//int main()
//{
//	struct s
//	{
//		char ch[12];
//	}arr[2];
//	//scanf("%s", arr->ch);
//	scanf("%s", &(*arr).ch);
//	printf("%s\n", arr->ch);
//	return 0;
//}