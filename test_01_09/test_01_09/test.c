#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
int convertInteger(int A, int B) {
    int tmp = A ^ B;
    int count = 0;
    for (int i = 0; i < 32; i++)
    {
        if ((tmp >> i) & 1)
        {
            count++;
        }
    }
    return count;
}

//#include <ctype.h>
//#include <stdio.h>
//#include<string.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    char arr[100] = { 0 };
//    for (int i = 0; i < n; i++)
//    {
//        int up = 0;
//        int low = 0;
//        int dig = 0;
//        scanf("%s", arr);
//        if (strlen(arr) < 8)
//        {
//            printf("NO\n");
//            continue;
//        }
//        if (arr[0] >= '0' && arr[0] <= '9')
//        {
//            printf("NO\n");
//            continue;
//        }
//        for (int j = 0; arr[j] != '\0'; j++)
//        {
//            if (isalpha(arr[j]) || isdigit(arr[j]))
//            {
//                if (isupper(arr[j]))
//                {
//                    up++;
//                }
//                else if (islower(arr[j]))
//                {
//                    low++;
//                }
//                else
//                {
//                    dig++;
//                }
//            }
//            else
//            {
//                printf("NO\n");
//                break;
//            }
//        }
//        if ((up == 0 && low == 0) || (up == 0 && dig == 0) || (low == 0 && dig == 0))
//        {
//            printf("NO\n");
//        }
//        else
//        {
//            printf("YES\n");
//        }
//    }
//
//    return 0;
//}

//int cmp(int* a, int* b)
//{
//    return *a - *b;
//}
//
//int* findErrorNums(int* nums, int numsSize, int* returnSize) {
//    int* arr = (int*)malloc(sizeof(int) * 2);
//    qsort(nums, numsSize, sizeof(int), cmp);
//    int pre = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        int cur = nums[i];
//        //当前元素和前一个相等，记录前一个
//        if (cur == pre)
//        {
//            arr[0] = pre;
//        }
//        else if (cur - pre > 1)
//        {
//            arr[1] = pre + 1;
//        }
//        pre = cur;
//    }
//    if (nums[numsSize - 1] != numsSize)
//    {
//        arr[1] = numsSize;
//    }
//    *returnSize = 2;
//    return arr;
//}

//int minNumberInRotateArray(int* nums, int numsLen) {
//    // write code here
//
//    //前提：该数组旋转前是升序的
//    int left = 0;
//    int right = numsLen - 1;
//    while (left < right)
//    {
//        int mid = (left + right) / 2;
//        //中间的数小于最右侧的，排除右侧
//        //5 1 2 3 4
//        if (*(nums + mid) < *(nums + right))
//        {
//            right = mid;
//        }
//        //中间的数大于最右侧,排除左侧
//        //3 4 5 1 2
//        else if (*(nums + mid) > *(nums + right))
//        {
//            left = mid + 1;
//        }
//        //中间数和两边都相等
//        else
//        {
//            right--;
//        }
//
//    }
//    return *(nums + left);
//}

//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n];
//    int count_minus = 0;
//    int count_pos = 0;
//    int sum = 0;
//    double averege = 0;
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (int i = 0; i < n; i++)
//    {
//        if (arr[i] < 0)
//        {
//            count_minus++;
//        }
//        else if (arr[i] > 0)//0不计入
//        {
//            count_pos++;
//            sum += arr[i];
//        }
//    }
//    averege = (double)sum / count_pos;
//    if (count_pos == 0)
//    {
//        averege = 0;
//    }
//    printf("%d %0.1lf\n", count_minus, averege);
//}

//int isPrime(int n)
//{
//    if (n < 2)
//    {
//        return 0;
//    }
//    for (int i = 2; i < n; i++)
//    {
//        if (n % i == 0)
//        {
//            return 0;
//        }
//    }
//    return 1;
//}
//
//void factorize(int n)
//{
//    int i = 0;
//    for (i = 2; i <= n; i++)
//    {
//        //如果i是质数且是n的因数
//        if (isPrime(i) && n % i == 0)
//        {
//            printf("%d ", i);
//            n = n / i;//n要变小，需要判断剩下的数是不是质因数
//
//            //继续输出相同的质因数
//            while (n % i == 0)
//            {
//                printf("%d ", i);
//                n /= i;
//            }
//            //如果n变为1，说明已经分解完毕
//            if (n == 1)
//            {
//                break;
//            }
//        }
//    }
//}
//
//int main() 
//{
//    int num;
//    printf("请输入一个数：");
//    scanf("%d", &num);
//    printf("分解后的质因数：");
//    factorize(num);
//    printf("\n");
//    return 0;
//}

//void primeFactors(int n) {
//    while (n % 2 == 0) {
//        printf("%d ", 2);
//        n = n / 2;
//    }
//    for (int i = 3; i * i <= n; i = i + 2) {
//        while (n % i == 0) {
//            printf("%d ", i);
//            n = n / i;
//        }
//    }
//    if (n > 2) {
//        printf("%d ", n);
//    }
//}
//
//int main() {
//    int number;
//    printf("Enter a number: ");
//    scanf("%d", &number);
//    printf("The prime factors of %d are: ", number);
//    primeFactors(number);
//    return 0;
//}
