#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>

int my_atoi(const char* str)
{
	int sum = 0;
	while (*str)
	{
		if (isspace(*str))
		{
			str++;
		}
		else
		{
			sum = sum * 10 + (*str - '0');
			str++;
		}
	}
	return sum;
}

int main()
{
	char str[10] = " 123";
	int ret = my_atoi(str);
	printf("%d\n", ret);
	return 0;
}

/*
当buf1<buf2时，返回值<0
当buf1=buf2时，返回值=0
当buf1>buf2时，返回值>0
*/
//int my_memcmp(const void* buffer1, const void* buffer2, int num)
//{
//	//// 当比较位数不为0时，且每位数据相等时，移动指针
//	while (--num  && *(char*)buffer1 == *(char*)buffer2)
//	{
//		buffer1 = (char*)buffer1 + 1;
//		buffer2 = (char*)buffer2 + 1;
//	}
//	return (*(char*)buffer1) - (*(char*)buffer2);
//}
//
//int main()
//{
//	char buffer1[] = "aaab";
//	char buffer2[] = "aaaa";
//	int n;
//	n = my_memcmp(buffer1, buffer2, sizeof(buffer1));
//
//	if (n > 0) 
//		printf("'%s' is greater than '%s'.\n", buffer1, buffer2);
//	else if (n < 0) 
//		printf("'%s' is less than '%s'.\n", buffer1, buffer2);
//	else 
//		printf("'%s' is the same as '%s'.\n", buffer1, buffer2);
//
//	return 0;
//}

//int main1()
//{
//	int num1 = 10;
//	while (num1--)
//	{
//		printf("%d ", num1);
//	}
//	printf("\n");
//	int num2 = 10;
//	while (--num2)
//	{
//		printf("%d ", num2);
//	}
//	return 0;
//}

//void* my_memset(void* ptr, int x, int num)
//{
//	void* ret = ptr;
//	while (num--)
//	{
//		*((char*)ptr + num) = x;
//	}
//	return ret;
//}
//int main()
//{
//	char str[] = "hello world";
//	my_memset(str, 'x', 6);
//	printf(str);
//	return 0;
//}

//void* my_memmove(void* dest, const void* src, int num)
//{
//	void* ret = dest;
//	//从前向后拷
//	if (dest < src)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	//从后向前拷
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 0 };
//	memmove(arr1+4, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);  //1 2 3 4 1 2 3 4 5 10
//	}
//	return 0;
//}

//#include<assert.h>
//#include <errno.h>
//void* my_memcpy(void* dest, const void* src, int num)
//{
//	void* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
//
//struct Stu
//{
//	int age;
//	char name[20];
//	int score;
//};
//int main()
//{
//	struct Stu s1 = { 18, "zhangsan", 60 };
//	struct Stu s2 = { 27, "lisi", 80 };
//	my_memcpy(&s1, &s2, sizeof(s1));
//	printf("%d %s %d\n", s1.age, s1.name, s1.score);
//	return 0;
//}

//int main()
//{
//	FILE* pFile;
//	pFile = fopen("unexist.ent", "r");
//	if (pFile == NULL)
//		printf("Error opening file unexist.ent: %s\n", strerror(errno));
//	return 0;
//}
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 10; i++) {
//		printf("%d:%s\n", i,strerror(i));
//	}
//	return 0;
//}

//int main()
//{
//	char arr[] = "www.baidu.com";
//	char* del = ".";
//	char* str = NULL;
//	for (str = strtok(arr, del); str != NULL; str = strtok(NULL, del))
//	{
//		printf("%s\n", str);
//	}
//	return 0;
//}
//char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//	char* ptr1 = NULL;
//	char* ptr2 = NULL;
//	if (!(*str2))
//	{
//		return (char*)str1;
//	}
//	while (*str1)
//	{
//		ptr1 = (char*)str1;
//		ptr2 = (char*)str2;
//		while ((*ptr1 == *ptr2) && *ptr1 && *ptr2)
//		{
//			ptr1++;
//			ptr2++;
//		}
//		if (*ptr2 == 0)
//		{
//			return (char*)str1;
//		}
//		str1++;
//	}
//	return NULL;
//}

//int main()
//{
//	char* str1 = "ab";
//	char* str2 = "cdab";
//	char* ret = my_strstr(str1, str2);
//	if (ret != NULL)
//	{
//		printf("找到了\n");
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}

//char* my_strncat(char* dest, const char* src, int num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* ret = dest;
//	//找dest的\0
//	while (*dest)
//	{
//		dest++;
//	}
//	//开始追加
//	while (num-- && (*dest++ = *src++))
//	{
//		;
//	}
//	*dest = 0;
//	return ret;
//}
//
//int main()
//{
//	char str1[20] = "hello-";
//	char str2[] = "world";
//	char* ret = my_strncat(str1, str2, 3);
//	printf("%s\n", ret);
//	return 0;
//}

//char* my_strncpy(char* dest, const char* src, int num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* ret = dest;
//	while ((*dest++ = *src++) && --num)
//	{
//		;
//	}
//	while (--num)
//	{
//		*dest = 0;
//		dest++;
//	}
//	return ret;
//}

//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}

//int main()
//{
//	char str1[10] = "123456789";
//	char str2[] = "hello";
//	char* ret = my_strncpy(str1, str2, 8);
//	printf("%s\n", ret);
//	return 0;
//}
//int my_strncmp(const char* str1, const char* str2, int num)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//	//无字符串可比，直接返回0
//	if (!num)
//	{
//		return 0;
//	}
//	//1.两字符串相等
//	//2.在规定的范围内
//	//3.字符串未到末尾
//	while ((*str1 == *str2) && --num && *str1 != '\0')
//	{
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//	char* str1 = "hello";
//	char* str2 = "hello";
//	int ret = ，my_strncmp(str1, str2, 9);
//	printf("%d\n", ret);
//	return 0;
//}

//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	return str1 - str2;
//	/*if (*str1 > *str2)
//	{
//		return 1;
//	}
//	else
//		return -1;*/
//}

//size_t my_strlen(char* str)
//{
//	if (*str == '\0')
//	{
//		return 0;
//	}
//	else
//	{
//		return 1 + my_strlen(str + 1);
//	}
//}

//size_t my_strlen(char* str)
//{
//	char* cur = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str-cur;
//}

//size_t my_strlen(char* str)
//{
//	size_t count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//int main()
//{
//	char* str = "hello world";
//	int ret = my_strlen(str);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	const char* str1 = "abcdef";
//	const char* str2 = "bbb";
//	if (strlen(str2) - strlen(str1) > 0)
//	{
//		printf("str2>str1\n");
//	}
//	else
//	{
//		printf("srt1>str2\n");
//	}
//	return 0;
//}