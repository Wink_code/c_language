#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
void Print(int* p, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", *(p + i));
	}
	printf("\n");
}
void Bubble_sort(int* p, int sz)
{
	int i = 0;
	for (i = 0; i < sz-1; i++)
	{
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (*(p + j) < *(p + j + 1))
			{
				int tmp = *(p + j);
				*(p + j) = *(p + j + 1);
				*(p + j + 1) = tmp;
			}
		}
	}
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	Bubble_sort(arr, sz);
	Print(arr, sz);
	return 0;
}

//void Adjust_arr(int* pa, int sz)
//{
//	int* left = pa;
//	int* right = pa + sz - 1;
//	while (left < right)
//	{
//		//前面的是奇数，不变，指针后移
//		while (left < right && *left % 2 != 0)
//		{
//			left++;
//		}
//		//后面的是偶数，不变，指针前移
//		while (left < right && *right % 2 == 0)
//		{
//			right--;
//		}
//		//前面是偶数，后面是奇数，交换
//		int tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	int arr[] = {2,2,2,2,1,1,1,1,1,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Adjust_arr(arr, sz);
//	Print(arr, sz);
//	return 0;
//}
//void Print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	printf("\n");
//}
//void Bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz-1; i++)
//	{
//		int j = 0;
//		int flag = 1;
//		for (j = 0; j <sz - 1 - i ; j++)
//		{
//			if (arr[j] < arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//		if (flag)
//		{
//			break;
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Bubble_sort(arr, sz);
//	Print(arr, sz);
//	return 0;
//}
//void Print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Print(arr,sz);
//	return 0;
//}
//int main()
//{
//	int* p = NULL;
//	int arr[10] = { 0 };
//	int(*ptr)[10] = &arr;
//	p = &arr;
//	p = &arr[0];
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[n];
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	float count = 0;
//	for (int i = 0; i < n; i++)
//	{
//		if (arr[i] <= 60)
//		{
//			count += 0.1;
//		}
//		else
//		{
//			count += 0.2;
//		}
//	}
//	printf("%.1f\n", count);
//	return 0;
//}
//#include<math.h>
//int is_prime(int n)
//{
//    int i = 0;
//    for (i = 2; i <= sqrt(n); i++)
//    {
//        if (n % i == 0)
//        {
//            break;
//        }
//    }
//    if (i > sqrt(n))
//    {
//        return 1;
//    }
//    else
//    {
//        return 0;
//    }
//}
//int main()
//{
//    int a = 0;
//    int b = 0;
//    scanf("%d %d", &a, &b);
//    int i = 0;
//    int sum = 0;
//    for (i = a; i <= b; i++)
//    {
//        if (is_prime(i))
//        {
//            sum += i;
//        }
//    }
//    printf("%d\n", sum);
//}