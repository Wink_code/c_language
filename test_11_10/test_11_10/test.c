#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
int fun(int n, int m)
{
    int count = 0;
    for (int i = n; i <= m; i++)
    {
        int tmp = i;
        while (tmp)
        {
            if (tmp % 10 == 2)
            {
                count++;
            }
            tmp /= 10;
        }
    }
    return count;
}
int main()
{
    int n = 0;
    int m = 0;
    scanf("%d %d", &n, &m);
    int ret = fun(n, m);
    printf("%d\n", ret);
    return 0;
}
//int fun(int n)
//{
//    int sum = 0;
//    while (n)
//    {
//        sum += n % 10;
//        n /= 10;
//    }
//    if (sum < 10)
//    {
//        return sum;
//    }
//    else
//    {
//        return   fun(sum);
//    }
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int ret = fun(n);
//    printf("%d\n", ret);
//    return 0;
//}
//int judge_number(int a)
//{
//    int sum = 0;
//    int flag = 0;
//    while (a)
//    {
//        sum += a % 10;
//        a /= 10;
//
//    }
//    if (sum % 5 == 0)
//    {
//        return 1;
//    }
//    else
//    {
//        return 0;
//    }
//}
//int main()
//{
//    int a = 0;
//    int b = 0;
//    scanf("%d %d", &a, &b);
//    int count = 0;
//    for (int i = a; i <= b; i++)
//    {
//        if (judge_number(i))
//        {
//            count++;
//        }
//    }
//    printf("%d\n", count);
//    return 0;
//}
//float max(int x, int y, int z)
//{
//    int max = x > y ? x : y;
//    max = max > z ? max : z;
//    return max;
//}
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    scanf("%d %d %d", &a, &b, &c);
//    float ret = max(a + b, b, c) / (max(a, b + c, c) + max(a, b, b + c));
//    printf("%.2f\n", ret);
//}
//int main()
//{
//	unsigned a = 0112 , x, y, z;
//	//010
//	x = a >> 3;
//	//00001001
//	y = ~(~0 << 4);
//	//11110000
//	//00001111
//	z = x & y;
//	printf("%o %o %o\n", x, y, z);
//	return 0;
//}
//int main()
//{
//	int a = 0123;
//	int b = 5 + (int)a;
//	int c = ~2;
//	int d = b & c;
//	return 0;
//}
//int main()
//{
//	int a;
//	a = ~0x9a;
//	//00000000000000000000000010011010 ---- 0x9a的补码
//	//11111111111111111111111101100101 ----~0x9a的补码
//	//10000000000000000000000010011010 ----~0x9a反码
//	//10000000000000000000000010011011 ----~0x9a原码
//
//	//x:-9b  
//	//d:-155
//	printf("%x\n", a);
//	printf("%d\n", a);
//	return 0;
//}


//void reverse(char* s, int k)
//{
//	while (k--)
//	{
//		char tmp = *s; //存储第一个字符
//		char* cur = s; //s不能动
//		while (*(cur+1) != '\0')
//		{
//			*cur = *(cur + 1);
//			cur++;
//		}
//		*cur = tmp;
//	}
//}
//int main()
//{
//	char s[] = "ABCD";
//	int k = 0;
//	scanf("%d", &k);
//	reverse(s, k);
//	puts(s);
//	return 0;
//}
//int my_strlen(char* star)
//{
//	char* end = star;
//	while (*end)
//	{
//		end++;
//	}
//	return end - star;
//}
//int main()
//{
//	char arr[] = "aaaaasssss";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//}
//#include<string.h>
//void reverse(char* pc)
//{
//    int len = strlen(pc) - 1;
//    char* left = pc;
//    char* right = pc+len;
//    while (left <= right)
//    {
//        char tmp = *left;
//        *left = *right;
//        *right = tmp;
//        left++;
//        right--;
//    }
//}
//int main()
//{
//    char arr[10000] = { '\0' };
//    char c = 0;
//    int i = 0;
//    while ((c = getchar()) != EOF)
//    {
//        arr[i] = c;
//        i++;
//    }
//    reverse(arr);
//    for (int i = 1; arr[i] != '\0'; i++)
//    {
//        printf("%c", arr[i]);
//    }
//    return 0;
//}
//int main()
//{
//    char arr[10] = { '\0' };
//    char c = 0;
//    int i = 0;
//    while ((c = getchar()) != '\0')
//    {
//        arr[i] = c;
//        i++;
//    }
//	return 0;
//}
//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}
//int* test()
//{
//	int a = 20;
//	return &a;
//}
//
//int main()
//{
//	int* ret = test();
//	printf("%d %d\n", 5,6);
//	printf("%d\n", *ret);
//	return 0;
//}