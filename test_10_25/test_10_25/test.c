#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//二分查找

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int n = 0;
	scanf("%d", &n);
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 1;

	while (left <= right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] == n)
		{
			printf("找到了，下标是：%d\n", mid);
			break;
		}
		else if (arr[mid] < n)
		{
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
	}
	//1.break    2.left>right
	if (left > right)
	{
		printf("找不到\n");
	}
	return 0;
}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	char ch = 0;
//	scanf("%d%c%di", &a, &ch, &b);
//	if (ch == '+')
//	{
//		ch = '-';
//	}
//	else if (ch == '-')
//	{
//		ch = '+';
//	}
//	else
//		printf("error\n");
//		return 1;
//	printf("%d%c%di\n", a, ch, b);
//	return 0;
//}
//int main()
//{
//	int score = 0;
//	char grade = 0;
//	scanf("%d", &score);
//	switch (score / 10)
//	{
//	case 10:
//	case 9:
//		grade = 'A';
//		break;
//	case 8:
//		grade = 'B';
//		break;
//	case 7:
//		grade = 'C';
//		break;
//	case 6:
//		grade = 'D';
//		break;
//	default:
//		grade = 'E';
//	}
//	printf("%d分为%c级\n", score, grade);
//	return 0;
//}

//int main()
//{
//	float data1 = 0;
//	float data2 = 0;
//	float data3 = 0;
//	char ch = 0;
//	scanf("%f %c %f", &data1, &ch, &data2);
//	switch (ch)
//	{
//	case '+':
//		data3 = data1 + data2;
//		break;
//	case '-':
//		data3 = data1 - data2;
//		break;
//	case '*':
//		data3 = data1 * data2;;
//		break;
//	case '/':
//		if (data2 == 0)
//		{
//			printf("除法分母不能为0\n");   //注意，容易遗忘
//			return 0;
//		}
//		data3 = data1 / data2;
//		break;
//	}
//	printf("%f %c %f= %f\n", data1, ch, data2, data3);
//	return 0;
//}
//int main()
//{
//	float x = 0;
//	int y = 0;
//	scanf("%f", &x);
//	switch (x < 0)
//	{
//	//不小于0
//	case 0: 
//		switch (x == 0)
//		{
//		//等于0
//		case 1: 
//			y = 0;
//			break;
//		//大于0
//		default: 
//			y = 1;
//			break;
//		}
//		break;
//	//小于0
//	case 1: 
//		y = -1;
//		break;
//	}
//	printf("y=%d\n", y);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	if (n % 3 == 0 && n % 5 == 0 && n % 7 == 0)
//		printf("能同时被3、5、7整除\n");
//	else if (n % 3 == 0 && n % 5 == 0)
//		printf("能同时被3、5整除\n");
//	else if (n % 3 == 0 && n % 7 == 0)
//		printf("能同时被3、7整除\n");
//	else if (n % 5 == 0 && n % 7 == 0)
//		printf("能同时被5、7整除\n");
//	else if (n % 3 == 0)
//		printf("能被3整除\n");
//	else if (n % 5 == 0)
//		printf("能被5整除\n");
//	else if (n % 7 == 0)
//		printf("能被7整除\n");
//	else
//		printf("不能被3、5、7中的任一个整除\n");
//	return 0;
//}

//int main()
//{
//	int x = 0;
//	int y = 0;
//	int flag = 0;
//	scanf("%d", &x);
//	if (x < 0)
//	{
//		y = 0;
//	}
//	else if (x > 0 && x <= 10)
//	{
//		y = x;
//	}
//	else if (x > 10 && x <= 20)
//	{
//		y = 10;
//	}
//	else if (x > 20 && x < 40)
//	{
//		y = -0.5 * x + 20;
//	}
//	else
//		flag = 1;
//	if (flag == 0)
//	{
//		printf("y=%d\n", y);
//	}
//	else
//		printf("errer\n");
//	return 0;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	if (n % 5 == 0 && n % 7 == 0)
//	{
//		printf("yes\n");
//	}
//	else
//		printf("no\n");
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int sum = a * a + b * b;
//	if (sum > 100)
//	{
//		printf("%d\n", sum / 100);
//	}
//	else
//	{
//		printf("%d\n", sum);
//	}
//}

//多个字符从两端移动，向中间汇聚
//#include<string.h>
//#include<stdlib.h>
//#include<Windows.h>
//
//int main()
//{
//	char arr1[] = "*********************";
//	char arr2[] = "welcome to China !!!!";
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	int left = 0;
//	int right = sz - 2;
//	//int sz = strlen(arr1);
//	//int left = 0;
//	//int right = sz - 1;
//	while (left <= right)
//	{
//		arr1[left] = arr2[left];
//		arr1[right] = arr2[right];
//		printf("%s\n", arr1); 
//		Sleep(1000);  //单位是毫秒
//		system("cls");
//		left++;
//		right--;
//	}
//	printf("%s\n", arr1);
//	return 0;
//}
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int arr2[10] = { 0,9,8,7,6,5,4,3,2,1 };
//	int i = 0;
//	printf("交换前：\n");
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	printf("\n");
//	for (i = 0; i < 10; i++)
//	{
//		int tmp = 0;
//		tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//	printf("交换后：\n");
//	//交换后
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int arr1[10][10] = { 0 };
//    int arr2[10][10] = { 0 };
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < n; i++)  //行
//    {
//        for (j = 0; j < m; j++) //列
//        {
//            scanf("%d", &arr1[i][j]);
//            arr2[j][i] = arr1[i][j];
//        }
//    }
//    for (i = 0; i < m; i++)
//    {
//        for (j = 0; j < n; j++)
//        {
//            printf("%d ", arr2[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//int main() 
//{
//    int arr[10] = { 0 };
//    int i = 0;
//    for (i = 0; i < 10; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = 9; i >= 0; i--)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//* * * * 
//*     * 
//*     *
//* * * *	
//int main()
//{
//    int a;
//    int i = 0;
//    while (scanf("%d", &a) != EOF)
//    {
//        for (i = 0; i < a; i++)
//        {
//            printf("* ");
//        }
//        printf("\n");
//
//        int j = 0;
//        //控制行
//        for (i = 1; i < a - 1; i++)
//        {
//            //控制列
//            for (j = 0; j < a; j++)
//            {
//                if (j == 0 || j == a - 1)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//
//            }
//            printf("\n");
//        }
//        for (i = 0; i < a; i++)
//        {
//            printf("* ");
//        }
//        printf("\n");
//    }
//    return 0;
//}





//*   *   00   04
// * *	  11   13
//  *	  22   22
// * *	  33   31
//*   *   44   40

//int main()
//{
//    int a;
//    int i = 0;
//    while (scanf("%d", &a) != EOF)
//    {
//        for (i = 0; i < a; i++)
//        {
//            int j = 0;
//            for (j = 0; j < a; j++)
//            {
//                //左对角线    
//                if (i == j || i + j == a - 1)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	double sum = 0;
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr[i]);
//		sum += arr[i];
//	}
//	printf("average=%.2lf\n", sum / sz);
//	return 0;
//}

//int main()
//{
//	int a = 1, b = 2, c = 2;
//	int t = 0;
//	while(a < b < c)
//	{
//		t = a;
//		a = b;
//		b = t;
//		c--;
//	}
//	printf("%d,%d,%d", a, b, c);
//	return 0;
//}