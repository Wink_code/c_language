#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int main()
//{
//	double sum = 0;
//	double flag = 1;
//	int i = 1;
//	for (i = 1; i <= 100; i++)
//	{
//		sum += flag / i;
//		flag = -flag;
//	}
//	printf("%lf\n", sum);
//	return 0;
//}

//int main()
//{
//	int count = 0;
//	int i = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		//十位数为9   90,91,92.....
//		if (i / 10 == 9)
//		{
//			count++;
//		}
//		//个位数为9   9,19,29.....
//		if (i % 10 == 9)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);//20
//	return 0;
//}

//int BinSearch(int arr[], int sz, int val)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		int mid = (left + right) / 2;
//		if (arr[mid] == val)
//		{
//			return mid;
//		}
//		else if (arr[mid] > val)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			left = mid + 1;
//		}
//	}
//	return -1;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = BinSearch(arr, sz, 1);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int money = 0;
//	scanf("%d", &money);
//	int empty = money;
//	int total = money;//共喝了多少
//
//	//如果空瓶的数量大于1个
//	while (empty >= 2)
//	{
//		total += empty / 2;//空瓶可以换多少
//		empty = empty / 2 + empty % 2;//换的+没用完的空瓶
//	}
//	printf("%d\n", total);
//	return 0;
//}

//#include<math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 100000; i++)
//	{
//		int tmp = i;
//		int n = 1;
//		int sum = 0;
//		//求位数
//		while (tmp > 9)
//		{
//			n++;
//			tmp /= 10;
//		}
//
//		tmp = i;
//		while (tmp)
//		{
//			sum += (int)pow(tmp % 10, n);
//			tmp /= 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int Pow(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	return n * Pow(n, k - 1);
//}

//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int ret = Pow(n, k);
//	printf("%d\n", ret);
//	return 0;
//}

//int DigitSum(int n)
//{
//	if (n < 10)
//	{
//		return n;
//	}
//	return (n % 10) + DigitSum(n / 10);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = DigitSum(n);
//	printf("%d\n", ret);
//	return 0;
//}

//void Print(int n)
//{
//	if (n > 10)
//	{
//		Print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	Print(n);
//	return 0;
//}


//方法1：对2取模
//int NumberOfOne(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		if (n % 2 == 1)
//		{
//			count++;
//		}
//		n /= 2;
//	}
//	return count;
//}

//int NumberOfOne(int n)
//{
//	int count = 0;
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((n >> i) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	return count;
//}

//int NumberOfOne(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		n = n & (n - 1);
//		count++;
//	}
//	return count;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	//打印奇数位
//	//要正序打印，所以从第31位开始
//	for (i=30; i>=0; i-=2)
//	{
//		printf("%d", (n >> i) & 1);
//	}
//	printf("\n");
//	//打印偶数位
//	for (i = 31; i >= 1; i-=2)
//	{
//		printf("%d", (n >> i) & 1);
//	}
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	int m = 0;
//	int count = 0;
//	int i = 0;
//	scanf("%d %d", &n, &m);
//	n = n ^ m;
//	
//	/*for (i = 0; i < 32; i++)
//	{
//		if (((n >> i) & 1) == 1)
//		{
//			count++;
//		}
//	}*/
//	while (n)
//	{
//		n = n & (n - 1);
//		count++;
//	}
//	printf("%d\n", count);
//	
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,1,2,2,3,4,4,5,5 };
//	int ret = 0;
//	for (int i = 0; i < 9; i++)
//	{
//		ret ^= arr[i];
//	}
//	printf("%d\n", ret);
//	return 0;
//}

//#include<string.h>
//void leftRound(char* str, int time)
//{
//	int i = 0;
//	int j = 0;
//	char tmp = 0;
//	int len = strlen(str);
//	time %= len;//长度为5的情况下，旋转6、11、16...次相当于1次，7、12、17...次相当于2次，以此类推。
//	for (i = 0; i < time; i++)
//	{
//		tmp = str[0];
//		for (j = 0; j < len - 1; j++)
//		{
//			str[j] = str[j + 1];
//		}
//		str[j] = tmp;
//	}
//}

//#include<string.h>
//#include<stdlib.h>
//void leftRound(char* str, int time)
//{
//	int len = strlen(str);
//	int pos = time % len;//断开的位置
//	char* tmp = (char*)malloc(sizeof(len + 1));
//	if (tmp == NULL)
//		return;
//	strcpy(tmp, str + pos);//先把后面的拷过来
//	strncat(tmp, str, pos);//拼接
//	strcpy(str, tmp);//再拷回去
//}

//#include<string.h>
//#include<stdlib.h>
//void reverse(char* str, int start, int end)
//{
//	int left = start;
//	int right = end;
//	while (left < right)
//	{
//		char tmp = str[left];
//		str[left] = str[right];
//		str[right] = tmp;
//		left++;
//		right--;
//	}
//}
//
//void leftRound(char* str, int time)
//{
//	int len = strlen(str);
//	int pos = time % len;//断开的位置
//
//	reverse(str, 0, pos - 1);//先逆序前半段
//	reverse(str, pos, len - 1);//再逆序后半段
//	reverse(str, 0, len - 1);//整体逆序
//}
//
//int main()
//{
//	char str[] = "AABCD";
//	leftRound(str, 1);
//	printf("%s\n", str);
//	return 0;
//}
//#include<string.h>
//#include<stdlib.h>
//int JudgeString(char* str, char* find)
//{
//	int len = strlen(str);
//	char* tmp = (char*)malloc(sizeof(len * 2 + 1));
//	if (tmp == NULL)
//		return -1;
//	strcpy(tmp, str);
//	strcat(tmp, str);
//
//	return strstr(tmp, find) != NULL; //strstr找不到返回空指针
//}
//
//int main()
//{
//	char s1[] = "AABCD";
//	char s2[] = "BCDAA";
//	int ret = JudgeString(s1, s2);
//	if (ret)
//	{
//		printf("是旋转得到的\n");
//	}
//	else
//	{
//		printf("不是旋转得到的\n");
//	}
//	return 0;
//}
//#include<stdlib.h>
//int cmp(const int* a, const int* b)
//{
//	return *a - *b;
//}
//
//void Find_sole_dog(int arr[], int sz, int dog[])
//{
//	int i = 0;
//	int j = 0;
//	while (i< sz)
//	{
//		//相等，走两步
//		if (arr[i] == arr[i + 1])
//		{
//			i += 2;
//		}
//		else
//		{
//			dog[j] = arr[i];
//			j++;
//			i++;
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int dog[2] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//先排序
//	qsort(arr, sz, sizeof(int), cmp);
//	Find_sole_dog(arr, sz, dog);
//	for (int i = 0; i < 2; i++)
//	{
//		printf("%d ", dog[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 10000; i < 100000; i++)
//	{
//		int tmp = i;
//		int sum = 0;
//		for (j = 10; j < 100000; j *= 10)
//		{
//			sum += (tmp / j) * (tmp % j);
//		}
//		if (sum == i)
//		{
//			printf("%d\n", i);
//		}
//	}
//	return 0;
//}


int main()
{
	int arr[] = { 1,2,3,4,8,1,2,3,4,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int i = 0;
	int tmp = 0;
	int dog1 = 0;
	int dog2 = 0;
	for (i = 0; i < sz ; i++)
	{
		tmp ^= arr[i];
	}
	//找出 一个 两单身狗不同的二进制位
	int pos = 0;
	for (i = 0; i < 32; i++)
	{
		if (((tmp >> i) & 1)) //一定不能写成==1！！！！
		{
			pos = i;//在这一位上，两个数一定是一个1一个0
			break;
		}
	}
	for (i = 0; i < sz; i++)
	{
		if (((1 << pos) & arr[i]))//一定不能写成==1！！！！
		{
			dog1 ^= arr[i];//将pos位为1的分为一组异或
		}
		else
		{
			dog2 ^= arr[i];//将pos位为0的分为一组异或
		}
	}
	printf("%d %d\n", dog1, dog2);
	return 0;
}