#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

#include<stdio.h>
#include<string>
#include<vector>

#include<Windows.h>
namespace my 
{
	class Task
	{
	public:
		Task(string path, int state = 0)
			:_path(path)
			, _state(state)
		{}

		string _path;
		int _state;
	};

	void Load(vector<Task>& arr, int& count)
	{
		FILE* pfread = fopen("problem.txt", "r");
		if (pfread == nullptr)
		{
			perror("open fail");
			exit(0);
		}
		fscanf(pfread, "%d", &count);
		char path[500] = { '\0' };
		int sta;
		int n = 0;
		while (fscanf(pfread, "%s %d", path, &sta) != EOF)
		{
			arr.push_back({ path,sta });
		}
		fclose(pfread);
	}

	void Save(vector<Task>& arr, int count)
	{
		FILE* pfwrite = fopen("problem.txt", "w");
		if (pfwrite == nullptr)
		{
			perror("open fail");
			exit(0);
		}
		fprintf(pfwrite, "%d\n", count);
		for (Task task : arr)
		{
			fprintf(pfwrite, "%s %d\n", task._path.c_str(), task._state);
		}

		fclose(pfwrite);
	}

	//设置路径和状态
	void Init(vector<Task>& arr, int& count)
	{
		vector<string> AllPath
		{
			//双指针
			"https://leetcode.cn/problems/move-zeroes/description/",
			"https://leetcode.cn/problems/duplicate-zeros/description/",
			"https://leetcode.cn/problems/happy-number/description/",
			"https://leetcode.cn/problems/container-with-most-water/description/",
			"https://leetcode.cn/problems/valid-triangle-number/description/",
			"https://leetcode.cn/problems/he-wei-sde-liang-ge-shu-zi-lcof/description/",
			"https://leetcode.cn/problems/he-wei-sde-liang-ge-shu-zi-lcof/description/",
			"https://leetcode.cn/problems/3sum/description/",
			"https://leetcode.cn/problems/4sum/description/",
			//滑动窗口
			"https://leetcode.cn/problems/minimum-size-subarray-sum/description/",
			"https://leetcode.cn/problems/longest-substring-without-repeating-characters/description/",
			"https://leetcode.cn/problems/max-consecutive-ones-iii/description/",
			"https://leetcode.cn/problems/minimum-operations-to-reduce-x-to-zero/description/",
			"https://leetcode.cn/problems/fruit-into-baskets/description/",
			"https://leetcode.cn/problems/find-all-anagrams-in-a-string/description/",
			"https://leetcode.cn/problems/substring-with-concatenation-of-all-words/description/",
			"https://leetcode.cn/problems/minimum-window-substring/description/",
			//二分
			"https://leetcode.cn/problems/binary-search/description/",
			"https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array/description/",
			"https://leetcode.cn/problems/search-insert-position/description/",
			"https://leetcode.cn/problems/sqrtx/description/",
			"https://leetcode.cn/problems/peak-index-in-a-mountain-array/description/",
			"https://leetcode.cn/problems/find-peak-element/description/",
			"https://leetcode.cn/problems/find-minimum-in-rotated-sorted-array/description/",
			"https://leetcode.cn/problems/que-shi-de-shu-zi-lcof/description/",
			//前缀和
			"https://www.nowcoder.com/practice/acead2f4c28c401889915da98ecdc6bf?tpId=230&tqId=2021480&ru=/exam/oj&qru=/ta/dynamic-programming/question-ranking&sourceUrl=%2Fexam%2Foj%3Fpage%3D1%26tab%3D%25E7%25AE%2597%25E6%25B3%2595%25E7%25AF%2587%26topicId%3D196",
			"https://www.nowcoder.com/practice/99eb8040d116414ea3296467ce81cbbc?tpId=230&tqId=2023819&ru=/exam/oj&qru=/ta/dynamic-programming/question-ranking&sourceUrl=%2Fexam%2Foj%3Fpage%3D1%26tab%3D%25E7%25AE%2597%25E6%25B3%2595%25E7%25AF%2587%26topicId%3D196",
			"https://leetcode.cn/problems/find-pivot-index/description/",
			"https://leetcode.cn/problems/product-of-array-except-self/description/",
			"https://leetcode.cn/problems/subarray-sum-equals-k/description/",
			"https://leetcode.cn/problems/subarray-sums-divisible-by-k/description/",
			"https://leetcode.cn/problems/contiguous-array/description/",
			"https://leetcode.cn/problems/matrix-block-sum/description/",
			//位运算
			"https://leetcode.cn/problems/is-unique-lcci/description/",
			"https://leetcode.cn/problems/missing-number/description/",
			"https://leetcode.cn/problems/sum-of-two-integers/description/",
			"https://leetcode.cn/problems/single-number-ii/description/",
			"https://leetcode.cn/problems/missing-two-lcci/description/",
			//模拟
			"https://leetcode.cn/problems/replace-all-s-to-avoid-consecutive-repeating-characters/description/",
			"https://leetcode.cn/problems/teemo-attacking/description/",
			"https://leetcode.cn/problems/zigzag-conversion/description/",
			"https://leetcode.cn/problems/count-and-say/description/",
			"https://leetcode.cn/problems/minimum-number-of-frogs-croaking/description/",
			//快排
			"https://leetcode.cn/problems/sort-colors/description/",
			"https://leetcode.cn/problems/sort-an-array/description/",
			"https://leetcode.cn/problems/kth-largest-element-in-an-array/description/",
			"https://leetcode.cn/problems/zui-xiao-de-kge-shu-lcof/description/",
			//归并
			"https://leetcode.cn/problems/sort-an-array/description/",
			"https://leetcode.cn/problems/shu-zu-zhong-de-ni-xu-dui-lcof/description/",
			"https://leetcode.cn/problems/count-of-smaller-numbers-after-self/description/",
			"https://leetcode.cn/problems/reverse-pairs/description/",
			//链表
			"https://leetcode.cn/problems/add-two-numbers/description/",
			"https://leetcode.cn/problems/swap-nodes-in-pairs/description/",
			"https://leetcode.cn/problems/reorder-list/description/",
			"https://leetcode.cn/problems/merge-k-sorted-lists/description/",
			"https://leetcode.cn/problems/reverse-nodes-in-k-group/description/",
			//哈希表
			"https://leetcode.cn/problems/two-sum/description/",
			"https://leetcode.cn/problems/check-permutation-lcci/description/",
			"https://leetcode.cn/problems/contains-duplicate/description/",
			"https://leetcode.cn/problems/contains-duplicate-ii/description/",
			"https://leetcode.cn/problems/group-anagrams/description/",
			//字符串
			"https://leetcode.cn/problems/longest-common-prefix/description/",
			"https://leetcode.cn/problems/longest-palindromic-substring/description/",
			"https://leetcode.cn/problems/add-binary/description/",
			"https://leetcode.cn/problems/multiply-strings/description/",
			//栈
			"https://leetcode.cn/problems/remove-all-adjacent-duplicates-in-string/description/",
			"https://leetcode.cn/problems/backspace-string-compare/description/",
			"https://leetcode.cn/problems/basic-calculator-ii/description/",
			"https://leetcode.cn/problems/decode-string/description/",
			"https://leetcode.cn/problems/validate-stack-sequences/description/",
			//队列+bfs
			"https://leetcode.cn/problems/n-ary-tree-level-order-traversal/description/",
			"https://leetcode.cn/problems/binary-tree-zigzag-level-order-traversal/",
			"https://leetcode.cn/problems/maximum-width-of-binary-tree/description/",
			"https://leetcode.cn/problems/find-largest-value-in-each-tree-row/",
			//堆
			"https://leetcode.cn/problems/last-stone-weight/description/",
			"https://leetcode.cn/problems/kth-largest-element-in-a-stream/description/",
			"https://leetcode.cn/problems/top-k-frequent-words/",
			"https://leetcode.cn/problems/find-median-from-data-stream/description/",
			//BFS
			"https://leetcode.cn/problems/flood-fill/description/",
			"https://leetcode.cn/problems/number-of-islands/description/",
			"https://leetcode.cn/problems/max-area-of-island/description/",
			"https://leetcode.cn/problems/surrounded-regions/description/",
			//最短路径
			"https://leetcode.cn/problems/nearest-exit-from-entrance-in-maze/description/",
			"https://leetcode.cn/problems/minimum-genetic-mutation/description/",
			"https://leetcode.cn/problems/word-ladder/description/",
			"https://leetcode.cn/problems/cut-off-trees-for-golf-event/description/",
			//多源BFS
			"https://leetcode.cn/problems/01-matrix/",
			"https://leetcode.cn/problems/number-of-enclaves/description/",
			"https://leetcode.cn/problems/map-of-highest-peak/description/",
			"https://leetcode.cn/problems/as-far-from-land-as-possible/description/",
			//拓扑排序
			"https://leetcode.cn/problems/course-schedule/description/", 
			"https://leetcode.cn/problems/course-schedule-ii/description/",
			
			"https://leetcode.cn/problems/Jf1JuT/description/"
		};

		for (string path : AllPath)
		{
			arr.push_back({ path });
		}

		Save(arr, count);
		cout << "题库已重置" << endl;
		exit(0);
	}

	void GetProblem()
	{
		vector<Task> arr;
		int count = 0;

		//Init(arr, count);//当全部题目抽完后，在重置

		Load(arr,count);

		if (count == arr.size())
		{
			cout << "所有题目均已抽到，需要重置!" << endl;
			//Save(arr, count);

			exit(0);
		}

		string tmp;
		int input = 0;
		do
		{
			while (1)
			{
				int n = rand() % arr.size();
				if (arr[n]._state == 0)
				{
					tmp = arr[n]._path;//抽取题目
					arr[n]._state = 1;
					count++;
					break;
				}
				if (count == arr.size())
				{
					cout << "所有题目均已抽到，需要重置!" << endl;
					goto over;
				}
			}
			ShellExecuteA(nullptr, "open", tmp.c_str(), nullptr, nullptr, SW_SHOWNORMAL);
			cout << "当前题目为抽到的第 " << count << " 题：" << tmp << endl;
			cout << "还剩余 " << arr.size() - count << " 道题目，" << "是否继续抽题（1/0）？" << endl;
			cin >> input;
		} while (input == 1);

over:
		Save(arr, count);//保存
		exit(0);
	}
}

int main()
{
	srand((unsigned int)time(nullptr));
	my::GetProblem();
	return 0;
}