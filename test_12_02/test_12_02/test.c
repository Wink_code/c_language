﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void Find_num(int arr[], int sz, int* num1, int* num2)
{
	int ret = 0;
	int pos = 0;
	//1.先将所有数按位异或，ret是两个单身狗按位异或的值
	//0101 -- 5
	//0110 -- 6
	//0011   这里为1的二进制位说明两个数的对应位不一样
	for (int i = 0; i < sz; i++)
	{
		ret ^= arr[i];
	}
	//2.确定ret中，哪一位二进制是1
	for (int i = 0; i < 32; i++)
	{
		if (((ret >> i) & 1) == 1)
		{
			pos = i;
			break;
		}
	}
	for (int i = 0; i < sz; i++)
	{
		//3.将两数分开
		if (((arr[i] >> pos) & 1) == 1)
		{
			*num1 ^= arr[i];
		}
		else
		{
			*num2 ^= arr[i];

		}
	}

}

//寻找两只单身狗
int main()
{
	int arr[] = { 1,2,3,4,5,6,1,2,3,4 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int num1 = 0;
	int num2 = 0;
	Find_num(arr, sz, &num1, &num2);
	printf("%d,%d\n", num1, num2);
	return 0;
}

//int main()
//{
//	int n = 9;
//	float* pFloat = (float*)&n;
//	printf("n的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	*pFloat = 9.0;
//	printf("num的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	return 0;
//}

//9.0 按浮点数的规则存储
//9：1001   1.001
//(-1)^0  * (1.001) *    2^3
//  S		   M		E=3+127
//0  10000010   001 00000000000000000000  补20个0 
//然后以整数的方式去拿，整数，原、反、补相同，直接拿
//01000001000100000000000000000000

//0000 0000 0000 0000 0000 0000 0000 1001  9在内存中是这样存得
//此时我们按照浮点数得存储规则去拿：
//0   00000000    0000000000000000001001
//S      E                 M
//此时就可以写成这种形式：
//(-1)^0*0.0000000000000000001001*2^-126
//显然，这是一个很小得数,用十进制小数表示就是0.000000

//int main()
//{
//	int a[4] = { 1, 2, 3, 4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x,%x", ptr1[-1], *ptr2);
//	return 0;
//}

//int main()
//{
//	unsigned int i;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//	}
//	return 0;
//}
//int main()
//{
//	char a[1000];
//	int i;
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;
//	}
//	//a: -1,-2,-3...-128 127,126,125....3,2,1,0
//	//共255个数
//	printf("%d", strlen(a));
//	return 0;
//}

//int main()
//{
//	char a = 128;
//	printf("%u\n", a);
//	return 0;
//}

//int main()
//{
//	//10000000000000000000000010000000  -128原码
//	//11111111111111111111111101111111		反码
//	//11111111111111111111111110000000		补码
//	//发生截断 a:10000000
//	//整型提升  :11111111111111111111111110000000
//	char a = -128;
//	printf("%u\n", a);
//	//%u,无符号打印
//	return 0;
//}

//int main()
//{
//	//10000000000000000000000000000001
//	//11111111111111111111111111111110
//	//11111111111111111111111111111111  --- 补码
//	//放进char类型数中，发生截断
//	char a = -1;
//	//a : 11111111
//	//发生整型提升，有符号数，按符号位提升
//	//11111111111111111111111111111111 --补码
//	//10000000000000000000000000000001 -- 原码
//	signed char b = -1;
//	//b : 11111111
//	//发生整型提升，有符号数，按符号位提升
//	//11111111111111111111111111111111 --补码
//	//10000000000000000000000000000001 -- 原码
//	unsigned char c = -1;
//	//c : 11111111
//	//发生整型提升，无符号数，补0
//	//00000000000000000000000011111111 -- 补码 == 原码
//	printf("a=%d,b=%d,c=%d", a, b, c); 
//	//a=-1,b=-1,c=255
//	//%d,十进制形式打印有符号整数，会发生整型提升
//	return 0;
//}

//int check_sys()
//{
//	int i = 1;
//	return (*(char*)&i);
//}
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}

//int main()
//{
//	int a = 0x11223344;
//
//	return 0;
//}

//atoi函数的模拟实现
//#include<stdlib.h>
//#include<ctype.h>
//#include<assert.h>
//int my_atoi(const char* str)
//{
//	char* p = (char*)str;
//	assert(str != NULL);
//	int flag = 1;
//	int ret = 0;
//	while (*p)
//	{
//		//是空格
//		if (isspace(*p))
//		{
//			p++;
//		}
//		//是负数
//		else if (*p == '-')
//		{
//			flag = -1;
//			p++;
//		}
//		//是正数
//		else if (*p == '+')
//		{
//			flag = 1;
//			p++;
//		}
//		else
//		{
//			if (isdigit(*p))
//			{
//				ret = ret * 10 + (*p - '0');
//				p++;
//			}
//			else
//			{
//				break;
//			}
//		}
//	}
//	return ret * flag;
//}
//
//int main()
//{
//	char* str = "   -123";
//	int ret = my_atoi(str);
//	printf("%d\n", ret);
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (int k = 0; k < 10; k++)
//	{
//		printf("%d ", *arr);
//	}
//	return 0;
//}