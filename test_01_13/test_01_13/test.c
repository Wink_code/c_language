#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

 //leetcode238. 除自身以外数组的乘积
int* productExceptSelf(int* nums, int numsSize, int* returnSize) {
    int* answer = (int*)calloc(numsSize, sizeof(int));
    *returnSize = numsSize;
    int left[numsSize];
    int right[numsSize];
    left[0] = 1;
    right[numsSize - 1] = 1;
    for (int i = 1; i < numsSize; i++)
    {
        left[i] = left[i - 1] * nums[i - 1];
    }
    for (int i = numsSize - 2; i >= 0; i--)
    {
        right[i] = right[i + 1] * nums[i + 1];
    }
    for (int i = 0; i < numsSize; i++)
    {
        answer[i] = left[i] * right[i];
    }
    return answer;
}

//leetcode728. 自除数
int* selfDividingNumbers(int left, int right, int* returnSize) {
    int* ret = (int*)malloc(sizeof(int) * ((right - left) + 1));
    int k = 0;
    for (int i = left; i <= right; i++)
    {
        int tmp = i;
        int sum = 0;
        int n = 1;
        while ((tmp % 10) != 0 && (i % (tmp % 10)) == 0)
        {
            sum += (tmp % 10) * n;
            n *= 10;
            tmp /= 10;
        }
        if (sum == i)
        {
            ret[k++] = i;
        }
    }
    *returnSize = k;
    return ret;
}

//int main()
//{
//	short int a = 65537;
//	printf("%d\n", a);
//	return 0;
//}

//int main()
//{
//    int arr[10] = { 0 };
//    int ret = (int)(arr + 1);
//    return 0;
//}
//int func(int x)
//{
//	int count = 0;
//	while (x)
//	{
//		count++;
//		x = x & (x - 1);//与运算
//	}
//	return count;
//}
//int main()
//{
//	int count = 0;
//	int x = -1;
//	while (x)
//	{
//		count++;
//		x = x >> 1;
//	}
//	printf("%d", count);
//	return 0;
//}

//int main()
//{
//	int arr[2][3] = { 1,1,1,2,2,2 };
//	int ret[3][2] = { 0 };
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			ret[j][i] = arr[i][j];
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			printf("%d ", ret[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//1 1 1
//2 2 2 
//3 3 3
//int main()
//{
//	int arr[3][3] = { 1,1,1,2,2,2 ,3,3,3};
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = i; j < 3; j++)
//		{
//			int tmp = arr[i][j];
//			arr[i][j] = arr[j][i];
//			arr[j][i] = tmp;
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			printf("%d ",arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int arr[3][4] = { 5,3,6,12,54,76,1,5,-1,343,66,77 };
//	int max = arr[0][0];
//	int x = 0;
//	int y = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 4; j++)
//		{
//			if (max < arr[i][j])
//			{
//				max = arr[i][j];
//				x = i;
//				y = j;
//			}
//		}
//	}
//	printf("%d %d %d\n", max, x, y);
//	return 0;
//}

//int main()
//{
//	char str[80] = { 0 };
//	gets(str);
//	int i = 0;
//	int count = 0;
//	int flag = 1;
//	while (str[i])
//	{
//		//fsf  fsfa
//		//是空格说明新单词未开始
//		if (str[i] == ' ')
//		{
//			flag = 1;
//		}
//		//是单词的起始位置
//		else if (flag == 1)
//		{
//			flag = 0;
//			count++;
//		}
//		i++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int arr[101] = { 0 ,0};
//	for (int i = 2; i <= 100; i++)
//	{
//		arr[i] = i;
//	}
//	for (int i = 2; i < 100; i++)
//	{
//		for (int j = i+1; j <=100; j++)
//		{
//			if (arr[j] % i == 0)
//			{
//				arr[j] = 0;
//			}
//		}
//	}
//	for (int i = 1; i <= 100; i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d ", arr[i]);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int arr[3][3] = { 1,2,10,4,5,6,7,8,9 };
//	int sum = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			if (i+j == 2)
//			{
//				sum += arr[i][j];
//			}
//		}
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//int main()
//{
//	int arr[11] = { 11,23,34,56,78,88,89,90,91,100 };
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	for (i = 9; i >= 0; i--)
//	{
//		if (arr[i] > n)
//		{
//			arr[i + 1] = arr[i];
//		}
//		else 
//		{
//			break;
//		}
//	}
//	arr[i + 1] = n;
//	for (int i = 0; i < 11; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr[] = { 11,23,34,56,78,88,89,90,91,100 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10][10] = { 0 };
//	for (int i = 0; i < 10; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			if (j == 0 || i == j)
//			{
//				arr[i][j] = 1;
//			}
//			else
//			{
//				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//			}
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		for (int j = 0; j <=i; j++)
//		{
//			printf("%d  ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int func(int i, int j)
//{
//	if (i == j || j == 0)
//	{
//		return 1;
//	}
//	return func(i - 1, j) + func(i - 1, j - 1);
//}
//
//int main()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			int ret =func(i, j);
//			printf("%d  ", ret);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int arr[50][50] = { 0 };
//	int n = 0;
//	int row = 0;
//	int col = 0;
//	int prer = 0;
//	int prec = 0;
//	while (1)
//	{
//		scanf("%d", &n);
//		if (n >= 3 && n <= 100 && n % 2 != 0)
//		{
//			break;
//		}
//		else
//		{
//			printf("输入错误,请重新输入\n");
//		}
//	}
//	col = n / 2;
//	arr[row][col] = 1;;
//	for (int i = 2; i <= n * n; i++)
//	{
//		row--;
//		col++;
//		if (row < 0)
//		{
//			row = n - 1;
//		}
//		if (col >= n)
//		{
//			col = 0;
//		}
//		if (arr[row][col] != 0)
//		{
//			row = prer + 1;
//			col = prec;
//		}
//		arr[row][col] = i;
//		prer = row;
//		prec = col;
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j <n; j++)
//		{
//			printf("%5d", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}