#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
void operation(int(*p)[5], int row, int col)
{
	//一、找最大值
	//为了不记录最大值的下标，此处使用指针更加方便
	int* mid =&p[row / 2][col/2];
	int* max = &p[0][0];
	//1.找最大值
	for (int i = 0;  i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			if (*max < p[i][j])
			{
				max = &p[i][j];
			}
		}
	}
	//2.找到最大值，交换
	int tmp = *mid;
	*mid = *max;
	*max = tmp;
	//二、找四个最小值
	//1.记录由四个角的位置
	int* corner[4] = { &p[0][0], &p[0][col - 1],&p[row-1][0], &p[row-1][col-1] };
	//2.遍历数组，寻找最小值
	for (int k = 0; k < 4; k++)//需要寻找四次
	{
		int* min = mid; //每次都要将最小值初始化为数组的最大值
		for (int n = 0; n < row; n++)
		{
			for (int m = 0; m < col; m++)
			{
				//3.判断该位置是否是角落位置
				int t = 0;
				for (t = 0; t < k; t++)
				{
					//找第0个最小数的时候，k=0,意味着没有角落被交换
					if (&p[n][m] == corner[t])
					{
						break;
					}
				}
				if (t != k)  //说明是break出来的，该位置已经被交换过了，不需要交换了
				{
					continue;//
				}
				if (*min > p[n][m])
				{
					min = &p[n][m];
				}
			}
		}
		int tmp = *corner[k];
		*corner[k] = *min;
		*min =tmp;
	}
}
int main()
{
	int arr[5][5] =
	{
		{1,2,3,4,5},
		{6,7,8,9,10},
		{11,12,13,14,15},
		{16,17,18,19,20},
		{21,22,23,24,25},
	};
	operation(arr,5,5);
	int i = 0; 
	for (i = 0; i < 5; i++)
	{
		int j = 0;
		for (j = 0; j < 5; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}

//void move1(int* parr)
//{
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = i; j < 3; j++)
//		{
//			int tmp = 0;
//			tmp = *(parr + 3 * i + j);
//			*(parr + 3 * i + j) = *(parr + 3 * j + i);
//			*(parr + 3 * j + i) = tmp;
//		}
//	}
//}
//
//void move2(int (*parr)[3])
//{
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		//此处应该是j=i，否则就会交换两次，变回原型了
//		for (j = i; j < 3; j++)
//		{
//			int tmp = 0;
//			tmp = *(*(parr + i) + j);
//			*(*(parr + i) + j) = *(*(parr + j) + i);
//			*(*(parr + j) + i) = tmp;
//		}
//	}
//}
//
//int main()
//{
//	int arr[3][3] = { 1,2,3,1,2,3,1,2,3};
//	move1(&arr[0][0]);  //普通指针，注意传的是第一个元素的地址
//
//	//int(*p)[3] = arr; //数组指针
//	//move2(p);
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0; 
//		for (j = 0; j < 3; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int upc = 0;
//	int lowc = 0;
//	int digit = 0;
//	int space = 0;
//	int other = 0;
//	char str[50] = { 0 };
//	gets(str);
//	char* p = str;
//	while (*p != '\0')
//	{
//		if (*p >= 'A' && *p <= 'Z')
//		{
//			upc++;
//		}
//		else if (*p >= 'a' && *p <= 'z')
//		{
//			lowc++;
//		}
//		else if(*p >= '0' && *p <= '9')
//		{
//			digit++;
//		}
//		else if (*p == ' ')
//		{
//			space++;
//		}
//		else
//		{
//			other++;
//		}
//		p++;
//	}
//	printf("%d %d %d %d %d\n", upc, lowc, digit, space, other);
//	return 0;
//}

//void my_m_strcpy(char* str1, char* str2, int m)
//{
//	int count = 0;
//	while (count < m - 1)
//	{
//		count++;
//		str1++;
//	}
//	while (*str1 != '\0')
//	{
//		*str2 = *str1;
//		str2++;
//		str1++;
//	}
//	*str2 = '\0';
//}
//
//int main()
//{
//	char str1[20] = "hello world";
//	char str2[20] = { 0 };
//	int m = 0;
//	scanf("%d", &m);
//	my_m_strcpy(str1, str2, m);
//	printf("%s\n", str2);
//	return 0;
//}

//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char* str = "abcdef";
//	printf("%d\n", my_strlen(str));
//	return 0;
//}

//#include<stdbool.h>
//#include<float.h>
//#include<math.h>
//int main()
//{
//
//	double d = 1.0;
//	double x = 0.1;
//	//if (d - 0.9 == x)
//	if(fabs(d-0.9)- 0.1 < DBL_EPSILON)
//	{
//		printf("6666\n");
//	}
//	else
//	{
//		printf("0000\n");
//	}
//	//bool  a = true;
//	////推荐
//	//if (a)
//	//{
//	//	//.....
//	//}
//	////以下方式不推荐
//	//if (a == 1)
//	//{
//	//	//....
//	//}
//	//if (a == true)
//	//{
//	//	//....
//	//}
//	/*printf("%d\n", a);*/
//	return 0;
//}