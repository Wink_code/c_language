#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct S
{
	int i;
	char ch;
	int* pa; //使用指针
};

int main()
{
	struct S* p = (struct S*)malloc(sizeof(struct S));
	if (p == NULL)
	{
		perror("malloc1");
		return 0;
	}
	p->pa = (int*)malloc(10 * sizeof(int));
	if (p->pa == NULL)
	{
		perror("malloc2");
		return 1;
	}
	p->i = 10;
	p->ch = 'c';
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		p->pa[i] = i;
	}
	printf("%d %c ", p->i, p->ch);
	for (i = 0; i < 10; i++)
	{
		printf("%d ", p->pa[i]);
	}
	free(p->pa);
	p->pa = NULL;
	free(p);
	p = NULL;
	return 0;
}

//struct S
//{
//	int i;
//	char ch;
//	int a[];
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S));
//	struct S* p = (struct S*)malloc(sizeof(struct S) + 10 * sizeof(int));
//	int i = 0;
//	if (NULL == p)
//	{
//		perror("malloc");
//		return 1;
//	}
//	p->i = 10;
//	p->ch = 'c';
//	for (i = 0; i < 10; i++)
//	{
//		p->a[i] = i;
//	}
//	printf("%d %c ", p->i, p->ch);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p->a[i]);
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//void GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str);
//	strcpy(str, "hello world");
//	printf(str);
//}

//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//}

//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hello");
//	printf(str);
//}

//void Test(void)
//{
//	char* str = (char*)malloc(100);
//	strcpy(str, "hello");
//	free(str);
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str);
//	}
//}

//int main()
//{
//	Test();
//	return 0;
//}
//int main()
//{
//	int* p = (int*)malloc(10);
//	free(p);
//	free(p);
//	p = NULL;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	free(p);
//	return 0;
//}

//int main()
//{
//	//1. 先申请一部分空间
//	int* p = (int*)malloc(20);
//	if (NULL == p)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//2.扩容空间
//	int* tmp = NULL;
//	tmp = (int*)realloc(p, 40);
//	if (tmp != NULL)
//	{
//		p = tmp;
//		tmp = NULL;
//	}
//	//3.使用
//	//....
//	free(p);
//	p = NULL;
//	return 0;
//}

//int main()
//{
//	int* p = (int*)calloc(10, sizeof(int));
//	if (NULL == p)
//	{
//		perror("calloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//int main()
//{
//	int* parr = (int*)malloc(40); //申请空间
//	if (NULL == parr)
//	{
//		perror("malloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(parr + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(parr + i));
//	}
//	free(parr);  //释放空间
//	parr = NULL; //指针置空
//	return 0;
//}

//int main()
//{
//	int* parr = (int*)malloc(0);
//	int i = 0;
//	if (NULL == parr)
//	{
//		perror("malloc");
//		return 1;
//	}
//	printf("%d\n", *parr);
//	return 0;
//}

//int main()
//{
//	char ch = 'a';
//	int val = 10;
//	float f_val = 12.8f;
//	int arr[20] = { 0 };
//	//....
//	return 0;
//}