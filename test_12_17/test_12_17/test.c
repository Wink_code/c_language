#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<math.h>

int main()
{
	int i = 0;
	for (i = 101; i <= 199; i+= 2)
	{
		int flag = 1;//假设该数是素数
		int j = 0;
		for (j = 2; j <= sqrt(i); j++)
		{
			if (i % j == 0)
			{
				flag = 0;
				break;  //不是素数，换下一个数
			}
		}
		if (flag == 1)
		{
			printf("%d ", i);
		}
	}
	return 0;
}

//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		int flag = 1;//假设该数是素数
//		int j = 0;
//		for (j = 2; j <=sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;  //不是素数，换下一个数
//			}
//		}
//		if (flag == 1)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		int flag = 1;//假设该数是素数
//		int j = 0;
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;  //不是素数，换下一个数
//			}
//		}
//		if (flag == 1)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		int j = 0; 
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				break;  //不是素数，换下一个数
//			}
//		}
//		if (j == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	float sum = 0;
//	float flag = 1.0;
//	for (i = 1; i <= 9; i++)
//	{
//		int tmp = i;
//		//求分母
//		for (int j = 1; j < i; j++)
//		{
//			tmp = tmp * 10 + i; 
//		}
//		printf("%d\n", tmp);
//		sum += flag / tmp;
//		flag = -flag;
//	}
//	printf("%f\n", sum);
//	return 0;
//}

//int main()
//{
//	float sum = 0;
//	int i = 0;
//	float flag = 1.0;
//	for (i = 1; i <= 100; i++)
//	{
//		sum += flag / i;
//		flag = -flag;
//	}
//	printf("%f\n", sum);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 2000; i <= 2500; i++)
//	{
//		//if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0))
//		if (!((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0)))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int func(int n)
//{
//	if (n == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return n * func(n - 1);
//	}
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int sum = 1;
//	//循环
//	for (int i = 1; i <= n; i++)
//	{
//		sum = sum * i;
//	}
//	printf("%d\n", sum);
//	//递归
//	int ret = func(n);
//	printf("%d\n", ret);
//	return 0;
//}