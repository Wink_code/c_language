#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>


int bin_search(int arr[], int left, int right, int key)
{
	int tmp = 0;
	while (left <= right)
	{
		int mid = (right + left) / 2;
		if (arr[mid] == key)
		{
			tmp = mid;
			break;
		}
		else if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
	}
	if (left > right)
	{
		return -1;
	}
	else
	{
		return tmp;
	}
}

int main()
{
	int arr[] = { 2,4,6,8,10,12,14,16,18,20 };
	int n = 0;
	scanf("%d", &n);
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 1;
	int ret = bin_search(arr, left, right, n);
	if (ret != -1)
	{
		printf("找的了，下标是：%d\n", ret);
	}
	else
	{
		printf("找不到\n");
	}
	return 0;
}


//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int i = 0;
//        for (i = 1; i <= a; i++)
//        {
//            int j = 0;
//            for (j = 1; j <= i; j++)
//            {
//                printf("%d ", j);
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < a; i++)
//        {
//            for (j = 0; j < a; j++)
//            {
//                if (j == 0 || i == j || i == a - 1)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int i = 0;
//        for (i = 0; i < a; i++)
//        {
//            int j = 0;
//            for (j = 0; j < a; j++)
//            {
//                if (i == 0 || i == a - 1)
//                {
//                    printf("* ");
//                }
//                else if (j == 0 || j == a - 1)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < a; i++)
//        {
//            for (j = 0; j < a - 1 - i; j++)
//            {
//                printf(" ");
//            }
//            printf("*");
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main() 
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < a; i++)
//        {
//            for (j = 0; j < i; j++)
//            {
//                printf(" ");
//            }
//            printf("*");
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < a; i++)
//        {
//            for (j = 0; j < 2 * a - 2 * i; j++)
//            {
//                printf(" ");
//            }
//            for (j = 0; j < i + 1; j++)
//            {
//                printf("*");
//            }
//            printf("\n");
//        }
//        for (i = 0; i < a + 1; i++)
//        {
//            for (j = 0; j < 2 * i; j++)
//            {
//                printf(" ");
//            }
//            for (j = 0; j < a + 1 - i; j++)
//            {
//                printf("*");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main()
//{
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int  i = 0;
//        int j = 0;
//        //上半部分
//        for (i = 0; i < a; i++)
//        {
//            for (j = 0; j < a + 1 - i; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//        //下半部分
//        for (i = 0; i < a + 1; i++)
//        {
//            for (j = 0; j < i + 1; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//int main() {
//    int a;
//    while (scanf("%d", &a) != EOF)
//    {
//        int  i = 0;
//        //上半部分
//        for (i = 0; i < a; i++)
//        {
//            int j = 0;
//            //打印空格
//            for (j = 0; j < a - i; j++)
//            {
//                printf(" ");
//            }
//            //打印*
//            for (j = 0; j <= i; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//        //下半部分
//        for (i = 0; i < a + 1; i++)
//        {
//            //打印空格
//            int j = 0;
//            for (j = 0; j < i; j++)
//            {
//                printf(" ");
//            }
//            //打印*
//            for (j = 0; j < a + 1 - i; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}