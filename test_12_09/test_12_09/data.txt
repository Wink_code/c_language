a b c d e f g h i j k l m n o p q r s t u v w x y z 


//3.fgetc  fputc
//int main()
//{
//	FILE* pfwrite = fopen("text.txt", "w");
//	if (pfwrite == NULL)
//	{
//		perror("fopen-w");
//		return 1;
//	}
//	for (int i = 0; i < 26; i++)
//	{
//		fputc('a' + i, pfwrite);
//		fputc(' ', pfwrite);
//	}
//	fclose(pfwrite);
//	pfwrite = NULL;
//	FILE* pfread = fopen("text.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen-r");
//		return 1;
//	}
//	char ch = 0;
//	for (int i = 0; i < 26 * 2; i++)
//	{
//		ch = fgetc(pfread);
//		printf("%c", ch);
//	}
//	fclose(pfread);
//	pfread = NULL;
//	return 0;
//}

//2.写的方式打开
//int main()
//{
//	FILE* p = fopen("text.txt", "w");
//	if (p == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//1.读的方式打开
//int main()
//{
//	FILE* p = fopen("text.txt", "r");
//	if (p == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fclose(p);
//	p = NULL;
//	return 0;
//}