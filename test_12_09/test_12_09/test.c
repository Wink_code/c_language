#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//6.fscanf、fprintf
struct s
{
	char name[20];
	int age;
	float score;
};

void write()
{
	struct s s1 = { "zhangsan", 20, 66.0f };
	FILE* pf = fopen("text.txt", "w");
	fprintf(pf, "%s %d %f", s1.name, s1.age, s1.score);
	fclose(pf);
	pf = NULL;
}
void read()
{
	struct s s2 = { 0 };
	FILE* pf = fopen("text.txt", "r");
	fscanf(pf, "%s %d %f", s2.name, &s2.age, &s2.score);
	fprintf(stdout, "%s %d %f", s2.name, s2.age, s2.score);
	fclose(pf);
	pf = NULL;

}
int main()
{
	write();
	read();
	return 0;
}

//5.fgets fputs
//int main()
//{
//	char str[20] = { 0 };
//	fgets(str, 10, stdin);
//	fputs(str, stdout);
//	//puts(str);
//	return 0;
//}

//int main()
//{
//	char ch[50] = { 0 };
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fgets(ch, 10, pf);
//	fclose(pf);
//	pf = NULL;
//	FILE* pfp = fopen("text.txt", "w");
//	if (pfp == NULL)
//	{
//		perror("fopen");
//		return 0;
//	}
//	fputs(ch, pfp);
//	fclose(pfp);
//	pfp = NULL;
//	return 0;
//}

//4.拷贝文件
//int main()
//{
//	FILE* pfread = fopen("data.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen-data-r");
//		return 0;
//	}
//	FILE* pfwrite = fopen("data_copy.txt", "w");
//	if (pfwrite == NULL)
//	{
//		fclose(pfread);
//		pfread = NULL;
//		perror("fopen_data_copy-w");
//		return 1;
//	}
//	//开始拷贝
//	char ch = 0;
//	while ((ch = fgetc(pfread)) != EOF)
//	{
//		fputc(ch, pfwrite);
//	}
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//	return 0;
//}

//3.fgetc  fputc
//int main()
//{
//	FILE* pfwrite = fopen("text.txt", "w");
//	if (pfwrite == NULL)
//	{
//		perror("fopen-w");
//		return 1;
//	}
//	for (int i = 0; i < 26; i++)
//	{
//		fputc('a' + i, pfwrite);
//		fputc(' ', pfwrite);
//	}
//	fclose(pfwrite);
//	pfwrite = NULL;
//	FILE* pfread = fopen("text.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen-r");
//		return 1;
//	}
//	char ch = 0;
//	for (int i = 0; i < 26 * 2; i++)
//	{
//		ch = fgetc(pfread);
//		printf("%c", ch);
//	}
//	fclose(pfread);
//	pfread = NULL;
//	return 0;
//}

//2.写的方式打开
//int main()
//{
//	FILE* p = fopen("text.txt", "w");
//	if (p == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//1.读的方式打开
//int main()
//{
//	FILE* p = fopen("text.txt", "r");
//	if (p == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fclose(p);
//	p = NULL;
//	return 0;
//}