﻿#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

union Un1
{
	char c[5];  //1   5
	int i;		//4
	//8
};
union Un2
{
	short c[7]; //2  14
	int i;		//4
	//16
};
int main()
{
	printf("%d\n", sizeof(union Un1));
	printf("%d\n", sizeof(union Un2));
	return 0;
}

//enum day
//{
//	Monday,		
//	Tuesday,	
//	Wednsdsday = 10,	
//	Thursday,	
//	Friday,		
//	Saturday = 20,	
//	Sunday		
//};
//int main()
//{
//	enum day d = Sunday;
//	/*printf("%d\n", Monday);
//	printf("%d\n", Tuesday);
//	printf("%d\n", Wednsdsday);
//	printf("%d\n", Thursday);
//	printf("%d\n", Friday);
//	printf("%d\n", Saturday);
//	printf("%d\n", Sunday);*/
//	return 0;
//}

//union Un
//{
//	char c;
//	int i;
//};
//int main()
//{
//	union Un un = { 0 };
//	printf("%p\n", &(un.i));
//	printf("%p\n", &(un.c));
//	printf("%p\n", &un);
//
//	un.i = 0x11223344;
//	un.c = 0x55;
//	printf("%x\n", un.i);
//	return 0;
//}

//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//int main()
//{
//	struct S sa = { 0 };
//	scanf("%d", &sa.b);//这是错误的
//
//
//
//	//正确示范
//	int b = 0;
//	scanf("%d", &b);
//	sa.b = b;
//	return 0;
//	return 0;
//}

//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//	return 0;
//}

//#include<stddef.h>
//int main()
//{
//struct S3
//{
//	char c1;
//	double d1;
//	int i;
//};
//struct S4
//{
//	char c2;
//	struct S3 s3;
//	double d2;
//};
//	printf("%d\n", sizeof(struct S4));
//	printf("%d\n", offsetof(struct S4,d2));
//	return 0;
//}

//struct
//{
//	int a;
//	char b;
//	float c;
//}x;
//struct
//{
//	int a;
//	char b;
//	float c;
//}a[20], * p;
//
//int main()
//{
//	p = &x;
//	return 0;
//}

////代码1：变量的定义
//struct Point
//{
//	int x;
//	int y;
//}p1; //声明类型的同时定义变量p1
//struct Point p2; //定义结构体变量p2
//
////代码2:初始化。
//struct Point p3 = { 10, 20 };
//struct Stu //类型声明
//{
//	char name[15];//名字
//	int age; //年龄
//};
//struct Stu s1 = { "zhangsan", 20 };//初始化
//struct Stu s2 = { .age = 20, .name = "lisi" };//指定顺序初始化
//
////代码3
//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化

//struct stu
//{
//	int age;
//	char name[20];
//	float score;
//};
//
//int main()
//{
//	//1.直接访问
//	struct stu student1 = { 22,"lisi", 90.0 };
//	printf("%d\n", student1.age);
//	printf("%s\n", student1.name);
//	printf("%f\n", student1.score);
//
//	//2.间接访问
//	struct stu student2 = { 16,"zhangsan", 60.0 };
//	struct stu* ps = &student2;
//	printf("%d\n", ps->age);
//	printf("%s\n", ps->name);
//	printf("%f\n", ps->score);
//	return 0;
//}
//
//struct Stu
//{
//	char name[20];//名字
//	int age;//年龄
//	char sex[5];//性别
//	char id[20];//学号
//};
//int main()
//{
//	//按照结构体成员的顺序初始化
//	struct Stu s = { "张三", 20, "男", "20230818001" };
//	//按照指定的顺序初始化
//	struct Stu s2 = { .age = 18, .name = "lisi", .id = "20230818002", .sex = "⼥" };
//	return 0;
//}


//int main()
//{
//	struct s
//	{
//		int s;
//		char name[10];
//	};
//	struct s a = { 1 , "zhangsan" };
//	struct s b;
//	struct s* ps = &b;
//	scanf("%d %s", &b.s, &b.name);
//	printf("%d %s\n", b.s, b.name);
//
//	scanf("%d %s", &ps->s, ps->name);
//	printf("%d %s", ps->s, ps->name);
//	/*printf("%d\n", sizeof(struct s));
//	printf("%d\n", sizeof(a));*/
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int max = 0;
//	int num = 0;
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &num);
//		int tmp = sqrt(num);
//		if ((tmp * tmp) != num && num > max)
//		{
//			max = num;
//		}
//	}
//	printf("%d\n", max);
//	return 0;
//}