#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
int* Find_max(int* arr, int count, int* index)
{
	int max = *arr;
	for (int i = 0; i < count; i++)
	{
		if (max < *(arr + i))
		{
			max = *(arr + i);
			*index = i;
		}
	}
	return arr + *index;
}

int main()
{
	int arr[10] = { 12,3,56,1,87,2,5,34,11,100 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int index = 0;
	int* ret = Find_max(arr, sz, &index);
	printf("max = %d, index = %d, 地址：%#x", arr[index], index, ret);
	return 0;
}

//int main()
//{
//	int arr1[2] = { 0 };
//	int arr2[2] = { 0 };
//	int arr3[2] = { 0 };
//	int* arr[3] = { arr1, arr2, arr3 };
//	int i = 0;
//	//输入
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 2; j++)
//		{
//			scanf("%d", (*(arr + i) + j));
//		}
//	}
//	//输出
//	for (i = 0; i < 2; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 3; j++)
//		{
//			printf("%d ", *(*(arr + j) + i));
//		}
//		printf("\n");
//	}
//	return 0;
//}

//void reverse(int* arr, int n, int m)
//{
//	int* left = arr + n - 1;
//	int* right = arr + m; //此处是几个数，不用-1
//	while (left < right)
//	{
//		int tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	reverse(arr, n, m);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//#include<string.h>
//void insert(char* str)
//{
//	int len = strlen(str);
//	for (int i = len-1; i > 0; i--)
//	{
//		*(str + 2 * i) = *(str + i);
//		*(str + 2 * i - 1) = ' ';
//	}
//}
//
//int main()
//{
//	char str[60] = "abcde";
//	insert(str);
//	puts(str);
//	return 0;
//}

//void my_strcpy(char* str1, char* str2, int m)
//{
//	char* p = str1 + m - 1;
//	while (*p != '\0')
//	{
//		*str2 = *p;
//		str2++;
//		p++;
//	}
//}
//
//int main()
//{
//	char str1[20] = { 0 };
//	char str2[20] = { 0 };
//	gets(str1);
//	int m = 0;
//	scanf("%d", &m);
//	int len = strlen(str1);
//	if (len < m)
//	{
//		printf("字符串长度不足%d\n", m);
//	}
//	else
//	{
//		my_strcpy(str1, str2, m);
//		printf("%s\n", str2);
//	}
//	return 0;
//}

//int main()
//{
//	char arr[] = "computer";
//	char* parr = arr;
//	for (; *parr != '\0'; parr += 2)
//	{
//		printf("%c", *parr);
//	}
//	return 0;
//}