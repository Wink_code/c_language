#define _CRT_SECURE_NO_WARNINGS 1

//求两个整数之和
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int sum = 0;
//	scanf("%d %d", &a, &b);
//	sum = a + b;
//	printf("sum=%d", sum);
//	return 0;
//}

////求三个数中的较大值（用函数）
//#include<stdio.h>
//int Max(int a, int b, int c)
//{
//	int max = 0;
//	max = a > b ? a : b;
//	max = max > c ? max : c;
//	return max;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int max = 0;
//	scanf("%d %d %d", &a, &b, &c);
//	max = Max(a, b, c);
//	printf("max=%d", max);
//	return 0;
//}

////求1*2*3...*n
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	int n = 0;
//	int sum = 1;
//	scanf("%d", &n);
//
//	/*for (i = 1; i <= n; i++)
//	{
//		sum = sum * i;
//	}*/
//
//	while (n)
//	{
//		sum = sum * i;
//		i++;
//		n--;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//有M个学生，输出成绩在80分以上的学生的学号和成绩,并统计人数
//#include<stdio.h>
//#define M  4
//int main()
//{
//	int arr[M][2] = { 0 };
//	int i = 0;
//	int number = 0;
//	int score = 0;
//	int count = 0;
//	for (i = 0; i < M; i++)
//	{
//		scanf("%d %d", &arr[i][0], &arr[i][1]);
//	}
//	for (i = 0; i < M; i++)
//	{
//		if (arr[i][1] > 80)
//		{
//			printf("学号：%d 分数：%d\n", arr[i][0], arr[i][1]);
//			count++;
//		}
//	}
//	printf("80分以上的共%d人\n", count);
//	return 0;
//}

//判断2000——2500的每一年是否是闰年，并将结果输出
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 2000; i <= 2500; i++)
//	{
//		if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0)) 
//		{
//			printf("%d年是闰年\n", i);
//		}
//		else
//		{
//			printf("%d年不是闰年\n", i);
//
//		}
//	}
//	return 0;
//}

//求1-1/2+1/3-1/4.....+1/99-1/100
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	//特别注意  一定要有变量是浮点型！！
//	float flag = 1;
//	float sum = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		sum += flag / i;
//		flag = -flag;
//	}
//	printf("%f\n", sum);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	printf("A\t1234\n");
//	printf("AAAAAAAAA\n");
//	return 0;
//}


//给出一个大于或等于3的正整数，判断是不是素数
#include<stdio.h>
#include<math.h>
int main()
{
	int a = 0;
	int i = 0;
//again:
	scanf("%d", &a);
	if (a >= 3) 
	{
		//注意起始点
		//for (i = 2; i < sqrt(a); i++)  //优化
		for (i = 2; i < a; i++)
		{
			if (a % i == 0)
			{
				printf("%d不是素数\n", a);
				break;
			}
		}
		//来到这里就两种情况  一.break  二.2到a-1都被除过了
		//从2到a-1都被除过了
		if (i == a)
			printf("%d是素数\n", a);
	}
	else
	{
		printf("您输入的数不符合要求，请重新输入！\n");
		//goto again;
	}
	return 0;
}