#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void func(int arr[][3],int row,int col)
{
	int* mid = &arr[row / 2][col / 2];
	int* max = &arr[0][0];
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (*max < arr[i][j])
			{
				max = &arr[i][j];
			}
		}
	}
	int tmp = *max;
	*max = *mid;
	*mid = tmp;
	int* corner[4] = { &arr[0][0],&arr[0][col - 1], &arr[row - 1][0], &arr[row - 1][col - 1] };
	for (int n = 0; n < 4; n++)
	{
		int* min = mid;//每次将数组最大值作为参照，找出数组最小值
		for (int i = 0; i < row; i++)
		{
			for (int j = 0; j < col; j++)
			{
				//将所找的最小值的地址与角落地址比较
				int k = 0;
				for (k = 0; k < n; k++)
				{
					if (&arr[i][j] == corner[k])
					{
						break;//如果该位置是角落位置，应略过该角落，找比该角落大的一个较小值
					}
				}
				//是break出来的
				if (k != n)
				{
					continue;
				}
				if (*min > arr[i][j])
				{
					min = &arr[i][j];
				}
			}
		}
		int tmp = *corner[n];
		*corner[n] = *min;
		*min = tmp;
	}
}

int main()
{
	int arr[3][3] = { {1,2,3},{4,5,6},{7,8,9} };
	func(arr,3,3);
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}

//void reverse(int* p,int num)
//{
//	int* left = p;
//	int* right = p + num - 1;
//	while (left < right)
//	{
//		int tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int arr[] = { 10,9,8,7,6,5,4,3,2 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	reverse(arr,sz);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//#define N 4
//#define M 5
//double average(double score[][M],int lesson)
//{
//	double sum = 0;
//	for (int i = 0; i < N; i++)
//	{
//		sum += score[i][lesson - 1];
//	}
//	return sum / N;
//}
//void find_student(double score[][M])
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < N; i++)
//	{
//		int count = 0;
//		for (j = 0; j < M; j++)
//		{
//			if (score[i][j] < 60)
//			{
//				count++;
//			}
//		}
//		if (count > 2)
//		{
//			printf("%d号有两门以上不及格\n", i + 1);
//		}
//	}
//}
//
//void find_good(double arr[N][M])
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < N; i++)
//	{
//		double sum = 0;
//		int count = 0;
//		for (j = 0; j < M; j++)
//		{
//			sum += arr[i][j];
//			if (arr[i][j] > 85)
//			{
//				count++;
//			}
//		}
//		if (count == 5 || ((sum / M) > 90))
//		{
//			printf("%d号学生优秀\n", i + 1);
//		}
//	}
//}
//
//int main()
//{
//	double score[N][M] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < N; i++)
//	{
//		for (j = 0; j < M; j++)
//		{
//			scanf("%lf", &score[i][j]);
//		}
//	}
//	printf("求第几科的平均成绩？\n");
//	int lesson = 0;
//	scanf("%d", &lesson);
//	double ave = average(score,lesson);
//	printf("average = %lf\n", ave);
//	find_student(score);
//	find_good(score);
//	
//	return 0;
//}

//int main()
//{
//	char* str[4] = {"NULL", "January", "February","March"};
//	int input = 0;
//	scanf("%d", &input);
//	printf("%s\n", str[input]);
//	return 0;
//}

//int main()
//{
//	putchar('\015');
//	putchar('\101');
//	return 0;
//}