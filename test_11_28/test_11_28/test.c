﻿#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>
int main()
{
	char a[20] = { 0 };
	char b[] = "ab";
	gets(a);
	puts(a);
	char max = a[0];
	int i = 0;
	int flag = 0;
	int len = strlen(a) - 1;
	while (a[i] != '\0')
	{
		if (a[i] > max)
		{
			max = a[i];
			flag = i;
		}
		i++;
	}
	int j = 0;
	for (j = len + 2; j > flag+2; j--)
	{
		a[j] = a[j - 2];
	}
	//将flag+2后面的数全都向后移了
	//移动完flag+2位置，i--了
	a[j] = b[1];
	a[j - 1] = b[0];
	puts(a);
	return 0;
}

//char* my_strcat(char* str1, char* str2,int n)
//{
//	char* ret = str1;
//	while (*str1)
//	{
//		str1++;
//	}
//	while (n-- && *str2 != '\0')
//	{
//		*str1 = *str2;
//		str1++;
//		str2++;
//	}
//	*str1 = 0;
//	return ret;
//}
//int main()
//{
//	char str1[20] = { 0 };
//	char str2[20] = { 0 };
//	gets(str1);
//	gets(str2);
//	char * ret  = my_strcat(str1, str2, 5);
//	printf("%s\n", ret);
//	return 0;
//}
 
//int main()
//{
//	char str[4][10] = { "first","second", "third", "fourth" };
//	char* strp[4] = {0};
//	for (int i = 0; i < 4; i++)
//	{
//		strp[i] = str[i];
//	}
//	printf("%s\n", *strp);
//	return 0;
//}

//int main()
//{
//	int arr[][3] = { 2,4,6,8,10,12,14,16,18};
//	int(*p)[3] = arr;
//	//printf("%d\n", **p++);
//	int b[10] = { 0 };
//	int* a = b;
//	printf("%d\n", a++);
//	/*int* p = arr;
//	printf("%x\n", p);
//	printf("%x\n", p+9);*/
//	return 0;
//}

//int main()
//{
//	int x[] = { 0,1,2,3,4,5,6,7,8,9};
//	int *p = x+2;
//	/*printf("%d\n", x[3]);
//	printf("%d\n", *p++);
//	p = p + 2;*/
//	//printf("%d\n", *(p++));
//	printf("%d\n", *++p);
//	return 0;
//}
//int main()
//{
//	char c = "0xab";
//	char a = '\'';
//	char* p = "\0127";
//	printf("%c\n", *(p));
//	return 0;
//}

//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);  //POINT
//	printf("%s\n", *-- * ++cpp + 3);//ER
//	printf("%s\n", *cpp[-2] + 3);//ST
//	printf("%s\n", cpp[-1][-1] + 1);//EW
//	return 0;
//}

//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** pa = a;
//	pa++;
//	printf("%s\n", *pa);  //at
//	return 0;
//}

//int main()
//{
//	int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* ptr1 = (int*)(&aa + 1);  
//	//&a,取出整个数组的地址加一，跳过整个数组
//	//int(*p)[2][5] = &aa;
//	int* ptr2 = (int*)(*(aa + 1));
//	//二维数组的数组名，是第一行的地址，加一就是第二行的地址，
//	//解引用找到第二行的地址,即元素6的地址,类型是 int*
//	//所以，此处的（int*）是没用的
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));  //10,5
//	//printf("\n%d\n", *(*(aa + 1) + 3));
//	return 0;
//}

////假设环境是x86环境，程序输出的结果是啥？
//#include <stdio.h>
//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);  //&p[4][2] - &a[4][2]  = -4
//	//-4当作地址打印，打印的是它的补码
//	//10000000000000000000000000000100  --原码
//	//11111111111111111111111111111011  --反码
//
//	//11111111111111111111111111111100  --补码
//	//FFFFFFFC
//	return 0;
//}

//int main()
//{
//	int a[3][2] = { (0, 1), (2, 3), (4, 5) }; //a{1,3,5,0,0,0}
//	int* p;
//	p = a[0];
//	printf("%d", p[0]);  //1
//	return 0;
//}

//在X86环境下
//假设结构体的⼤⼩是20个字节
//程序输出的结构是啥？
//struct Test
//{
//	int Num;  //0-3
//	char* pcName; //8-15
//	short sDate;  //16-17
//	char cha[2];  //18-19
//	short sBa[4];//20-23
//}*p = (struct Test*)0x100000;
//int main()
//{
//	printf("%p\n", p + 0x1);  //结构体指针加一，加的是一个结构体的大小20，16进制的20就是14， 0x100014
//	printf("%p\n", (unsigned long)p + 0x1);//将结构体类型的指针强制类型转换为（unsigned long）类型的变量+1，
//	//就是正常的加1
//	printf("%p\n", (unsigned int*)p + 0x1);//将结构体类型的指针强制类型转换为（unsigned int*）类型的变量+1,
//	//就是加整型指针变量的大小4，0x10004
//	return 0;
//}

//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);  //&a,取出整个数组的地址+1，跳过整个数组
//	printf("%d,%d", *(a + 1), *(ptr - 1)); //2，5
//	return 0;
//}