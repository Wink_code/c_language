#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>

int main()
{
	int arr[100] = { 0 };//定义一个数组，存放每个人喊得数字
	int count = 0;//几个人
	scanf("%d", &count);
	int digit = 1;//要喊得数字
	int remain = count;	//剩余人数

	while (remain > 1) //还未找出赢家
	{
		for (int i = 1; i <= count; i++)
		{
			if (*(arr+i) == 3)
			{
				continue;//当前位置是3，跳过
			}
			*(arr + i) = digit;
			if (digit == 3)//喊完3就要喊1了，又因为下面还有个++，所以赋值为0
			{
				digit = 0;
				remain--;//剩余人数-1
			}
			digit++;
		}
	}
	for (int j = 1; j <= count; j++)
	{
		if (*(arr + j) != 3)
		{
			printf("%d\n", j);
			break;
		}
	}
	return 0;
}

//void rotate(int* parr, int m, int n)
//{
//	for (int j = 0; j < m; j++)
//	{
//		int tmp = *(parr + n - 1);
//		//旋转一次
//		for (int i = n - 1; i > 0; i--)
//		{
//			*(parr + i) = *(parr + i - 1);
//		}
//		//最后一个数放在前面
//		*parr = tmp;
//	}
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int m = 3;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	rotate(arr, m, 10);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//void exchange(int* arr, int num)
//{
//	int* max = arr;
//	int* min = arr;
//	int i = 0;
//	for (i = 0; i < num; i++)
//	{
//		if (*(arr + i) >= *max)
//		{
//			max = arr + i;
//		}
//		if (*(arr + i) <= *min)
//		{
//			min = arr + i;
//		}
//	}
//	//若最大值就是首元素，为了避免最小值与首元素交换后找不到最大值
//	if (max == arr)
//	{
//		max = min;
//	}
//	int tmp = *arr;
//	*arr = *min;
//	*min = tmp;
//
//	tmp = *(arr + num - 1);
//	*(arr + num - 1) = *max;
//	*max = tmp;
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	exchange(arr, 10);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//void swap(char* s1, char* s2)
//{
//	char str[20] = { 0 };
//	strcpy(str, s1);
//	strcpy(s1, s2);
//	strcpy(s2, str);
//}
//
//int main()
//{
//	char str1[20] = { 0 };
//	char str2[20] = { 0 };
//	char str3[20] = { 0 };
//	gets(str1);
//	gets(str2);
//	gets(str3);
//	if (strcmp(str1, str2) > 0)
//		swap(str1, str2);
//	if (strcmp(str1, str3) > 0)
//		swap(str1, str3);
//	if (strcmp(str2, str3) > 0)
//		swap(str2, str3);
//	printf("%s %s %s\n", str1, str2, str3);
//	return 0;
//}

//void reverse(int* arr, int num)
//{
//	int n = num / 2;
//	int* left = arr;
//	int* right = arr + num - 1;
//	for (int i = 0; i < n; i++)
//	{
//		int tmp = 0;
//		tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int  main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	reverse(arr, 10);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//void swap(int* a, int* b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	if (a < b)
//	{
//		swap(&a, &b);
//	}
//	printf("max = %d, min = %d\n", a, b);
//	return 0;
//}