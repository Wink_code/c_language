#define _CRT_SECURE_NO_WARNINGS 1
#include<string.h>
#include<stdio.h>

int main()
{
	//二维数组的数组名是第一行的地址
	int a[3][4] = { 0 };	
	printf("%zd\n", sizeof(a));			//48	二维数组的数组名单独放在sizeof内部，计算的是整个二维数组的大小
	printf("%zd\n", sizeof(a[0][0]));	//4		第一行第一个元素
	printf("%zd\n", sizeof(a[0]));		//16	第一行的数组名，数组名单独放在sizeof内部，计算整个一维数组的大小
	printf("%zd\n", sizeof(a[0] + 1));	//4/8	第一行第二个元素的地址
	printf("%zd\n", sizeof(*(a[0] + 1)));//4	第一行第二个元素
	printf("%zd\n", sizeof(a + 1));		//4/8	第二行的地址
	printf("%zd\n", sizeof(*(a + 1)));	//16	等价于a[1]，第二行的数组名，计算第二行的大小

	printf("%zd\n", sizeof(&a[0] + 1));	//4/8		&a[0]是第一行的地址，第一行的地址+1就是第二行的地址
	//&a[0] , 就是&数组名，取出的是整个一维数组的地址，+1就是跳过整个数组，还是地址
	printf("%zd\n", sizeof(*(&a[0] + 1)));//16	第二行的地址解引用，就是第二行，计算的是第二行的大小
	printf("%zd\n", sizeof(*a));		//16	*a == *(a+0) == a[0],a是第一行的地址，*a就是第一行
	printf("%zd\n", sizeof(a[3]));		//16	
	//不会越界访问，因为sizeof中的表达式不会真实进行计算，此时表示第三行的数组名
	return 0;
}

//int main()
//{
//	char* p = "abcdef";
//	//p --->"abcdef"
//	printf("%d\n", strlen(p));		//6  a
//	printf("%d\n", strlen(p + 1));	//5  b
//	//printf("%d\n", strlen(*p));//err
//	//printf("%d\n", strlen(p[0]));//err
//	printf("%d\n", strlen(&p));//随机值	取得是指针变量p得地址
//	printf("%d\n", strlen(&p + 1));//随机值	取得是跳过一个指针变量p得地址
//	printf("%d\n", strlen(&p[0] + 1));//5	b
//	return 0;
//}

//int main()
//{
//	char* p = "abcdef";		
//	printf("%d\n", sizeof(p));		//4/8	p是字符指针，指针-》地址
//	printf("%d\n", sizeof(p + 1));	//4/8   p+1是b的地址
//	printf("%d\n", sizeof(*p));		//1		a
//	printf("%d\n", sizeof(p[0]));	//1		a  *(p+0)
//	printf("%d\n", sizeof(&p));		//4/8   指针变量p的地址
//	printf("%d\n", sizeof(&p + 1));	//4/8 
//	printf("%d\n", sizeof(&p[0] + 1));//4/8	&(*(p+0)+1)  b的地址
//	return 0;
//}

//int main()
//{
//	char arr[] = "abcdef";
//	printf("%d\n", strlen(arr));	//6
//	printf("%d\n", strlen(arr + 0));//6
//	//printf("%d\n", strlen(*arr));	//err  a, 97地址处？
//	//printf("%d\n", strlen(arr[1]));	//err	b,98地址处？
//	printf("%d\n", strlen(&arr));	//6
//	printf("%d\n", strlen(&arr + 1));//随机值
//	printf("%d\n", strlen(&arr[0] + 1));//5
//	return 0;
//}

//int main()
//{
//	//字符串末尾有\0
//	char arr[] = "abcdef";
//	printf("%d\n", sizeof(arr));	//7
//	printf("%d\n", sizeof(arr + 0));//4/8	首元素地址
//	printf("%d\n", sizeof(*arr));	//1		a
//	printf("%d\n", sizeof(arr[1]));	//1		b
//	printf("%d\n", sizeof(&arr));	//4/8	整个数组的地址
//	printf("%d\n", sizeof(&arr + 1));//4/8	跳过整个数组后的地址
//	printf("%d\n", sizeof(&arr[0] + 1));//4/8第二个元素的地址
//	return 0;
//}

//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("%d\n", strlen(arr));	//随机值
//	printf("%d\n", strlen(arr + 0));//随机值
//	//printf("%d\n", strlen(*arr));	//err  strlen的参数是地址，传过去一个a,97，访问97地址处？
//	//printf("%d\n", strlen(arr[1]));	//err
//	printf("%d\n", strlen(&arr));	//随机值
//	printf("%d\n", strlen(&arr + 1)); //随机值，跳过一个数组后的地址处
//	printf("%d\n", strlen(&arr[0] + 1)); //随机值-1
//	return 0;
//}

//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("%d\n", sizeof(arr));	//6   sizeof中单独放数组名，计算的是整个数组的大小
//	printf("%d\n", sizeof(arr + 0));//4/8 首元素的地址
//	printf("%d\n", sizeof(*arr));	//1	  a, 1
// 	printf("%d\n", sizeof(arr[1])); //1   b
//	printf("%d\n", sizeof(&arr));	//4/8
//	printf("%d\n", sizeof(&arr + 1));//4/8
//	printf("%d\n", sizeof(&arr[0] + 1));//4/8   第二个元素的地址
//	return 0;
//}

//int main()
//{
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));		//16  sizeof(数组名)，计算的是整个数组的大小  
//	printf("%d\n", sizeof(a + 0));  //4/8 数组名,数组首元素的地址，+0还是首元素的地址，是地址就是4/8字节
//	printf("%d\n", sizeof(*a));		//4   *a  ，首元素  
//	printf("%d\n", sizeof(a + 1));  //4/8 第二个元素的地址，是地址就是4/8字节
//	printf("%d\n", sizeof(a[1]));	//4
//	printf("%d\n", sizeof(&a));		//4/8 &数组名，取出的是整个数组的地址，是地址就是4/8字节
//	printf("%d\n", sizeof(*&a));	//16  *与&抵消     &a，取出的是整个数组的地址，*&a就是整个数组
//	printf("%d\n", sizeof(&a + 1));	//4/8 &数组名，取出整个数组的地址，+1就是跳过整个数组的地址，还是地址，是地址就是4/8字节
//	printf("%d\n", sizeof(&a[0]));	//4/8 首元素的地址
//	printf("%d\n", sizeof(&a[0] + 1));//4/8  第二个元素的地址
//	return 0;
//}

//int my_strcmp1(char* s, char* t)
//{
//	for (; *s == *t; s++, t++)
//	{
//		if (!*s)
//			return 0;
//	}
//	return *s - *t;
//}
//
//int my_strcmp2(char* s, char* t)
//{
//	for (; *s == *t; )
//	{
//		if (!*s)
//		{
//			return 0;
//		}
//		s++;
//		t++;
//	}
//	return *s - *t;
//}
//
//int main()
//{
//	char* str1 = "abcd";
//	char* str2 = "aba";
//	int ret = my_strcmp2(str1, str2);
//	printf("%d\n", ret);
//	return 0;
//}

//char func(char* s)
//{
//	if (*s <= 'Z' && *s >= 'A')
//	{
//		*s += 32;
//	}
//	return *s;
//}
//
//int main()
//{
//	char c[80];
//	char* p = c;
//	scanf("%s", p);
//	while (*p)
//	{
//		*p = func(p);
//		putchar(*p);
//		p++;
//	}
//	return 0;
//}

//int main()
//{
//	char s[] = "abcd\0aaaaa";
//	printf("\"%s\"\n", s);  
//	printf("111\0aaa");
//	return 0;
//}

//int main()
//{
//	char s1[] = "string";
//	char* s3 = NULL;
//	//strcpy(s1, "string2");
//	strcpy(s3, "string2");
//	return 0;
//}


//int main()
//{
//	char* format = "%s, a=%d,b=%d\n";
//	int a = 1;
//	int b = 10;
//	a += b;
//	printf(*format, "a+=b", a, b);
//	return 0;
//}

//int main()
//{
//	/*char* s = "aaa\08";
//	printf("%d\n", sizeof("\ta\017bc"));
//	printf("%s\n", s);*/
//	/*char s[6] = "abcd";
//	printf("\"%s\"", s);*/
//	/*char c = '1';
//	int a = c + '0';*/
//	/*int a = 0;
//	char c = a + '0';*/
//	char* s = "sadfa";
//	char* s1[] = {"asdf"};
//	return 0;
//}