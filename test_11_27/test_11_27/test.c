#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
	int arr1[10] = { 0 };
	int(*p1)[10] = &arr1;

	int arr[2][3] = { 1,2,3,4,5,6 };
	int(*p)[3] = arr;
	printf("%d\n", *(*(p+1) + 2));
	return 0;
}