#define _CRT_SECURE_NO_WARNINGS 1

int* masterMind(char* solution, char* guess, int* returnSize) {
    int* answer = (int*)calloc(2, sizeof(int));
    for (int i = 0; i < 4; i++)
    {
        //若二者对应相等，则猜对
        if (solution[i] == guess[i])
        {
            answer[0]++;
        }
    }
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            //若是伪猜中，则只记录一次
            if (guess[i] == solution[j])
            {
                answer[1]++;
                solution[j] = 1;//防止重复记录
                break;
            }
        }
    }
    *returnSize = 2;
    answer[1] = answer[1] - answer[0];
    return answer;
}

int* twoSum(int* numbers, int numbersLen, int target, int* returnSize) {
    int* answer = (int*)calloc(2, sizeof(int));
    for (int i = 0; i < numbersLen; i++)
    {
        //若该数字本身就大于目标数，就无需再求和了
        if (numbers[i] > target)
        {
            continue;
        }
        for (int j = i + 1; j < numbersLen; j++)
        {
            if (numbers[i] + numbers[j] == target)
            {
                answer[0] = i + 1;
                answer[1] = j + 1;
            }
        }
    }
    *returnSize = 2;
    return answer;
}