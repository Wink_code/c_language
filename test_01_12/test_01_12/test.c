#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h> 
#include<stdlib.h>
int cmp(const void* a, const void* b)
{
    return *(int*)a - *(int*)b;
}
int majorityElement(int* nums, int numsSize) {
    qsort(nums, numsSize, sizeof(int), cmp);
    return nums[numsSize / 2];
}

// int majorityElement(int* nums, int numsSize) {
//     for(int i=0; i<numsSize; i++)
//     {
//         int count = 0;
//         for(int j=0; j<numsSize; j++)
//         {
//             if(nums[i] == nums[j])
//             {
//                 count++;
//             }
//         }
//         if(count > numsSize / 2)
//         {
//             return nums[i];
//         }
//     }
//     return -1;
// }

//牛客： HJ10 字符个数统计
//#include<memory.h>
//int main()
//{
//	char str[500] = { 0 };
//	int arr[128];
//	memset(arr, -1, sizeof(int) * 128);
//	while (scanf("%s", str) != EOF)
//	{
//		int i = 0;
//		int count = 0;
//		while (str[i] != '\0')
//		{
//			arr[str[i]]++;
//			i++;
//		}
//		for (int j = 0; j < 128; j++)
//		{
//			if (arr[j] > -1)
//			{
//				count++;
//			}
//		}
//		printf("%d\n", count);
//	}
//	return 0;
//}

//int main()
//{
//    int n = 5;
//    int sum = 0;
//    int tmp = 1;
//    /*for (int i = 1; i <= n; i++)
//    {
//        tmp *= i;
//        sum += tmp;
//    }*/
//    for (int i = 1; i <= n; i++)
//    {
//        tmp = 1;
//        for (int j = 1; j <= i; j++)
//        {
//            tmp *= j;
//        }
//        sum += tmp;
//    }
//    printf("%d\n", sum);
//    return 0;
//}
//157
//int main()
//{
//    int i = 0;
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    for (i = 100; i < 1000; i++)
//    {
//        a = i / 100;
//        b = i / 10 % 10;
//        c = i % 10;
//        if (a * a * a + b * b * b + c * c * c == i)
//        {
//            printf("%d ", i);
//        }
//    }
//    return 0;
//}

//int main()
//{
//    int sum = 0;
//    for (int i = 0; i < 1000; i++)
//    {
//        sum = 0;
//        for (int j = 1; j < i; j++)
//        {
//            if (i % j == 0)
//            {
//                sum += j;
//            }
//        }
//        if (sum == i)
//        {
//            printf("%d = ", i);
//            for (int j = 1; j < i; j++)
//            {
//                if (i % j == 0)
//                {
//                    printf("%d ", j);
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

//int main()
//{
//    double sum = 0;
//    double up = 2;
//    double down = 1;
//    double tmp = 0;
//    for (int i = 1; i <= 20; i++)
//    {
//        printf("%.0lf / %.0lf \n", up, down);
//        tmp = up;
//        sum += up / down;
//        up = up + down;
//        down = tmp;
//    }
//    printf("%lf\n", sum);
//    return 0;
//}

//int main()
//{
//    int n = 9;
//    int x1 = 0;
//    int x2 = 1;
//    while (n)
//    {
//        x1 = (x2 + 1) * 2; 
//        x2 = x1;
//        n--;
//    }
//    printf("%d\n", x1);
//    return 0;
//}

//int func(int n)
//{
//    if (n == 10)
//    {
//        return 1;
//    }
//    return (func(n + 1) + 1) * 2;
//}
//
//int main()
//{
//    int n = 1;
//    int ret = func(n);
//    printf("%d\n", ret);
//    return 0;
//}

//int main()
//{
//    double sum = 100;
//    double high = sum / 2;
//    int count = 2;
//    while (count <= 10)
//    {
//        sum += high * 2;
//        high = high / 2;
//        count++;
//    }
//    printf("%lf %lf\n", sum, high);
//    return 0;
//}

//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    n = n / 2;
//    for (int i = 0; i <= n; i++)
//    {
//        for (int j = 0; j < n - i; j++)
//        {
//            printf(" ");
//        }
//        for (int k = 0; k < i*2 + 1; k++)
//        {
//            printf("*");
//        }
//        printf("\n");
//    }
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j <= i; j++)
//        {
//            printf(" ");
//        }
//        for (int k = 0; k < 2*(n-1-i)+1; k++)
//        {
//            printf("*");
//        }
//        printf("\n");
//    }
//    return 0;
//}

//int main()
//{
//    for (int a = 'X'; a <= 'Z'; a++)
//    {
//        for (int b = 'X'; b <= 'Z'; b++)
//        {
//            if (a != b)
//            {
//                for (int c = 'X'; c <= 'Z'; c++)
//                {
//                    if (a != c && b != c)
//                    {
//                        if (a != 'X' && c != 'X' && c != 'Z')
//                        {
//                            printf("a--%c b--%c c--%c\n", a, b, c);
//                        }
//                    }
//                }
//            }
//        }
//    }
//    return 0;
//}

void print(int arr[], int sz)
{
    for (int i = 0; i < sz; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void swap(int* a, int* b)
{
    int tmp = *a;
    *a = *b; 
    *b = tmp;
}

void bubble_sort(int arr[], int sz)
{
    for (int i = 0; i < sz-1; i++)
    {
        for (int j = 0; j < sz - 1 - i; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                swap(&arr[j], &arr[j + 1]);
            }
        }
    }
}

void d_bubble_sort(int arr[], int sz)
{
    int left = 0;
    int right = sz - 1;
    while (left < right)
    {
        for (int i = left; i < right; i++)
        {
            if (arr[i] > arr[i + 1])
            {
                swap(&arr[i], &arr[i + 1]);
            }
        }
        right--;
        for (int i = right; i > left; i--)
        {
            if (arr[i] < arr[i - 1])
            {
                swap(&arr[i], &arr[i - 1]);
            }
        }
        left++;
    }
}

void select_sort(int arr[], int sz)
{
    for (int i = 0; i < sz - 1; i++)
    {
        int min = i;
        for (int j = i+1; j < sz; j++)
        {
            if (arr[j] < arr[min])
            {
                min = j;
            }
        }
        if (min != i)
        {
            swap(&arr[i], &arr[min]);
        }
    }
}

void insert_sort(int arr[], int sz)
{
    for (int i = 1; i < sz; i++)
    {
        int tmp = arr[i];
        int j = i - 1;
        while (j >= 0 && tmp > arr[j])
        {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = tmp;
    }
}

int main2()
{
    int arr[] = { 3,6,1,45,87,12,76,-1,54 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    print(arr, sz);
    //bubble_sort(arr, sz);
    //d_bubble_sort(arr, sz);
    //select_sort(arr, sz);
    insert_sort(arr, sz);
    print(arr, sz);

}