#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main()
{
    char str[101];
    scanf("%s", str);
    int k = 0;
    scanf("%d", &k);
    str[k] = '\0';
    printf("%s\n", str);
    return 0;
}


int main()
{
    long n = 0;
    long k = 0;
    scanf("%d %d", &n, &k);
    long i = 0;
    long count = 0;
    if (k == 0)
        count = n * n;
    else
    {
        for (i = k + 1; i <= n; i++)
        {
            long help = n % i < k ? 0 : (n % i) - k + 1;
            count += (i - k) * (n / i) + help;
        }

    }
    printf("%ld\n", count);
    return 0;
}