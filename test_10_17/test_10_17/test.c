#define _CRT_SECURE_NO_WARNINGS 1


//#include<stdio.h>
//int main()
//{
//	//int m = 5;
//	//int y = 2;
//	//m *= y;
//	//y -= m;
//	//y += y;
//	////printf("%d\n", y += y -= m *= y);
//	//return 0;
//	/*int x1 = 0xabc;
//	int x2 = 0xdef;
//	x2 -= x1;
//	printf("%X\n", x2);*/
//	/*int a = -2;
//	printf("%8lx\n", a);*/
//	//long y = -43456;
//	/*printf("y=%-8ld\n", y);
//	printf("y=%-08ld", y);
//	printf("---------\n");
//	printf("y=%012ld\n", y);
//	printf("y=%+8ld\n", y);*/
//	//printf("%#8x\n", 2456);
//}
//#include <stdio.h>
//
//int main()
//{
//    int radiu = 0;
//    float v = 0.0;
//    scanf("%d", &radiu);
//    v = (4.0 / 3) * 3.14 * radiu * radiu * radiu;
//    printf("%.2f", v);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int hour = 0;
//    int minute = 0;
//    int k = 0;
//    int set_h = 0;
//    int set_m = 0;
//    scanf("%d:%d %d", &hour, &minute, &k);
//    int tmp = 0;
//    //计算小时
//    set_h = hour + (k / 60);
//    //大于23个小时，进入下一天，小时从0再开始
//    if (set_h > 24)
//    {
//        set_h = set_h % 24;
//    }
//    //减去小时后，剩下多少分钟
//    tmp = k - (k / 60) * 60;
//    set_m = minute + tmp;
//    //大于60分钟，算到小时里面去
//    if (set_m > 60)
//    {
//        set_m = set_m - 60;
//        set_h++;
//    }
//    printf("%02d:%02d\n", set_h, set_m);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    int minute = 0;
//    scanf("%d", &n);
//    //前面的人数多余12
//    while (n > 12)
//    {
//        minute = minute + 4;
//        n = n - 12;
//    }
//    //少于12，乐乐上电梯
//    minute = minute + 2;
//    printf("%d", minute);
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    long long n = 0;
//    long long m = 0;
//    long long sum = 0;
//    long long total = 0;
//    scanf("%d %d", &n, &m);
//    total = n * m;//两数之积
//    while (n % m)
//    {
//        long long tmp = n % m;
//        n = m;
//        m = tmp;
//    }
//    //此时m为最大公约数
//    //最小公倍数就等于：两个数的乘积/最大公约数
//    sum = m + total / m;
//    printf("%lld\n", sum);
//}

//#include <stdio.h>

//int main()
//{
//    int n = 0;
//    int m = 0;
//    int sum = 0;
//    int total = 0;
//    scanf("%d %d", &n, &m);
//    total = n * m;//两数之积
//    while (n % m)
//    {
//        int tmp = n % m;
//        n = m;
//        m = tmp;
//    }
//    //此时m为最大公约数
//    //最小公倍数就等于：两个数的乘积/最大公约数
//    sum = m + total / m;
//    printf("%d\n", sum);
//}

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int total = 0;
//    scanf("%d %d", &n, &m);
//    total = n * m;//两数之积
//    int min = n < m ? n : m;
//    while (1) 
//    {
//        if (n % min == 0 && m % min == 0)
//        {
//            break;
//        }
//        min--;
//    }
//    //此时min为最大公约数
//    //最小公倍数就等于：两个数的乘积/最大公约数
//    printf("%d\n", min);
//    printf("%d\n", total / min);
//}


//以较大的数减较小的数，接着把所得的差与较小的数比较，并以大数减小数。
//继续这个操作，直到它们两个数相等为止。则相等的两个数就是所求的最大公约数。
//int Fun(int n, int m)
//{
//	if (n > m)
//	{
//		return Fun(m, n - m);
//	}
//	else if (n < m)
//	{
//		return Fun(n, m - n);
//	}
//	//两数相等
//	else
//		return n;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	int total = 0;
//	scanf("%d %d", &n, &m);
//	total = n * m;//两数之积
//	int ret = Fun(n, m);
//	printf("%d\n", ret);
//    printf("%d\n", total /ret);
//	return 0;
//}

//#include <stdio.h>
//#include<math.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int i = 0;
//    int sum = 0;
//    scanf("%d", &a);
//    for (i = 0; a > 0; i++)
//    {
//        //判断个位数是奇数还是偶数
//        if ((a % 10) % 2 == 0)
//        {
//            b = 0;
//        }
//        else
//        {
//            b = 1;
//        }
//        sum += b * pow(10, i);
//        //去除个位数,继续遍历
//        a /= 10;
//    }
//    printf("%d\n", sum);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    float a = 0.0;
//    float b = 0.0;
//    float c = 0.0;
//    float d = 0.0;
//    float score = 0.0;
//    scanf("%f %f %f %f", &a, &b, &c, &d);
//    score = 0.2 * a + 0.1 * b + c * 0.2 + d * 0.5;
//    printf("%.1f", score);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    int d = 0;
//    int sum = 0;
//    scanf("%d %d %d %d", &a, &b, &c, &d);
//    sum = (a + b - c) * d;
//    printf("%d\n", sum);
//    return 0;
//}
//#include <stdio.h>
//#include<math.h>
//int main()
//{
//    int x1 = 0;
//    int x2 = 0;
//    int y1 = 0;
//    int y2 = 0;
//    int sum = 0;
//    scanf("%d %d\n%d %d", &x1, &y1, &x2, &y2);
//    sum = pow((x1 - x2), 2) + pow((y1 - y2), 2);
//    printf("%d\n", sum);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    scanf("%d %d", &a, &b);
//    int sum = a + b;
//    int ret = 0;
//    if (sum >= 100)
//    {
//        if ((sum / 10) % 10 == 0)
//        {
//            ret = sum % 10;
//        }
//        else
//        {
//            ret = sum % 100;
//        }
//    }
//    else
//    {
//        ret = sum;
//    }
//    printf("%d\n", ret);
//    return 0;
//}
// 
// 
//输入10个数，从中找最大
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int max = arr[0];
//	for (i = 0; i < 10; i++) 
//	{
//		if (arr[i] > max)
//		{
//			max = arr[i];
//		}
//	}
//	printf("max=%d", max);
//	return 0;
//}

//有三个数a,b,c,按大小顺序输出
//#include<stdio.h>
//
//void swap(int* n, int* m)
//{
//	int tmp = *n;
//	*n = *m;
//	*m = tmp;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d %d %d", &a, &b, &c);
//	if (a < b)
//		swap(&a, &b);
//	if (a < c)
//		swap(&a, &c);
//	if (b < c)
//		swap(&b, &c);
//	//a最大，c最小
//	printf("%d %d %d\n", a, b, c);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	if (0 == a % 3 && 0 == a % 5)
//	{
//		printf("%d能被3和5整除\n", a);
//	}
//	else
//		printf("%d不能被3和5整除\n", a);
//	return 0;
//}

//求方程ax^2+bx+c=0的根
#include<stdio.h>
#include<math.h>
int main()
{
	double a, b, c;
	double flag = 0;
	double x1, x2;
	scanf("%lf %lf %lf", &a, &b, &c);
	flag = b * b - 4 * a * c;
	if (flag > 0)
	{
		x1 = (-b + sqrt(flag)) / (2 * a);
		x2 = (-b - sqrt(flag)) / (2 * a);
		printf("方程有两个不相等的实根：x1=%f,x2=%f\n", x1, x2);
	}
	else if (flag < 0)
	{
		printf("方程无解\n");
	}
	else
	{
		x1 = x2 = -b / (2 * a);
		printf("方程有两个不相等的实根：x1=x2=%f\n", x1);
	}
	return 0;
}