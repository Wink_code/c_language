#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
void reverse(char c[], int left, int right)
{
	while (left <= right)
	{
		char tmp = c[left];
		c[left] = c[right];
		c[right] = tmp;
		left++;
		right--;
	}
}
void Print(char arr[])
{
	int i = 0;
	for (i = 0; arr[i] != '\0'; i++)
	{
		printf("%c", arr[i]);
	}
	printf("\n");
}
int main()
{
	char arr[] = "abcdef";
	int start = 0;
	int end = strlen(arr)-1;
	Print(arr);
	reverse(arr, start, end);
	Print(arr);

	return 0;
}
//int JudgeString(char* ps1, char* ps2)
//{
//	//给定s1 =AABCD
//	//和 s2 = EAABC，返回1
//	char* tmp = ps1;
//	int flag = 1;
//	//先比较后半部分
//	while ((*ps1 != '\0') &&  (*ps2 != '\0'))
//	{
//		if(*ps1 != *ps2)
//		{
//			ps1++;
//		}
//		//二者相等了
//		else
//		{
//			ps1++;
//			ps2++;
//		}
//	}
//	//比较前半部分
//	while (*ps2 != '\0')
//	{
//		if (*tmp == *ps2)
//		{
//			tmp++;
//			ps2++;
//		}
//		else
//		{
//			flag = 0;
//			break;
//		}
//	}
//	if (flag == 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	char s1[] = "AABCD";
//	char s2[] = "BCDAA";
//	int ret = JudgeString(s1, s2);
//	if (ret)
//	{
//		printf("是旋转得到的\n");
//	}
//	else
//	{
//		printf("不是旋转得到的\n");
//	}
//	return 0;
//}



//Find(int p[3][5], int num, int* row, int* col)
//{
//	int x = *row;
//	int y = *col - 1;
//	while (x <= 4 && y>=0)
//	{
//		if (p[x][y] == num)
//		{
//			*row = x;
//			*col = y;
//			return 1;
//		}
//		else if (p[x][y] > num)
//		{
//			//p[x][y]是本列最小的数，大于目标数，列左移
//			y--;
//		}
//		else if (p[x][y] < num)
//		{
//			//p[x][y]是本行最大的数，小于目标数，行下移
//			x++;
//		}
//	}
//	return 0;
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15} };
//	//时间复杂度小于O(n)
//	int n = 0;
//	//从右上角开始找
//	int x = 0;
//	int y = 5;
//	scanf("%d", &n);
//	int ret = Find(arr, n, &x, &y);
//	if (ret)
//	{
//		printf("找到了，坐标为：%d %d\n", x, y);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}
//int main()
//{
//	for (char crime = 'A'; crime <= 'D'; crime++)
//	{
//		//3人说了真话，一人说了假话
//		if ((crime != 'A') + (crime == 'C') + (crime == 'D') + ( crime != 'D') == 3)
//		{
//			printf("凶手是%c\n", crime);
//			break;
//		}
//	}
//	return 0;
//}
//void Yh_triangle(int a[][10], int m)
//{
//	int i = 0;
//	for (i = 0; i < m; i++)
//	{
//		int j = 0;
//		for (j = 0; j <= i; j++)
//		{
//			if (j == 0 || i == j)
//			{
//				a[i][j] = 1;
//			}
//			else
//			{
//				a[i][j] = a[i - 1][j] + a[i - 1][j - 1];
//			}
//		}
//	}
//}
//void Print(int a[][10], int m)
//{
//	int i = 0; 
//	for (i = 0; i < m; i++)
//	{
//		int j = 0; 
//		for (j = 0; j <= i; j++)
//		{
//			printf("%-2d ", a[i][j]);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[10][10] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	Yh_triangle(arr, n);
//	Print(arr, n);
//	return 0;
//}

//int (*((*pfun)(int, int)))(int);
//int main()
//{
//
//	/*char ch = 0;
//	char* arr[] = { &ch};
//	char* (*parr)[1] = &arr;*/
//	return 0;
//}