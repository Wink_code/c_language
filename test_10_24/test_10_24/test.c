#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    while (n)
//    {
//        printf("%d", n % 10);
//        n = n / 10;
//    }
//    return 0;
//}

//int main()
//{
//    long int a = 0;
//    long int sum = 0;
//    int count = 0;
//    scanf("%ld", &a);
//    while (a)
//    {
//        sum += (a % 6) * pow(10, count);
//        a = a / 6;
//        count++;
//    }
//    printf("%ld", sum);
//    return 0;
//}

int main()
{
    int k = 0;  //发放金币天数
    scanf("%d", &k);
    int sum = 0;   //金币总数
    int count = 0;
    int i = 0;
    for (i = 1; ; i++)
    {
        int j = 0;
        for (j = 1; j <= i; j++)
        {
            sum += i;
            count++;
            //如果已经加过了k个i了,结束，输出sum
            if (count == k)
            {
                goto end;
            }
        }
    }
end:
    printf("%d\n", sum);
    return 0;
}