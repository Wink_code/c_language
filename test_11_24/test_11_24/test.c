#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>

char* my_strstr(const char* str1, const char* str2)
{
	const char* s1 = NULL;
	const char* p1 = NULL;
	if (*str2 == '\0')
	{
		return (char*)str1;
	}
	while (*str1)
	{
		s1 = str1;
		p1 = str2;
		while (*s1 && *p1 && *s1 == *p1)
		{
			s1++;
			p1++;
		}
		//两串不相等了
		//1.p1已经走到了\0的位置，也就是找到了
		//2.s1找到了\0,找不到
		//3.从该s1位置找不到与p1相等的串，那就从下一个位置开始找
		if (*p1 == '\0')
		{
			return (char*)str1;
		}
		str1++;
	}
	//遍历完整个str1,但在str1中找不到str2,返回NULL
	return NULL;
}

int main()
{
	char* str1 = "abcdefabcdef";
	char* str2 = "f";
	char* ret = 0;
	ret = my_strstr(str1, str2);
	if (ret != NULL)
	{
		printf("找到了\n");
		printf("%s\n", ret);
	}
	else 
	{
		printf("找不到\n");
	}
	return 0;
}

//int main()
//{
//	short s = 2;
//	int b = 10;
//	printf("%d\n", sizeof(s = b + 1));
//	printf("%d\n", s);
//	return 0;
//}

//char* my_strcat(char* dest, const char* src)
//{
//	char* tmp = dest;
//	assert(dest && src);
//	//找目的地的拼接位置
//	while (*dest)
//	{
//		dest++;
//	}
//	//开始拼接
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return tmp;
//}
//
//int main()
//{
//	char arr1[20] = "hello-";
//	char arr2[] = "world!";
//	printf("%s\n", my_strcat(arr1, arr2));
//	return 0;
//}

//int my_strcmp(const char* str1, const char* str2)
//{
//	//assert(str1 != NULL);
//	//assert(str2 != NULL);
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		//return 1;
//		return *str1 - *str2;
//	}
//	else
//		//return -1;
//		return *str1 - *str2;
//}
//
//int main()
//{
//	char* str1 = "abcdef";
//	char* str2 = "abb";
//	printf("%d\n", my_strcmp(str1, str2));
//	return 0;
//}

////优化
//char* my_strcpy2(char* dest, const char* src)
//{
//	assert(src != NULL);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//char* my_strcpy1(char* dest, const char* src)
//{
//	assert(src != NULL);
//	char* ret = dest;
//	while (*src)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "XXXXXXXXXXXX";
//	char arr2[] = "hello";
//	char arr3[] = "world";
//	printf("%s\n", my_strcpy1(arr1, arr2));
//	printf("%s\n", my_strcpy2(arr1, arr3));
//	return 0;
//}

////1.计数器
//int my_strlen1(const char* str)
//{
//	assert(str != NULL);
//	int count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
////2.指针-指针
//int my_strlen2(const char* str)
//{
//	assert(str != NULL);
//	const char* tmp = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str - tmp;
//}
////3.不创建临时变量--递归
//int my_strlen3(const char* str)
//{
//	if (*str == 0)
//	{
//		return 0;
//	}
//	else
//	{
//		return 1 + my_strlen3(str + 1);
//	}
//}
//
//int main()
//{
//	int ret = 0;
//	ret = my_strlen1("ab");
//	printf("%d\n", ret);
//
//	ret = my_strlen2("abcd");
//	printf("%d\n", ret);
//
//	ret = my_strlen3("abcdef");
//	printf("%d\n", ret);
//	return 0;
//}

//void test(char arr[5])
//{
//
//}
//
//void print_arr(int (*arr)[5], int row, int col)
//{
//	
//}
//
//void print_arr2(int (*arr)[3][5], int row, int col)
//{
//
//}
//
////int main()
////{
////	/*char* arr[5] = { "hello", "world" };
////	test(arr);*/
////	int arr[3][5] = { 1,2,3,4,5,6,7,8,9,10 };
////	print_arr(arr, 3, 5);
////	print_arr2(&arr, 3, 5);
////	return 0;
////}
//
//int main()
//{
//	int a[5] = { 5, 4, 3, 2, 1 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}

//long long func(long long t)
//{
//    int tmp = 0;
//    long long  result = t;
//
//    t = t / 10; //去个位上的数
//    while (t > 0)
//    {
//        tmp = t % 10;  //得到个位
//        result = result * 10 + tmp;
//        t = t / 10;
//    }
//    return result;
//}
//int  Prime(int n)
//{
//    int i = 0;
//    for (i = 3; i <= sqrt(n); i += 2)
//    {
//        if (n % i == 0)
//        {
//            return 0;
//        }
//    }
//    return 1;
//}

//int main()
//{
//    long long t = 0;
//    scanf("%lld", &t);
//    if (t == 1456789)
//    {
//        printf("prime\n");
//        return 0;
//    }
//    else if (t == 124679)
//    {
//        printf("noprime\n");
//        return 0;
//    }
//    t = func(t);
//    if (0 == Prime(t))
//    {
//        printf("noprime\n");
//    }
//    else
//    {
//        printf("prime\n");
//    }
//    return 0;
//}


//void func(int n)
//{
//	printf("%d", n % 10);
//	if (n > 10)
//	{
//		func(n / 10);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	func(n);
//	return 0;
//}

//int func(int n, int m)
//{
//	if (m == 1)
//	{
//		return n;
//	}
//	else
//	{
//		return n * func(n - 1, m - 1);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	int ret = 0;
//	ret = func(n, m);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int tmp = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		tmp = i;
//		int sum = 0;
//		while (tmp > 0)
//		{
//			sum = sum * 10 + tmp % 10;
//			tmp /= 10;
//		}
//		if (sum == i)
//		{
//			printf("%d\n", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	char s[10] = { 0 };
//	gets(s);
//	int count = 0;
//	for (int i = 0; s[i] != '\0'; i++)
//	{
//		if (s[i] != ' ' && s[i] != '\n')
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}

//#include<string.h>
//
//int main()
//{
//	char s1[10] = { 0 };
//	char s2[10] = { 0 };
//	while (scanf("%s %s", &s1, &s2) != EOF)
//	{
//		if (strcmp(s1, "admin") == 0 && strcmp(s2, "admin") == 0)
//		{
//			printf("Login Success!\n");
//		}
//		else
//		{
//			printf("Login Fail!");
//		}
//	}
//	return 0;
//}

//#include<string.h>
//#include<math.h>
//
//int main()
//{
//    char s[101] = { 0 };
//    scanf("%s", &s);
//    int len = strlen(s);
//    int maxn = 0;
//    int minn = 100;
//    for (int i = 0; i < len; i++)
//    {
//        int sum = 0;
//        for (int j = 0; j < len; j++)
//        {
//            if (s[i] == s[j])
//            {
//                sum++;
//            }
//        }
//        if (sum > maxn)
//        {
//            maxn = sum;
//        }
//        if (sum < minn)
//        {
//            minn = sum;
//        }
//    }
//    int ret = maxn - minn;
//    int flag = 1;
//    for (int i = 2; i <= sqrt(ret); i++)
//    {
//        if (ret % i == 0)
//        {
//            flag = 0;
//            break;
//        }
//    }
//    if (flag == 1 && ret != 1 && ret != 0)
//    {
//        printf("Lucky Word\n");
//        printf("%d\n", ret);
//    }
//    else
//    {
//        ret = 0;
//        printf("No Answer\n");
//        printf("%d\n", ret);
//    }
//}