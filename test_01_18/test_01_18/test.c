#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int findMaxConsecutiveOnes(int* nums, int numsSize) {
    int max_length = 0;
    int count = 0;
    for (int i = 0; i < numsSize; i++)
    {
        //是1，记录当前个数
        if (nums[i] == 1)
        {
            count++;
        }
        //不是1
        else
        {
            //计数器归零
            count = 0;
        }
        //比较当前连续1的个数
        if (count > max_length)
        {
            max_length = count;
        }
    }
    return max_length;
}

//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int sum = 0;
//    int count = 0;
//    for (int i = 1; i < n; i++)
//    {
//        sum = 0;
//        for (int j = 1; j < i; j++)
//        {
//            if (i % j == 0)
//            {
//                sum += j;
//            }
//        }
//        if (sum == i)
//        {
//            count++;
//        }
//    }
//    printf("%d\n", count);
//    return 0;
//}

//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        //先求数字的长度
//        int len = 1;//至少是个一位数
//        int tmp = n;
//        while (tmp / 10)//位数大于2长度+1
//        {
//            len++;
//            tmp /= 10;
//        }
//        while (len)
//        {
//            printf("%c", (n % 10) + '0');
//            n /= 10;
//            len--;
//        }
//    }
//    return 0;
//}

//#include <stdio.h>
//#include<string.h>
//int main()
//{
//    //由于n是整型
//    char str[11] = { 0 };
//    gets(str);
//    int len = strlen(str) - 1;
//    for (int i = len; i >= 0; i--)
//    {
//        printf("%c", str[i]);
//    }
//    return 0;
//}

//#include <stdio.h>
//#include<ctype.h>
//int main()
//{
//    char str[100][21] = { 0 };
//    int row = 0;
//    int col = 0;
//    char ch = 0;
//    while ((ch = getchar()) != EOF)
//    {
//        if (isalpha(ch))
//        {
//            str[row][col++] = ch;
//        }
//        else
//        {
//            str[row][col] = '\0';
//            row++;
//            col = 0;
//        }
//    }
//    for (int i = row - 1; i >= 0; i--)
//    {
//        printf("%s ", str[i]);
//    }
//    return 0;
//}

//int fun(unsigned int x)
//{
//    int n = 0;
//    while (x + 1)
//    {
//        n++;
//        x = x | (x + 1);
//    } 
//    return n;
//}
//int main()
//{
//    int arr[8] = { 0 };
//    int* p = &arr[0];
//    int* b = &arr[2];
//    if (p == b)
//    {
//        printf("gg");
//    }
//    //printf("%d\n", fun(2014));
//    return 0;
//}

//#include <stdio.h>
//#include<math.h>
//int main()
//{
//    int n, m;
//    while (scanf("%d %d", &n, &m) != EOF)
//    {
//        double a = n;
//        double sum = 0;
//        for (int i = 1; i <= m; i++)
//        {
//            sum += a;
//            a = sqrt(a);
//        }
//        printf("%.2lf\n", sum);
//    }
//    return 0;
//}