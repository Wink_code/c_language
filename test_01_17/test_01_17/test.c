#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

void func(int arr[][5],int row,int col)
{
	int* max = &arr[0][0];
	int* mid = &arr[row / 2][col / 2];
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (*max < arr[i][j])
			{
				max = &arr[i][j];
			}
		}
	}
	//将最大值与中间位置交换
	int tmp = *max;
	*max = *mid;
	*mid = tmp;
	int* corner[4] = { &arr[0][0],&arr[0][col - 1], &arr[row - 1][0],&arr[row - 1][col - 1] };
	//总共寻找4次（4个较小值）
	for (int k = 0; k < 4; k++)
	{
		int* min = mid;//每次将最小值设为最大值
		for (int i = 0; i < row; i++)
		{
			for (int j = 0; j < col; j++)
			{
				//如果当前位置是角落位置，并且已经有最小值放在该位置
				int m = 0;
				for (m = 0; m < k; m++)
				{
					if (&arr[i][j] == corner[m])
					{
						break;
					}
				}
				if (m < k)//若是break出来的
				{
					continue;//说明当前位置是已经找出来的最小值，跳过该位置
				}
				//寻找最小值
				if (*min > arr[i][j])
				{
					min = &arr[i][j];
				}
			}
		}
		int tmp = *corner[k];
		*corner[k] = *min;
		*min = tmp;
	}
}

int main()
{
	int arr[5][5] = { 
		1,2,3,4,5,
		6,7,8,9,10,
		11,12,13,14,
		15,16,17,18,19,
		20,21,22,23,24,25 };
	func(arr,5,5);
	for (int i=0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}

//int main()
//{
//	int count = 0;
//	scanf("%d", &count);
//	int arr[50] = { 0 };
//	int remain = count;
//	int dig = 1;//从1开始喊
//	while (remain > 1) //剩余人数大于1
//	{
//		for (int i = 0; i < count; i++)
//		{
//			if (arr[i] == 3) 
//			{
//				continue;
//			}
//			arr[i] = dig;
//			if (dig == 3)//喊完3要喊1
//			{
//				remain--;//喊过3的淘汰
//				dig = 0;
//			}
//			dig++;
//		}
//	}
//	for (int i = 0; i < count; i++)
//	{
//		if (arr[i] != 3)
//		{
//			printf("%d\n", i);
//			break;
//		}
//	}
//	return 0;
//}

//int func(char str[], char dig[][10])
//{
//	int row = 0;
//	int col = 0;
//	for (int i = 0; str[i] != '\0'; i++)
//	{
//		if (str[i] >= '0' && str[i] <= '9')
//		{
//			while (str[i] >= '0' && str[i] <= '9')
//			{
//				dig[row][col] = str[i];
//				i++;
//				col++;
//			}
//			dig[row][col] = '\0';
//			row++;
//			col = 0;
//		}
//	}
//	return row;
//}
//
//int main()
//{
//	char str[50] = { 0 };
//	char dig[10][10] = { 0 };
//	gets(str);
//	int ret = func(str, dig);
//	for (int i = 0; i < ret; i++)
//	{
//		for (int j = 0; dig[i][j] != '\0'; j++)
//		{
//			printf("%c", dig[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//void average_lesson(int arr[4][5], int n)
//{
//	if (n <= 0 || n >5)
//	{
//		return;
//	}
//	double sum = 0;
//	for (int i = 0; i < 4; i++)
//	{
//		sum += arr[i][n - 1];
//	}
//	printf("第%d门课程的平均分是：%.1lf\n", n, sum / 4);
//}
//
//void find_bad(int arr[][5])
//{
//	for (int i = 0; i < 4; i++)
//	{
//		int count = 0;
//		for (int j = 0; j < 5; j++)
//		{
//			if (arr[i][j] < 60)
//			{
//				count++;
//			}
//		}
//		if (count > 2)
//		{
//			printf("第%d名学生有两门以上成绩不及格\n", i + 1);
//		}
//	}
//}
//
//void find_good(int arr[4][5])
//{
//	for (int i = 0; i < 4; i++)
//	{
//		double sum = 0;
//		int count = 0;
//		for (int j = 0; j < 5; j++)
//		{
//			sum += arr[i][j];
//			if (arr[i][j] >= 85)
//			{
//				count++;
//			}
//		}
//		double ave = sum / 5;
//		if (ave > 90 || count == 5)
//		{
//			printf("第%d名学生位优秀\n", i + 1);
//		}
//	}
//}
//
//int main()
//{
//	int arr[4][5] = { 0 };
//	printf("请输入每名学生的成绩：\n");
//	for (int i = 0; i < 4; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	printf("求第几门课程的平均分？\n");
//	int n = 0;
//	scanf("%d", &n);
//	average_lesson(arr, n);
//	find_bad(arr);
//	find_good(arr);
//	return 0;
//}

//#include<string.h>
//void sort(char(*ps)[20])
//{
//	char tmp[20] = { 0 };
//	for (int i = 0; i < 10-1; i++)
//	{
//		for (int j = 0; j<10-1-i; j++)
//		{
//			if (strcmp(ps[j], ps[j + 1]) > 0)
//			{
//				strcpy(tmp, ps[j]);
//				strcpy(ps[j], ps[j + 1]);
//				strcpy(ps[j + 1], tmp);
//			}
//		}
//	}
//}
//
//int main()
//{
//	char str[10][20] = { 0 };
//	for (int i = 0; i < 10; i++)
//	{
//		gets(str[i]);
//	}
//	sort(str);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%s\n", str[i]);
//
//	}
//	return 0;
//}

//void move(int* arr)
//{
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 1; j < 3; j++)
//		{
//			int tmp = 0;
//			tmp = *(arr + 3 * i + j);
//			*(arr + 3 * i + j) = *(arr + 3 * j + i);
//			*(arr + 3 * j + i) = tmp;
//		}
//	}
//}

//void move(int(*p)[3])
//{
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 1; j < 3; j++)
//		{
//			int tmp = 0;
//			tmp = *(*(p + i) + j);
//			*(*(p + i) + j) = *(*(p + j) + i);
//			*(*(p + j) + i) = tmp;
//		}
//	}
//}
//
//
//int main()
//{
//	int arr[3][3] = { 1,2,3,1,2,3,1,2,3 };
//	//move(&arr[0][0]);
//	move(arr);
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//void my_strncpy(char* str, int m, char* arr)
//{
//	char* tmp = str + (m - 1);
//	while (*tmp)
//	{
//		*arr = *tmp;
//		arr++;
//		tmp++;
//	}
//}
//
//int main()
//{
//	char* str = "123456789";
//	char arr[20] = { 0 };
//	int m = 0;
//	scanf("%d", &m);
//	my_strncpy(str, m, arr);
//	printf("%s", arr);
//	
//	return 0;
//}

//int my_strlen(char* str)
//{
//	if (*str == 0)
//	{
//		return 0;
//	}
//	return 1 + my_strlen(++str);
//}

//int my_strlen(char* str)
//{
//	char* start = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str - start;
//}

//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//int main()
//{
//	char* str = "hello";
//	int ret = my_strlen(str);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	//翻转m次
//	for (int i = 0; i < m; i++)
//	{
//		int tmp = arr[n - 1];
//		for (int j = n - 1; j > 0; j--)
//		{
//			arr[j] = arr[j - 1];
//		}
//		arr[0] = tmp;
//	}
//	for (int i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}