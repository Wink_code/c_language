#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//int main()
//{
//	int arr[10] = { 10,9,8,7,6,5,4,3,2,1 };
//	int min = 0;
//	int max = 0;
//
//	for (int i = 0; i < 10; i++)
//	{
//		if (arr[i] < arr[min])
//			min = i;
//		if (arr[i] > arr[max])
//			max = i;
//	}
//
//	int tmp = arr[0];
//	arr[0] = arr[min];
//	arr[min] = tmp;
//	if (max == 0)
//	{
//		max = min;
//	}
//	tmp = arr[9];
//	arr[9] = arr[max];
//	arr[max] = tmp;
//
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

int main()
{
	int arr[3][3] = 
		{ 15,23,1,
		  17,4,3,
		  16,12,45 };
	int col = 0;
	int flag = 0;
	for (int i = 0; i < 3; i++)
	{
		flag = 1;//假设鞍点在该行
		col = 0;

		int max = arr[i][0];
		for (int j = 1; j < 3; j++)
		{
			if (arr[i][j] > max)
			{
				max = arr[i][j];
				col = j;//记录最大数的列号
			}
		}
		//检查在该列是不是最小
		for (int j = 0; j < 3; j++)
		{
			if (arr[j][col] < max)
			{
				flag = 0;
				break;
			}
		}
		if (flag)
		{
			printf("有鞍点，%d %d\n", i, col);
			break;
		}
	}
	if (flag == 0)
	{
		printf("无鞍点\n");
	}
	return 0;
}