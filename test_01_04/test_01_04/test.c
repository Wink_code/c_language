#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<windows.h>
#include<stdlib.h>
#define N 10

typedef struct node
{
	int data;
	struct node* next;
}Node;

Node* CreatNode(int data)
{
	Node* node = (Node*)malloc(sizeof(Node));
	if (NULL == node)
	{
		perror("malloc");
		exit(1);
	}
	node->data = data;
	node->next = NULL;
	return node;
}

void InsertNode(Node* curNode ,int data)
{
	Node* node = CreatNode(data);

	node->next = curNode->next;
	curNode->next = node;
}

void DeleteNode(Node* cur)
{
	Node* tmp = cur->next;//越过头节点
	cur->next = tmp->next;
	free(tmp);
	tmp = NULL;
}


void ShowList(Node* cur)
{
	//cur = cur->next;
	while (cur)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

int main()
{
	Node* head = CreatNode(0);

	printf("头插法插入数据\n");
	for (int i = 1; i <= 10; i++)
	{
		InsertNode(head,i);
		ShowList(head);
		Sleep(1000);
	}

	printf("头删法删除数据\n");
	for (int i = 1; i <= N; i++)
	{
		DeleteNode(head);
		ShowList(head);
		Sleep(1000);
	}

	free(head);
	head = NULL;
	return 0;
}

//int func(int n, int m)
//{
//	if (n == m || m == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return func(n - 1, m) + func(n - 1, m - 1);
//	}
//}

//int main()
//{
//	int arr[10][10] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	//for (int i = 0; i < n; i++)
//	//{
//	//	for (int j = 0; j <= i; j++)
//	//	{
//	//		if (i == j || j == 0)
//	//		{
//	//			arr[i][j] = 1;
//	//		}
//	//		else
//	//		{
//	//			arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//	//		}
//	//	}
//	//}
//	//for (int i = 0; i < n; i++)
//	//{
//	//	for (int j = 0; j <= i; j++)
//	//	{
//	//		printf("%d ", arr[i][j]);
//	//	}
//	//	printf("\n");
//	//}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			arr[i][j] = func(i, j);
//		}
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			printf("%d ",arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int arr[101] = { 0,0 };
//	for (int i = 2; i <= 100; i++)
//	{
//		arr[i] = i;
//	}
//	for (int i = 2; i < 100; i++)
//	{
//		for (int j = i + 1; j <= 100; j++)
//		{
//			if (arr[j] % i == 0)
//			{
//				arr[j] = 0;
//			}
//		}
//	}
//	int count = 0;
//	for (int i = 0; i < 101; i++)
//	{
//		if (arr[i])
//		{
//			printf("%d ", arr[i]);
//			count++;
//			if (count % 5 == 0)
//			{
//				printf("\n");
//			}
//		}
//	}
//	return 0;
//}

//int func(int n, int m)
//{
//	if (m == 0)
//	{
//		return n;
//	}
//	else
//	{
//		return func(m, n % m);
//	}
//}
//
//int func2(int n, int m)
//{
//	if (n < m)
//	{
//		return func2(n, m - n);
//	}
//	else if (m < n)
//	{
//		return func2(m, n - m);
//	}
//	else
//	{
//		return m;
//	}
//}
//
//int func3(int n, int m)
//{
//	int min = n < m ? n : m;
//	while (n % min != 0 || m % min != 0)
//	{
//		min--;
//	}
//	return min;
//}
//
//int main()
//{
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	int mul = n * m;
//	//while (n % m)
//	//{
//	//	int tmp = n % m;
//	//	n = m;
//	//	m = tmp;
//	//}
//	int ret = func2(n, m);
//	printf("最大公约数：%d\n", ret);
//	printf("最小公倍数：%d\n", mul / m);
//	return 0;
//}