#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
	char* arr[13] = { NULL,"January","February","March","April","May",
		"June","July","August","Septembet","October","November","December" };
	int month = 0;
	scanf("%d", &month);
	if (month >= 1 && month <= 12)
	{
		printf("%s\n", arr[month]);
	}
	else
	{
		printf("illegal input\n");
	}
	return 0;
}

//int my_strcmp(char* p1, char* p2)
//{
//	while (*p1 == *p2)
//	{
//		if (*p1 == '\0')
//		{
//			return 0;
//		}
//		p1++;
//		p2++;
//	}
//	return *p1 - *p2;
//}
//
//int main()
//{
//	char str1[20] = { 0 };
//	char str2[20] = { 0 };
//	gets(str1);
//	gets(str2);
//	int ret = my_strcmp(str1, str2);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	char str[100] = { 0 };
//	char a[10][100] = { 0 };
//	gets(str);
//	char* ptr = str;
//	int row = 0;
//	int col = 0;
//	while (*ptr != '\0')
//	{
//		//当前字符是数字，开始读取数字字符串
//		if (*ptr >= '0' && *ptr <= '9')   
//		{
//			while (*ptr >= '0' && *ptr <= '9' && *ptr != '\0')
//			{
//				//连续存储一个数字字符串
//				a[row][col] = *ptr;
//				col++;
//				ptr++;
//			}
//			a[row][col] = '\0';
//			//来到这里，说明非数字字符或者\0
//			//1.非数字字符，开始存储下一个数字字符串
//			row++;
//			col = 0;
//			//2.若是\0,跳出循环，停止访问，以防止越界
//			if (*ptr == '\0')
//			{
//				break;
//			}
//		}
//		//不是字符串，指针后移
//		else
//		{
//			ptr++;
//		}
//	}
//	printf("%d个数字\n", row);
//	for (int i = 0; i < row; i++)
//	{
//		printf("%s\n", a[i]);
//	}
//	return 0;
//}

//void average(int arr[4][5], int row, int col, int course)
//{
//	int i = 0;
//	int j = 0;
//	int sum = 0;
//	for (i = 0; i < row; i++)
//	{
//		sum += arr[i][course - 1];
//	}
//	printf("课程序号:%d, average = %d\n", course, sum / row);
//}
//
//void find_student(int arr[4][5], int row, int col)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < row; i++)
//	{
//		int count = 0;
//		for (j = 0; j < col; j++)
//		{
//			if (arr[i][j] < 60)
//			{
//				count++;
//			}
//		}
//		if (count > 2)
//		{
//			printf("第%d名学生有两门以上不及格\n", i+1);
//		}
//	}
//	printf("\n");
//
//}
//
//void find_ave_85(int arr[4][5], int row, int col)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < row; i++)
//	{
//		int sum = 0;
//		int average = 0;
//		int count = 0;
//		for (j = 0; j < col; j++)
//		{
//			sum += arr[i][j];
//			if (arr[i][j] > 85)
//			{
//				count++;
//			}
//		}
//		average = sum / col;
//		//平均分大于90或所有全在85以上
//		if (average > 90 || count == col)
//		{
//			printf("第%d名学生优秀\n", i + 1);
//		}
//	}
//	printf("\n");
//}
//
//int main()
//{
//	int arr[4][5] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 4; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	//计算某一门课程平均分
//	average(arr, 4, 5, 1);
//	//找2门以上不及格学生
//	find_student(arr, 4, 5);
//	//找优秀学生
//	find_ave_85(arr, 4, 5);
//
//	return 0;
//}

//void reverse(int* arr, int count)
//{
//	int* left = arr;
//	int* right = arr + count - 1;
//	while (left < right)
//	{
//		int tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	reverse(arr, 10);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//void sort(char* *p, int count)
//{
//	int i = 0;
//	for (i = 0; i < count-1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < count - 1 - i; j++)
//		{
//			//由大到小排序
//			if (strcmp(*(p + j), *(p + j + 1)) < 0)
//			{
//				char* tmp = *(p + j);
//				*(p + j) = *(p + j + 1);
//				*(p + j + 1) = tmp;
//			}
//		}
//	}
//}
//int main()
//{
//	int i = 0;
//	char str[10][20] = { 0 };
//	char* arr[10] = { 0 };
//	for (i = 0; i < 10; i++)
//	{
//		//指针数组要想初始化，必须先有一个数组
//		scanf("%s", str[i]);
//		arr[i] = str[i];
//	}
//	sort(arr, 10);
//	for (i = 0; i < 10; i++)
//	{
//		printf("%s\n", arr[i]);
//	}
//	return 0;
//}