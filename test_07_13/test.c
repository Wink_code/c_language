#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>
#include<string.h>


bool CheckPermutation(char* s1, char* s2) {
    if (strlen(s1) != strlen(s2))
        return false;
    int hash1[26] = { 0 };
    int hash2[26] = { 0 };

    char* ps1 = s1;
    char* ps2 = s2;
    while (*ps1 && *ps2)
    {
        hash1[*ps1 - 'a']++;
        hash2[*ps2 - 'a']++;
        ps1++;
        ps2++;
    }

    for (int i = 0; i < 26; i++)
    {
        if (hash1[i] != hash2[i])
            return false;
    }
    return true;
}

bool canPermutePalindrome(char* s) {
    int hash[128] = { 0 };
    for (int i = 0; s[i]; i++)
    {
        hash[s[i]]++;
    }
    int n = 0;
    for (int i = 0; i < 128; i++)
    {
        if (hash[i] % 2 != 0)
        {
            n++;
            if (n > 1) //奇数个字符多于一个，就不是
                return false;
        }
    }
    return true;
}
//int main()
//{
//	//char s[3][10],(*k)[3],*p;
//	//k = s;
//	char str[sizeof("ab")];
//	printf("%d\n", sizeof(str));
//	char a[5] = { 0 };
//	char (*p1)[5] = &a;
//	char* s = "ab";
//	char* p2 = *s;
//	//p = s[0];
//	//p = k;
//	//scanf("%s", p);
//	return 0;
//}

//#include<string.h>
//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        char str[101] = { 0 };
//        scanf("%s", str);
//        int len = strlen(str);
//        int tmp = len - 2;
//        char ret[10] = { 0 };
//        if (len >= 10)
//            printf("%c%d%c\n", str[0], tmp, str[len - 1]);
//        else {
//            printf("%s\n", str);
//        }
//    }
//    return 0;
//}

#include<stdlib.h>
//char* replaceSpaces(char* s, int length) {
//    int count = 0;
//    for (int i = 0; i < length; i++)
//    {
//        if (s[i] == ' ')
//            count++;
//    }
//    char* ret = (char*)malloc(2 * count + length + 1);
//    char* ps = s;
//    char* pr = ret;
//    while (ps < s + length)
//    {
//        if (*ps == ' ')
//        {
//            char* tmp = "%20";
//            int j = 0;
//            while (tmp[j])
//            {
//                *ret++ = tmp[j++];
//            }
//        }
//        else
//        {
//            *ret = *ps;
//            ret++;
//        }
//        ps++;
//    }
//    *ret = '\0';
//    strcpy(s, pr);//常量字符串不能修改
//    free(ret);
//    return s;
//}

char* replaceSpaces(char* s, int length) {
    int count = 0;
    for (int i = 0; i < length; i++)
    {
        if (s[i] == ' ')
            count++;
    }
    char* ret = (char*)malloc(sizeof(char) * 2 * count + length + 1);
    char* ps = s;
    char* pr = ret;
    while (ps < s + length)
    {
        if (*ps == ' ')
        {
            char* tmp = "%20";
            int j = 0;
            while (tmp[j])
            {
                *ret++ = tmp[j++];
            }
        }
        else
        {
            *ret = *ps;
            ret++;
        }
        ps++;
    }
    *ret = '\0';
    return pr;
}

#include <stdio.h>
int main()
{
    //char* str[3] = { "stra", "strb", "strc" };
    //char** p = str;
    //int i = 0;
    //while (i < 3)
    //{
    //    printf("%s ", *p++);
    //    i++;
    //} 
    //printf("%s\n", replaceSpaces("Mr John Smith1111111111111111", 13));

   // printf("%s\n", strcpy("12345", "33333")); //error
    return 0;
}