#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

#define M 7
 int main() {
	/*int n;
	scanf("%d", &n);*/
	int a[7];
	int n = M;
	for (int i = 0; i < n; i++)
		scanf("%d", &a[i]);
	for (int i = 0; i < n; i++)
	{
		for (int j = i + 1; j < n;)
		{
			//如果arr[i]与后面的数相等，就覆盖它
			if (a[i] == a[j])
			{
				//每个数往前挪，并且数组长度减1
				for (int k = j; k < n; k++)
				{
					a[k] = a[k + 1];
				}
				n--;
			}
			else
			{
				j++;
			}
		}
	}
	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);
	return 0;
}
//int main()
//{
//    /*int M = 0;
//    scanf("%d", &M);*/
//    int arr[M];
//    int i = 0;
//    for (i = 0; i < M; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int count = 0;
//    for (i = 0; i < M; i++)
//    {
//        int j = 0;
//        for (j = i + 1; j < M; j++)
//        {
//            //如果有两个相等的数，则说明重复了
//            if (arr[i] == arr[j])
//            {
//                int k = 0;
//                //后者需要删除
//                for (k = j; k < M; k++)
//                {
//                    arr[k] = arr[k + 1];
//                }
//                count++; //记录删除了几个
//                j--;
//            }
//        }
//    }
//    for (i = 0; i < M - count; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

////筛选法求1-100的素数
//int main()
//{
//	int arr[101] = { 0,0 };//1不是素数，可以直接设置为0
//	int i = 0;
//	for (i = 2; i <= 100; i++)
//	{
//		arr[i] = i;
//	}
//	//用2 - n-1的数去除
//	for (i = 2; i < 100; i++)
//	{
//		int j = 0;
//		//被除数是3-100
//		for (j = i + 1; j <= 100; j++)
//		{
//			if (arr[j] % i == 0)
//			{
//				arr[j] = 0;
//			}
//		}
//	}
//	int count = 0;
//	for (i = 0; i < 101; i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d\t", arr[i]);
//			count++;
//		}
//		if (count == 10)
//		{
//			printf("\n");
//			count = 0;
//		}
//	}
//	return 0;
//}

//int main()
//{
//    int n;
//    int arr[102] = { 0,0 };
//    while (scanf("%d", &n) != EOF)
//    {
//        int i = 0;
//        //将2—n之间的数放在数组中存储
//        for (i = 2; i <= n; i++)
//        {
//            arr[i] = i;
//        }
//        //i为除数
//        for (i = 2; i <= n; i++)
//        {
//            int j = 0;
//            //arr[j]为被除数
//            for (j = i + 1; j < n; j++)
//            {
//                if (arr[j] % i == 0)
//                {
//                    arr[j] = 0;
//                }
//            }
//        }
//        int count = 0;
//        for (i = 0; i < n; i++)
//        {
//            if (arr[i] != 0)
//            {
//                printf("%d ", arr[i]);
//                count++;
//            }
//        }
//        printf("\n%d\n", n - count - 1);
//    }
//    return 0;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n];
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int num = 0;
//    scanf("%d", &num);
//    int count = 0;
//    for (i = 0; i < n; i++)
//    {
//        if (arr[i] == num)
//        {
//            count++;
//        }
//    }
//    printf("%d", count);
//    return 0;
//}


////删除数组中指定的数字
//int main()
//{
//    int arr[50];
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int del = 0;
//    int count = 0;
//    scanf("%d", &del);
//    for (i = 0; i < n; )
//    {
//        //是要删除的数
//        if (arr[i] == del)
//        {
//            int j = 0;
//            //将该数后面的数全部往前挪，并且i不能变
//            for (j = i; j < n; j++)
//            {
//                arr[j] = arr[j + 1];
//            }
//            //记录被删除了几个数
//            count++;
//        }
//        //不是要删除的数，i++
//        else
//        {
//            i++;
//        }
//    }
//    for (i = 0; i < n - count; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//int main()
//{
//    int arr[11] = { 1,2,3,4,5,6,7,8,9,10 };
//    int insert = 0;
//    scanf("%d", &insert);
//    int  i = 0;
//    //从后往前遍历
//    for (i = 9; i >= 0; i--)
//    {
//        //arr[i] > insert，arr[i]往后移
//        if (arr[i] > insert)
//        {
//            arr[i + 1] = arr[i];
//        }
//        else
//        {
//            arr[i + 1] = insert;
//            //插入后，停止遍历
//            break;
//        }
//    }
//    //如果insert是最小的，那么i就减到了-1
//    if (i < 0)
//    {
//        arr[0] = insert;
//    }
//    for (i = 0; i <= 10; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//插入
//int main()
//{
//    int arr[51] = { 0 };
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int insert = 0;
//    scanf("%d", &insert);
//    //从后往前遍历
//    for (i = n - 1; i >= 0; i--)
//    {
//        //arr[i] > insert，arr[i]往后移
//        if (arr[i] > insert)
//        {
//            arr[i + 1] = arr[i];
//        }
//        else
//        {
//            arr[i + 1] = insert;
//            //插入后，停止遍历
//            break;
//        }
//    }
//    //如果insert是最小的，那么i就减到了-1
//    if (i < 0)
//    {
//        arr[0] = insert;
//    }
//    for (i = 0; i < n + 1; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//int main()
//{
//	char str[] = "asdfg";
//	printf("%d\n", sizeof(str));
//	printf("%d\n", strlen(str));
//	/*char a = 0;
//	switch (a)
//	{
//	case 'c' + 'd':
//	case 'a':
//	case 'g':
//	}*/
//	return 0;
//}