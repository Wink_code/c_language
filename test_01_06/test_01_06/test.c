#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdarg.h>
int FindMax(int num,...)
{
	va_list arg;  //定义可以访问可变参数部分的变量，其实arg是char*类型的
	va_start(arg, num);//使用arg指向可变参数部分
	int max = va_arg(arg, int);//根据类型，获得可变参数部分的第一个参数
	for (int i = 1; i < num; i++)
	{
		int cur = va_arg(arg, int);//获取待比较元素
		if (max < cur)
		{
			max = cur;
		}
	}
	va_end(arg);//arg使用完毕，避免野指针，arg置空
	return max;
}

int main()
{
	char a = 'a';
	char b = 'b';
	char c = 'c';
	char d = 'd';
	char e = 'e';
	int ret = FindMax(5, a,b,c,d,e);
	printf("%d\n", ret);
	
	return 0;


//#define __crt_va_arg(arg, int)     (*(int*)((arg += _INTSIZEOF(int)) - _INTSIZEOF(int)))
//
//#define __crt_va_start_a(ap, v) ((void)(ap = (va_list)(&(v)) + _INTSIZEOF(v)))
}

//int Max(int a, int b)
//{
//	return a > b ? a : b;
//}

//int FindMax(int num, ...)
//{
//	va_list arg;  
//	va_start(arg, num);
//	int max = va_arg(arg, int);
//	for (int i = 1; i < num; i++)
//	{
//		int cur = va_arg(arg, int);
//		if (max < cur)
//		{
//			max = cur;
//		}
//	}
//	va_end(arg);
//	return max;
//}
