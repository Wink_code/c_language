#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main()
{
    int n = 0;
    scanf("%d", &n);
    int arr[n];
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    int ret = 0;
    for (int i = 0; i < n; i++)
    {
        ret ^= arr[i];
    }
    printf("%d\n", ret);
    return 0;
}

int findPeakElement(int* nums, int numsLen) {
    int i = 0;
    if (nums[0] > nums[1])
    {
        return 0;
    }
    if (nums[numsLen - 2] < nums[numsLen - 1])
    {
        return numsLen - 1;
    }
    for (i = 0; i < numsLen; i++)
    {
        int prev = nums[i - 1];
        int cur = nums[i];
        int next = nums[i + 1];
        if (cur > prev && cur > next)
        {
            break;
        }
    }
    return i;
}