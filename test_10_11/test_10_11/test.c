#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//int main()
//{
//	//'\b'光标回退
//	//printf("%s\n", "abc\bde\b");//abd
//	//printf("%s\n","abcde\b\b\bfghijk");//abfghijk
//
//	//printf("%s\n", "abcdefg\b\b\b");
//	//printf("%s","abcdefg\b\b\b");//ab de   有空格的原因是字符串的末尾有\0
//	return 0;
//}

//使用_Bool类型需要引头文件<stdboool.h>
#include<stdbool.h>
//int main()
//{
//	_Bool flag = true;
//	//_Bool也可以写成bool
//	bool flag2 = false;
//	if (flag2)
//	{
//		printf("666\n");
//	}
//	else
//	{
//		printf("haha\n");
//	}
//	return 0;
//}

//int main()
//{
//	printf("%d\n", sizeof(char));
//	printf("%d\n", sizeof(short));
//	printf("%d\n", sizeof(_Bool));
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(long));
//	printf("%d\n", sizeof(long long));
//	printf("%d\n", sizeof(float));
//	printf("%d\n", sizeof(double));
//	printf("%d\n", sizeof(long double));
//	printf("%d\n", sizeof(3.14));
//	printf("%d\n", sizeof(3.14f));
//
//	return 0;
//}

	
//int main()
//{
//	int a = 10;
//	char c = 0;
//	printf("%d\n", sizeof(a));
//	printf("%d\n", sizeof a);//可以省略()
//	printf("%d\n", sizeof(a + 10));//
//	printf("%d\n", sizeof(c = a + 10));//sizeof中的表达式是不计算的，当一个a放c里面时会发生截断，最终结果由C决定
//
//	return 0;
//}
//int a = 100;
// 
 

// 
//int main()
//{
//	int a = 10;
//	printf("%d", a);//10
//	return 0;
//}
void print(int arr[],int len)
{
	int i = 0;
	for (i = 0; i < len - 1; i++)
	{
		printf("%d ", arr[i]);
	}
}
void bubble_sort(int arr[], int len)
{
	int i = 0;
	int j = 0;
	//进行多少趟
	for (i = 0; i < len - 1; i++)
	{
		//每趟比较几次
		for (j = 0; j < len - 1 - i; j++)
		{
			if (arr[j] > arr[j+1])
			{
				int temp = 0;
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}
void select_sort(int arr[], int len)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < len - 1; i++)
	{
		int max = i;
		for (j = i+1; j < len - 1; j++)
		{
			if (arr[max]< arr[j])
			{
				max = j;
			}
		}
		int temp = 0;
		temp = arr[i];
		arr[i] = arr[max];
		arr[max] = temp;
	}
}
int main()
{
	int arr[] = { 1,2,64,3,7,5,8,0,6,84, };
	int len = sizeof(arr) / sizeof(arr[0]);
	int i = 0;
	int j = 0;
	print(arr,len);
	printf("\n");
	bubble_sort(arr, len);
	print(arr, len);
	printf("\n");
	select_sort(arr, len);
	print(arr, len);

	return 0;
}