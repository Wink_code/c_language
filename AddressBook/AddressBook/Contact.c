#define _CRT_SECURE_NO_WARNINGS 1
#include"Contact.h"

//静态初始化通讯录
//void InitCon(Contact* con)
//{
//	assert(con);
//	con->sz = 0;
//	memset(con, 0, sizeof(con->data));
//}

//动态初始化通讯录
void InitCon(Contact* con)
{
	assert(con);
	con->sz = 0;
	con->capacity = DEFAULT_SZ;
	con->data = (PeopleInfo*)calloc(con->capacity, sizeof(PeopleInfo));
}

//增容
void CheckCapcity(Contact* pc)
{
	assert(pc);
	if (pc->sz == pc->capacity)
	{
		PeopleInfo* tmp;
		tmp = (PeopleInfo*)realloc(pc->data, pc->capacity + ADD_SZ);
		if (tmp != NULL)
		{
			pc->data = tmp;
			printf("增容成功\n");
		}
		else
		{
			perror("CheckCapcity-realloc");
			return;
		}
	}
}

//销毁通讯录
void DestoryCon(Contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;
}

static int FindPeople(char* name, Contact* pc)
{
	for (int i = 0; i < pc->sz; i++)
	{
		if (strcmp(name, pc->data[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}

void AddContact(Contact* pc)
{
	assert(pc);
	CheckCapcity(pc);
	/*if (pc->sz == MAX)
	{
		printf("通讯录已满，无法添加\n");
		return;
	}*/
	printf("请输入姓名:");
	scanf("%s", pc->data[pc->sz].name);
	printf("请输入性别:");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入年龄:");
	scanf("%d", &pc->data[pc->sz].age);
	printf("请输入电话:");
	scanf("%s", pc->data[pc->sz].tel);
	printf("请输入地址:");
	scanf("%s", pc->data[pc->sz].addr);
	pc->sz++;
	printf("添加成功！\n");
}

void ShowContact(const Contact* pc)
{
	if (pc->sz == 0)
	{
		printf("通讯录为空\n");
		return;
	}
	printf("%-5s%-12s%-8s%-8s%-12s%-15s\n", "序号","姓名", "性别", "年龄", "电话", "地址");
	for (int i = 0; i < pc->sz; i++)
	{
		printf("%-5d%-12s%-8s%-8d%-12s%-15s\n", i+1,pc->data[i].name, pc->data[i].sex,
			pc->data[i].age, pc->data[i].tel, pc->data[i].addr);
	}
}

void DeleteContact(Contact* pc)
{
	char name[NAME_MAX] = { 0 };
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空，无法删除\n");
		return;
	}
	printf("请输入要删除人的姓名：");
	scanf("%s", name);
	int index = FindPeople(name, pc);
	if (index != -1)
	{
		for (int i = index; i < pc->sz - 1; i++)
		{
			pc->data[i] = pc->data[i + 1];
		}
		pc->sz--;
		printf("删除成功！\n");
	}
	else
	{
		printf("不存在此人\n");
	}
	
}

void SearchContact(Contact* pc)
{
	char name[NAME_MAX] = { 0 };
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空，无法查找\n");
		return;
	}
	printf("请输入要查找人的姓名\n");
	scanf("%s", name);
	int ret = FindPeople(name,pc);
	if (ret != -1)
	{
		printf("找到了\n");
		printf("%-5s%-12s%-8s%-8s%-12s%-15s\n", "序号", "姓名", "性别", "年龄", "电话", "地址");
		printf("%-5d%-12s%-8s%-8d%-12s%-15s\n", ret + 1, pc->data[ret].name, pc->data[ret].sex,
				pc->data[ret].age, pc->data[ret].tel, pc->data[ret].addr);

	}
	else
	{
		printf("查无此人\n");
	}
}

void ModifyContact(Contact* pc)
{
	char name[NAME_MAX] = { 0 };
	assert(pc);
	if (pc->sz == 0)
	{
		printf("通讯录为空，无法修改\n");
		return;
	}
	printf("请输入要修改人的姓名\n");
	scanf("%s", name);
	int ret = FindPeople(name, pc);
	if (ret != -1)
	{
		printf("请输入姓名:");
		scanf("%s", pc->data[ret].name);
		printf("请输入性别:");
		scanf("%s", pc->data[ret].sex);
		printf("请输入年龄:");
		scanf("%d", &pc->data[ret].age);
		printf("请输入电话:");
		scanf("%s", pc->data[ret].tel);
		printf("请输入地址:");
		scanf("%s", pc->data[ret].addr);
	}
	else
	{
		printf("查无此人，无法修改\n");
	}
}

static int cmp(const void *p1, const void *p2)
{
	return strcmp(((Contact*)p1)->data->name, ((Contact*)p2)->data->name);
}

void SortByName(Contact* pc)
{
	qsort(pc, pc->sz, sizeof(pc->data[0]), cmp);
	printf("排序完成\n");
	ShowContact(pc);
}

void SaveContact(Contact* pc)
{
	FILE* pf = fopen("contact.txt", "wb+");
	if (pf == NULL)
	{
		perror("fopen");
		return;
	}
	for (int i = 0; i < pc->sz; i++)
	{
		fwrite(pc->data+i, sizeof(PeopleInfo), 1, pf);
	}
	fclose(pf);
	pf = NULL;
	printf("保存成功！\n");
}