#define _CRT_SECURE_NO_WARNINGS 1

#include"Contact.h"
void menu()
{
	printf("*****************************\n");
	printf("**** 0.Exit     1.Add    ****\n");
	printf("**** 2.Delete   3.Search ****\n");
	printf("**** 4.Modify   5.Show   ****\n");
	printf("**** 6.Sort              ****\n");
	printf("**** 7.Save     8.Load   ****\n");
	printf("*****************************\n");
}

typedef enum Choice
{
	EXIT,
	ADD,
	DELETE,
	SEARCH,
	DODIFY,
	SHOW,
	SORT,
	SAVE,
	LOAD
}Choice;

int main()
{
	Contact con;
	InitCon(&con);//初始化通讯录
	int input = 0;
	do
	{
		menu();
		printf("请选择：");
		scanf("%d", &input);
		switch (input)
		{
		case EXIT:
			DestoryCon(&con);
			printf("退出通讯录");
			break;
		case ADD:
			AddContact(&con);
			break;
		case DELETE:
			DeleteContact(&con);
			break;
		case SEARCH:
			SearchContact(&con);
			break;
		case DODIFY:
			ModifyContact(&con);
			break;
		case SHOW:
			ShowContact(&con);
			break;
		case SORT:
			SortByName(&con);
			break;
		case SAVE:
			SaveContact(&con);
			break;
		case LOAD:
			break;
		default:
			printf("选择错误，请重新选择：");
			break;
		}
	} while (input);
	return 0;
}