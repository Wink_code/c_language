#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<memory.h>
#include<assert.h>
#include<string.h>
#include<stdlib.h>

#define MAX 100

#define NAME_MAX 20
#define SEX_MAX 10
#define TEL_MAX 12
#define ADDR_MAX 20

#define DEFAULT_SZ 3
#define ADD_SZ 2

//个人信息
typedef struct People
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tel[TEL_MAX];
	char addr[ADDR_MAX];
}PeopleInfo;

//静态版本、整个通讯录
//typedef struct Contact
//{
//	PeopleInfo data[MAX];
//	int sz;
//}Contact;

typedef struct Contact
{
	int sz;//当前通讯录中的联系人个数
	int capacity;//通讯录的容量
	PeopleInfo* data;
}Contact;


//初始化通讯录
void InitCon(Contact* con);
//添加信息
void AddContact(Contact* pc);
//显示信息
void ShowContact(const Contact* pc);
//删除信息
void DeleteContact(Contact* pc);
//查找
void SearchContact(Contact* pc);
//修改
void ModifyContact(Contact* pc);
//排序
void SortByName(Contact* pc);
//将通讯路存储到文件
void SaveContact(Contact* pc);
//销毁通讯录
void DestoryCon(Contact* pc);