#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//leetcode 724.寻找数组的中心下标
int pivotIndex(int* nums, int numsSize) {
    int sum = 0;
    int sum_tmp = 0;
    for (int i = 0; i < numsSize; i++)
    {
        sum += nums[i];
    }
    for (int i = 0; i < numsSize; i++)
    {
        if (2 * sum_tmp + nums[i] == sum)
        {
            return i;
        }
        sum_tmp += nums[i];
    }
    return -1;
}

//int pivotIndex(int* nums, int numsSize) {
//    int index = 0;
//    int left_sum = 0;
//    int right_sum = 0;
//    int flag = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        index = i;
//        left_sum = 0;
//        right_sum = 0;
//        //计算中心左侧数的和
//        for (int j = 0; j < index; j++)
//        {
//            left_sum += nums[j];
//        }
//        //计算中心右侧数的和
//        for (int k = index + 1; k < numsSize; k++)
//        {
//            right_sum += nums[k];
//        }
//        if (left_sum == right_sum)
//        {
//            flag = 1;
//            break;
//        }
//    }
//    if (flag == 1)
//    {
//        return index;
//    }
//    return -1;
//}

//牛客： HJ34 图片整理
//#include <stdio.h>
//#include<string.h>
//int main() {
//    char arr[1001];
//    while (scanf("%s", arr) != EOF)
//    {
//        int ret[123] = { 0 };//z的ascll值为122
//        int len = strlen(arr);
//        for (int i = 0; i < len; i++)
//        {
//            ret[arr[i]]++;//将字符对应的ascll码值的个数放入组中
//        }
//        for (int i = 48; i <= 122; i++)
//        {
//            while (ret[i] != 0)
//            {
//                printf("%c", i);
//                ret[i]--;
//            }
//        }
//    }
//    return 0;
//}

//#include <stdio.h>
//#include <string.h>
//#include<stdlib.h>
//int  cmp(const void* a, const void* b)
//{
//    return (*(char*)a) - (*(char*)b);
//}
//
//int main()
//{
//    char* str = (char*)malloc(sizeof(char) * 1000);
//    scanf("%s", str);
//    int len = strlen(str);
//    qsort(str, len, sizeof(char), cmp);
//    printf("%s\n", str);
//    return 0;
//}

//print(char* s);
//int main()
//{
//	char str[] = "Geneius";
//	print(str);
//	return 0;
//}
//print(char* s)
//{
//	if (*s)
//	{
//		print(++s);
//		printf("%c", *s);
//	}
//}

//int main()
//{
//    char ch = 0;
//    scanf("%c", &ch);
//    if (ch >= 'A' && ch <= 'Z')
//    {
//        ch += 32;
//    }
//    else if (ch >= 'a' && ch <= 'z')
//    {
//        ch -= 32;
//    }
//    printf("%c\n", ch);
//    return 0;
//}
//#include<math.h>
//int main()
//{
//    int n = 0;
//    do
//    {
//        scanf("%d", &n);
//        if (n >= 1000 || n < 0)
//        {
//            printf("请重新输入：\n");
//        }
//    } while (n >= 1000 || n < 0);
//    int ret = (int)sqrt(n);
//    printf("%d\n", ret);
//    return 0;
//}

//int main()
//{
//    double x = 0;
//    double y = 0;
//    scanf("%lf", &x);
//    if (x < 1)
//    {
//        y = x;
//    }
//    else if (x >= 1 && x < 10)
//    {
//        y = 2 * x - 1;
//    }
//    else
//    {
//        y = 3 * x - 11;
//    }
//    printf("y = %lf\n", y);
//    return 0;
//}

//int main()
//{
//    int i = 0;
//    int j = 0;
//    /*for (i=9; i>0; i--)
//    {
//        for (j = 1; j <= i; j++)
//        {
//            printf("%-2d*%-2d=%-2d ", i, j, j * i);
//        }
//        printf("\n");
//    }*/
//    /*for (i = 1; i <= 9; i++)
//    {
//        for (j = 1; j <= i; j++)
//        {
//            printf("%-2d*%-2d=%-2d ", i, j, i * j);
//        }
//        printf("\n");
//    }*/
//    /*for (i=9; i>0; i--)
//    {
//        for (int k = 0; k < 9 - i; k++)
//        {
//            printf("\t");
//        }
//        for (j = 1; j <= i; j++)
//        {
//            printf("%d*%d=%d\t", j, i, i * j);
//        }
//        printf("\n");
//    }*/
//
//    /*for (i = 1; i <= 9; i++)
//    {
//        for (j = 1; j <= 9; j++)
//        {
//            if (j < i)
//            {
//                printf("\t");
//                continue;
//            }
//            printf("%d*%d=%d\t", j, i, i * j);
//        }
//        printf("\n");
//    }*/
//    for (i = 1; i <= 9; i++)
//    {
//        for (int k = 0; k < 9 - i; k++)
//        {
//            printf("\t");
//        }
//        for (j = 1; j <= i; j++)
//        {
//            printf("%d*%d=%d\t", j, i, j * i);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//int main()
//{
//    double sum = 0;
//    int count = 0;
//    double money = 0;
//    while (sum < 100000)
//    {
//        scanf("%lf", &money);
//        sum += money;
//        count++;
//    }
//    printf("%lf %d %lf\n", sum, count, sum / count);
//    return 0;
//}

//int main()
//{
//    for (int i = 1; i <= 5; i++)
//    {
//        for (int j = 1; j <= 5; j++)
//        {
//            printf("%d ", i * j);
//        }
//        printf("\n");
//    }
//
//    /*for (int i = 100; i <= 200; i++)
//    {
//        if (i % 3 != 0)
//        {
//            printf("%d ", i);
//        }
//    }*/
//    return 0;
//}

//#include<math.h>
//int main()
//{
//    double flag = 1;
//    double sum = 0;
//    double den = 1;
//    double tmp = flag / den;
//    while (fabs(tmp) >= 1e-6)
//    {
//        sum += tmp;
//        den += 2;
//        flag = -flag;
//        tmp = flag / den;
//    }
//    printf("%lf\n", 4 * sum);
//    return 0;
//}

//int main()
//{
//    int i = 0;
//    int j = 0;
//    for (i = 100; i <= 200; i++)
//    {
//        for (j = 2; j < i; j++)
//        {
//            if (i % j == 0)
//            {
//                break;
//            }
//        }
//        if (j == i)
//        {
//            printf("%d ", i);
//        }
//    }
//    return 0;
//}

//int func(int n)
//{
//    if (n == 1 || n == 2)
//    {
//        return 1;
//    }
//    return func(n - 1) + func(n - 2);
//}
//
//int main()
//{
//    /*int arr[40] = { 0 };
//    arr[0] = 1;
//    arr[1] = 1;
//    for (int i = 2; i < 40; i++)
//    {
//        arr[i] = arr[i - 1] + arr[i - 2];
//    }
//    for (int i = 0; i < 40; i++)
//    {
//        printf("%d ", arr[i]);
//    }*/
//
//    /*int f1 = 1;
//    int f2 = 1;
//    printf("%d %d ", f1, f2);
//    for (int i = 3; i <= 40; i++)
//    {
//        int f3 = f1 + f2;
//        printf("%d ", f3);
//        f1 = f2;
//        f2 = f3;
//    }*/
//
//    /*int f1 = 1;
//    int f2 = 1;
//    for (int i = 1; i <= 20; i++)
//    {
//        printf("%d %d ", f1, f2);
//        f1 = f1 + f2;
//        f2 = f2 + f1;
//    }*/
//
//    for (int i = 1; i <= 40; i++)
//    {
//        int ret = func(i);
//        printf("%d ", ret);
//    }
//    return 0;
//}
//#include<ctype.h>
//int main()
//{
//    char arr[80] = { 0 };
//    gets(arr);
//    int i = 0;
//    int ch = 0;
//    int dig = 0;
//    int space = 0;
//    int other = 0;
//    while (arr[i] != '\0')
//    {
//        if (isalpha(arr[i]))
//        {
//            ch++;
//        }
//        else if (isdigit(arr[i]))
//        {
//            dig++;
//        }
//        else if (isspace(arr[i]))
//        {
//            space++;
//        }
//        else
//        {
//            other++;
//        }
//        i++;
//    }
//    printf("%d %d %d %d \n", ch, dig, space, other);
//    return 0;
//}

//int main()
//{
//    int a = 0;
//    int n = 0;
//    scanf("%d %d", &a, &n);
//    int sum = 0;
//    int tmp = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        tmp = a;
//        for (int j = 1; j < i; j++)
//        {
//            tmp = tmp * 10 + a;
//        }
//        printf("%d\n", tmp);
//        sum += tmp;
//    }
//    printf("%d\n", sum);
//    return 0;
//}