#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int main()
//{
//	int a = 2, b = 3, c = -4,x;
//	x = a > b && b < c && b;
//	printf("%d\n", x);
//	printf("%d\n", b);
//	return 0;
//}

//int main()
//{
//	int x = 2, y = 2, z = 0, a; 
//	a = ++x || ++y && z++; 
//	printf("%d %d %d\n", x, y, z); 
//	printf("%d\n", a);
//	return 0;
//}

//int main()
//{
//	//printf("%e\n", 12345.678);
//	//printf("%e\n", 1.123456789);
//	//printf("%e\n", 1.678);
//
//	printf("%g\n", 12345.678);
//	printf("%g\n", 1.123456789);
//	printf("%g\n", 123456789.111);
//	printf("%g\n", 1.678);
//	return 0;
//}

//int main()
//{
//	int i, j;
//	for (i = 9; i > 0; i--)
//	{
//		for (j = 9; j > i; j--)
//		{
//			printf("\t");
//		}
//		for (j = 1; j <= i; j++)
//		{
//			printf("%2d*%2d=%-2d", j, i, j * i);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	//int a, b, c;
//	//scanf("%d%d%d", &a, &b, &c);
//	//printf("%d\n", a + b + c);
//	//printf("%d %d %d\n", a, b, c);
//	//float m1 = m2 = 10.0;
//
//	//int a = b = c = 5;
//	return 0;
//}

int fun(int n, int m)
{
	if (m == 1)
	{
		return 1;
	}
	else
	{
		//当前数能整除它
		if (n % m == 0)
		{
			return m + fun(n, m - 1);//当前数+其余能整数它的数
		}
		//换下一个数
		else
		{
			return fun(n, m - 1);
		}
	}
}

void func(int n)
{
	int num = fun(n, n / 2);//从一半开始
	if (num == n)
	{
		printf("%d是完全数\n",n);
	}
}

int main()
{
	for (int i = 2; i < 1000; i++)
	{
		func(i);
	}
	return 0;
}