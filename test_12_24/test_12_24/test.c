#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

void bubble_sort(int arr[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}

void double_bubble_sort(int arr[], int n)
{
	int left = 0;
	int right = n - 1;
	while (left < right)
	{
		int flag = 1;
		//从左向右找最大
		for (int i = left; i < right; i++)
		{
			if (arr[i] > arr[i + 1])
			{
				flag = 0;
				int tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
			}
		}
		right--;
		if (flag == 1)
		{
			break;
		}
		//从右向左找最小
		for (int j = right; j > left; j--)
		{
			if (arr[j] < arr[j - 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j - 1];
				arr[j - 1] = tmp;
			}
		}
		left++;
	}
}
void select_sort(int arr[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int m = i;
		for (int j = i+1; j < n; j++)
		{
			if (arr[m] > arr[j])
			{
				m = j;
			}
		}
		if (m != i)
		{
			int tmp = arr[i];
			arr[i] = arr[m];
			arr[m] = tmp;
		}
	}
}

void print(int arr[10],int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
} 

int main()
{
	int arr[10] = { 23,12,4,67,23,8,34,65,22,10 };
	print(arr, 10);
	//bubble_sort(arr,10);
	double_bubble_sort(arr, 10);
	//select_sort(arr, 10);
	print(arr,10);
	return 0;
}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);  //输入要打印行的数量
//	int line = n / 2 + 1;
//	//打印上半部分
//	for (int i = 0; i < line; i++)
//	{
//		//打印空格
//		for (int j = 0; j <line - 1- i ; j++)
//		{
//			printf(" ");
//		}
//		//打印*
//		for (int k = 0; k < 2 * i + 1; k++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//打印下半部分
//	for (int i = 0; i < line - 1; i++)
//	{
//		//打印空格
//		for (int j = 0; j < i + 1; j++)
//		{
//			printf(" ");
//		}
//		for (int k = 0; k < 2 * (line - 1 - i) - 1; k++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//#define print printf("d:\\admin\
//\\code\
//\\test.c")
//int main()
//{
//	print;
//	return 0;
//}