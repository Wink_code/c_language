#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int func(int m, int n)
{
	if (m == n || n == 0)
	{
		return 1;
	}
	else
		return func(m - 1, n) + func(m - 1, n - 1);
}
int main()
{
	int line = 0;
	scanf("%d", &line);
	int i = 0;
	int j = 0;
	for (i = 0; i < line; i++)
	{
		for (j = 0; j < line - i; j++)
		{
			printf("  ");
		}
		for (j = 0; j <=i; j++)
		{
			printf("%4d", func(i, j));
		}
		printf("\n");
	}
	return 0;
}

//int main()
//{
//	int arr[10][10] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 10; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			
//			if (j == 0 || i == j)
//			{
//				arr[i][j] = 1;
//			}
//			else
//			{
//				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//			}
//		}
//	}
//	for (i = 0; i < 10; i++)
//	{
//		for (j = 0; j < 10 - i; j++)
//			printf("  ");
//		for (j = 0; j <= i; j++)
//		{
//			printf("%4d", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10};
//	int left = 0;
//	int right = sizeof(arr) / sizeof(arr[0]) - 1;
//	while (left < right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	int sum = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			//横、纵坐标的和等于：行数/列数减一
//			if (i+j == 3-1)
//			{
//				sum += arr[i][j];
//			}
//		}
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	int sum = 0;
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			//横、纵坐标相等则是正对角线
//			if (i == j)
//			{
//				sum += arr[i][j];
//			}
//		}
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//int main()
//{
//    int i = 0;
//    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//    for (i = 0; i <= 12; i++)
//    {
//        arr[i] = 0;
//        printf("hello bit\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int n = 5;
//	int ret = 0;
//	int sum = 0;
//	while (n)
//	{
//		ret = ret * 10 + a;
//		printf("%d ", ret);
//		sum += ret;
//		n--;
//	}
//	printf("\n%d\n", sum);
//	return 0;
//}
//#include<math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 100000; i++)
//	{
//		//计算位数
//		int tmp = i;
//		int n = 1;
//		while (tmp / 10)
//		{
//			n++;
//			tmp = tmp / 10;
//		}
//		//算每位的n次方
//		int sum = 0;
//		tmp = i;
//		while (tmp)
//		{
//			int  ret = 0;
//			ret = (int)pow(tmp % 10, n);
//			sum += ret;
//			tmp /= 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int line = 0;
//	scanf("%d", &line);
//	int i = 0;
//	int j = 0;
//	//上半部分
//	for (i = 0; i < line; i++)
//	{
//		//空格
//		for(j = 0; j < line -1 -i; j++)
//		{
//			printf(" ");
//		}
//		//*
//		for (j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//下半部分
//	for (i = 0; i < line - 1; i++)
//	{
//		//空格
//		for (j = 0; j < i+1; j++)
//		{
//			printf(" ");
//		}
//		//*
//		for (j = 0; j < 2 * (line-1-i)-1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}
//int main()
//{
//	int bottle = 20;
//	int drink = 20;
//	while (bottle >= 2)
//	{
//		drink += bottle / 2;
//		bottle = bottle / 2 + bottle % 2;
//	}
//	printf("%d\n", drink);
//	return 0;
//}
//int main()
//{
//	printf("%5.2f", 6.00);
//	printf("%5.2f", 6.00);
//	printf("%5.2f", 16.00);
//	printf("%5.2f", 6.00);
//	return 0;
//}