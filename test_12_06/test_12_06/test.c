#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int convert(int n)
{
	int sum = 0;
	int ret = 0;
	if (n)
	{
		ret = convert(n / 2) * 10 ;
		sum = ret + (n % 2);
	}
	return sum;
}

int main()
{
	int num = 0;
	scanf("%d", &num);
	int ret = convert(num);
	printf("%d\n", ret);
	return 0;
}

//void convert(int n)
//{
//	if (n)
//	{
//		convert(n / 8);
//		printf("%d", n % 8);
//	}
//}
//
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	convert(num);
//	return 0;
//}

//int convert(char* p)
//{
//    int sum = 0;
//    while (*p)
//    {
//        if (*p >= 'A' && *p <= 'f')
//        {
//            sum = sum * 16 + *p - 'A' + 10;
//        }
//        else if (*p >= 'a' && *p <= 'f')
//        {
//            sum = sum * 16 + *p - 'a' + 10;
//        }
//        else if (*p >= '0' && *p <= "9")
//        {
//            sum = sum * 16 + (*p - '0') * 16;
//        }
//        p++;
//    }
//    return sum;
//}

//int main()
//{
//    char str[10] = {0};
//    gets(str);
//    int sum = convert(str);
//    printf("%d\n", sum);
//    return 0;
//}

//#define M 2
//#define N 5
////计算每个人的平均分
//void student_ave(float arr[M][N], float ave[N])
//{
//	int i = 0;
//	for (i = 0; i < M; i++)
//	{
//		float sum = 0;
//		int j = 0;
//		for (j = 0; j < N; j++)
//		{
//			sum += arr[i][j];
//		}
//		ave[i] = sum / N;
//		printf("Num %d: average score = %.2f\n", i+1, ave[i]);
//	}
//	printf("\n");
//}
//
////计算每门课程平均分
//void lesson_ave(float arr[M][N])
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < N; i++)
//	{
//		float sum = 0;
//		for (j = 0; j < M; j++)
//		{
//			sum += arr[j][i];
//		}
//		printf("lesson%d average:%.2f\n", i + 1, sum / M);
//	}
//	printf("\n");
//}
////找出所有课程中最大的分数及其学生、课程名
//void findmax(float arr[M][N])
//{
//	float max = arr[0][0];
//	int i = 0;
//	int j = 0;
//	int student = 0;
//	int course = 0;
//	for (i = 0; i < M; i++)
//	{
//		for (j = 0; j < N; j++)
//		{
//			if (arr[i][j] > max)
//			{
//				max = arr[i][j];
//				student = i;  //记录名字
//				course = j;	//记录课程
//			}
//		}
//	}
//	printf("max = %.2f student = %d course = %d\n", max, student+1, course+1);
//}
////求平均分的方差
//void s_s(float ave[N])
//{
//	float sum_s = 0.0;
//	float sum = 0.0;
//	for (int i = 0; i < N; i++)
//	{
//		sum_s += ave[i] * ave[i];
//		sum += ave[i];
//	}
//	printf("方差是：%.2f\n", (sum_s / N) - ((sum / N) * (sum / N)));
//}
//
//int main()
//{
//	float arr[M][N] = { 0 };
//	float ave_score[N] = { 0 };
//	int i = 0;
//	for (i = 0; i < M; i++)
//	{
//		int j = 0;
//		for (j = 0; j < N; j++)
//		{
//			scanf("%f", &arr[i][j]);
//		}
//	}
//	//计算每个学生平均分
//	student_ave(arr, ave_score);
//	//lesson_ave(arr);
//	//findmax(arr);
//	s_s(ave_score);
//	return 0;
//}