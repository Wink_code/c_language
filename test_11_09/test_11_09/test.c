#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int search(char arr[1002][1002], int n, int m)
{
    int count = 0;
    for (int i = n - 1; i <= n + 1; i++)
    {
        for (int j = m - 1; j <= m + 1; j++)
        {
            if (arr[i][j] == '*')
            {
                count++;
            }
        }
    }
    return count;

}

int main()
{
    int n = 0;
    int m = 0;
    scanf("%d %d", &n, &m);
    getchar();
    char arr[1002][1002] = { 0 };
    for (int i = 1; i <= n; i++)
    {

        for (int j = 1; j <= m; j++)
        {
            scanf("%c", &arr[i][j]);
        }
        getchar();
    }
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            if (arr[i][j] != '*')
            {
                int ret = search(arr, i, j);
                arr[i][j] = ret + '0';
            }
        }
    }
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            printf("%c", arr[i][j]);
        }
        printf("\n");
    }
}
//void judge(char c[][3], int n, int m)
//{
//    if (c[n][m] == 'K')
//    {
//        printf("KiKi wins!");
//    }
//    else if (c[n][m] == 'B')
//    {
//        printf("BoBo wins!");
//    }
//    else
//    {
//        printf("No winner!");
//    }
//}
//int main()
//{
//    char c[3][3] = { 0 };
//    for (int i = 0; i < 3; i++)
//    {
//        for (int j = 0; j < 3; j++)
//        {
//            scanf("%c ", &c[i][j]);
//        }
//    }
//    //正对角线
//    if (c[0][0] == c[1][1] && c[0][0] == c[2][2])
//    {
//        judge(c, 0, 0);
//    }
//    //反对角线
//    if (c[0][2] == c[1][1] && c[0][2] == c[2][0])
//    {
//        judge(c, 0, 2);
//    }
//    //行-列
//    for (int i = 0; i < 3; i++)
//    {
//        //行
//        if (c[i][0] == c[i][1] && c[i][0] == c[i][2])
//        {
//            judge(c, i, 0);
//        }
//        //列
//        if (c[0][i] == c[1][i] && c[0][i] == c[2][i])
//        {
//            judge(c, 0, i);
//        }
//    }
//    return 0;
//}
//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int arr[n][m];
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    int k = 0;
//    scanf("%d", &k);
//    while (k)
//    {
//        getchar();
//        char t = 0;
//        int a = 0;
//        int b = 0;
//        scanf("%c %d %d", &t, &a, &b);
//        //进行行交换
//        if (t == 'r')
//        {
//            //交换几次
//
//            for (int j = 0; j < m; j++)
//            {
//                int tmp = arr[a - 1][j];
//                arr[a - 1][j] = arr[b - 1][j];
//                arr[b - 1][j] = tmp;
//            }
//        }
//        //进行列交换
//        else if (t == 'c')
//        {
//
//            for (int j = 0; j < n; j++)
//            {
//                int tmp = arr[j][a - 1];
//                arr[j][a - 1] = arr[j][b - 1];
//                arr[j][b - 1] = tmp;
//            }
//        }
//        k--;
//    }
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            printf("%d ", arr[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}