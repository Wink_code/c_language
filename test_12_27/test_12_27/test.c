#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define COMBINE(base,n)  base##e##n
int main()
{
	printf("%lf\n", COMBINE(3.14, 2));
	printf("%lf\n", COMBINE(3.14, 3));
	printf("%lf\n", COMBINE(3.14, 4));
	return 0;
}

//#define PRINT(name) printf("tha value of "#name" is %d\n", name)
//int main()
//{
//	int a = 10;
//	int b = 20;
//	PRINT(a);
//	PRINT(b);
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("the value of a is %d\n", a);
//	printf("the value of b is %d\n", b);
//	return 0;
//}

//#define MY_OFFSETOF(type,member_name)  (int)&(((type*)0)->member_name)
//struct stu
//{
//	char c1;
//	int i;
//	char c2;
//};
//int main()
//{
//	struct stu s = { 0 };
//	printf("%d\n", MY_OFFSETOF(struct stu, c1));
//	printf("%d\n", MY_OFFSETOF(struct stu, i));
//	printf("%d\n", MY_OFFSETOF(struct stu, c2));
//	return 0;
//}

//#include<stddef.h>
//struct stu
//{
//	char c1;
//	int i;
//	char c2;
//};
//int main()
//{
//	struct stu s = { 0 };
//	printf("%d\n", offsetof(struct stu, c1));
//	printf("%d\n", offsetof(struct stu, i));
//	printf("%d\n", offsetof(struct stu, c2));
//	return 0;
//}

//#define N 5
//int main()
//{
//#if !defined N
//	printf("未定义N\n");
//#else
//	printf("定义了N\n");
//#endif
//	return 0;
//}

//int main()
//{
//#if N<5
//	printf("N<5\n");
//#elif N<7
//	printf("N<7\n");
//#elif N<9
//	printf("N<9\n");
//#elif N<10
//	printf("N<10\n");
//#else
//	printf("N>=10\n");
//#endif
//	return 0;
//}

//#define N 7
//int main()
//{
//#if N<2
//	printf("n\n");
//#elif N<6
//	printf("N<6\n");
//#else
//	printf("haha\n");
//#endif
//	return 0;
//}

//#define DEBUG 0
//int main()
//{
//#ifndef DEBUG
//	printf("DEBUG未定义，编译程序段1\n");
//#else
//	printf("已定义DEBUG，编译程序段2\n");
//#endif
//	return 0;
//}

//#define DEBUG 0
//int main()
//{
//#ifndef DEBUG
//	printf("DEBUG以定义，编译程序段1\n");
//#else
//	printf("未定义DEBUG，编译程序段2\n");
//#endif
//	return 0;
//}
