#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>

//int main()
//{
//	int a[] = { 1,3,6,10,15,21,28,36,45 };
//	int sz = sizeof(a) / sizeof(a[0]);
//	int n, m;
//	scanf("%d %d", &m, &n);
//	if (m > n)
//	{
//		int tmp = m;
//		m = n;
//		n = tmp;
//	}
//	if (n > a[sz - 1])
//	{
//		printf("n 错误\n");
//		exit(0);
//	}
//	int left = 0;
//	int right = sz - 1;
//	int start = 0;
//	int end = 0;
//	//0 1 2 3 4
//	//1 3 5 7 9
//	while (left <= right)
//	{
//		int mid = (left + right) / 2;
//		if(a[mid] > m)
//		{
//			right = mid - 1;
//		}
//		else if (a[mid] < m)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			right--;
//		}
//	}
//	start = left;
//	left = 0;
//	right = sz - 1;
//	while (left <= right)
//	{
//		int mid = (left + right) / 2;
//		if (a[mid] > n)
//		{
//			right = mid - 1;
//		}
//		else if (a[mid] < n)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			left++;
//		}
//	}
//	end = left;
//	for (int i = start; i < end; i++)
//	{
//		printf("%d ", a[i]);
//	}
//	return 0;
//}

void Print(int a[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void BubbleSort(int a[], int sz)
{
	for (int i = 0; i < sz - 1; i++)
	{
		for (int j = 0; j < sz - 1 - i; j++)
		{
			if (a[j] < a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
			}
		}
	}
}

void DBubbleSort(int a[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		for (int i = left; i < right; i++)
		{
			if (a[i] < a[i + 1])
			{
				Swap(&a[i], &a[i + 1]);
			}
		}
		right--;
		for (int i = right; i > left; i--)
		{
			if (a[i] > a[i - 1])
			{
				Swap(&a[i], &a[i - 1]);
			}
		}
		left++;
	}
}

void SelectSort(int a[], int sz)
{
	/*for (int i = 0; i < sz-1; i++)
	{
		int max = i;
		for (int j = i + 1; j < sz; j++)
		{
			if (a[max] < a[j])
			{
				max = j;
			}
		}
		if (max != i)
		{
			Swap(&a[i], &a[max]);
		}
	}*/

	for (int i = 0; i < sz - 1; i++)
	{
		for (int j = i + 1; j < sz; j++)
		{
			if (a[i] < a[j])
			{
				Swap(&a[i], &a[j]);
			}
		}
	}
}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Print(arr, sz);
//	//BubbleSort(arr, sz);
//	//DBubbleSort(arr, sz);
//	SelectSort(arr, sz);
//	Print(arr, sz);
//
//
//	return 0;
//}

//int main()
//{
//	char str[50] = { 0 };
//	gets(str);
//	int count = 0;
//	int flag = 1;
//	int i = 0;
//	while (str[i])
//	{
//		if (str[i] == ' ')
//		{
//			flag = 1;
//		}
//		else if(flag == 1)
//		{
//			count++;
//			flag = 0;
//		}
//		i++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int arr[101] = { 0,0 };
//	for (int i = 2; i <= 100; i++)
//	{
//		arr[i] = i;
//	}
//
//	for (int i = 2; i*i < 100; i++)
//	{
//		for (int j = i + 1; j <= 100; j++)
//		{
//			//if (arr[i] == 0)
//			//{
//			//	break;	//当前数以被挖去了，用下一个未被挖去的数除
//			//}
//			
//			//if (arr[j] != 0)//用下一个为被挖去的数去除
//			//{
//			//	if (arr[j] % arr[i] == 0)
//			//	{
//			//		arr[j] = 0;
//			//	}
//			//}
//			if (arr[i] != 0 && arr[j] != 0)//用下一个为被挖去的数去除
//			{
//				if (arr[j] % arr[i] == 0)
//				{
//					arr[j] = 0;
//				}
//			}
//		}
//	}
//	int count = 0;
//	for (int i = 1; i <= 100; i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d ", arr[i]);
//			count++;
//			if (count % 5 == 0)
//				printf("\n");
//		}
//	}
//	return 0;
//}
//int main()
//{
//	for (int i = 0; i < 5; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			if (i >= j)
//			{
//				printf("%d ", i - j + 1);
//			}
//			else
//			{
//				printf("0 ");
//			}
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int arr[11] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	for (i = sz - 2; i >= 0; i--)
//	{
//		if (arr[i] > n)
//		{
//			arr[i + 1] = arr[i];
//		}
//		else
//		{
//			arr[i + 1] = n;
//			break;
//		}
//	}
//	if (i < 0)
//	{
//		arr[i+1] = n;
//	}
//	Print(arr, sz);
//	return 0;
//}

//int main()
//{
//	int arr[10][10] = { 0 };
//
//	for (int i = 0; i < 10; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			if (i == j || j == 0)
//			{
//				arr[i][j] = 1;
//			}
//			else
//			{
//				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//			}
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		
//		for (int k = 0; k < 10 - i; k++)
//		{
//			printf("  ");
//		}
//		for (int j = 0; j <= i; j++)
//		{
//			if (j <= i)
//			{
//				printf("%4d", arr[i][j]);
//			}
//		}
//		printf("\n");
//	}
//	return 0;
//}

int main()
{
	int arr[][5] = { 1,2,3,4,12,
					12,7,8,9,10,
					11,12,13,14,15,
					16,17,18,19,20,
					21,22,23,24,25 };

	int max = 0;
	int row = 0;
	int col = 0;
	int i, j;
	for (i = 0; i < 5; i++)
	{
		max = arr[i][0];
		for (j = 0; j < 5; j++)
		{
			if (arr[i][j] > max)
			{
				max = arr[i][j];
				row = i;
				col = j;
			}
		}
		for (j = 0; j < 5; j++)
		{
			if (max > arr[j][col])
			{
				break;
			}
		}
		if (j >= 5)
		{
			printf("找到了%d %d %d\n", arr[row][col], row, col);
			break;
		}
	}
	if (i == 5)
	{
		printf("找不到\n");
	}
}
