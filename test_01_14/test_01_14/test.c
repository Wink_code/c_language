#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

int* productExceptSelf(int* nums, int numsSize, int* returnSize) {
    int* answer = (int*)calloc(numsSize, sizeof(int));
    *returnSize = numsSize;
    int left[numsSize];
    int right[numsSize];
    answer[0] = 1;
    int R = 1;
    //先求前缀之积
    for (int i = 1; i < numsSize; i++)
    {
        answer[i] = answer[i - 1] * nums[i - 1];
    }
    //求后缀之积与answer
    for (int i = numsSize - 1; i >= 0; i--)
    {
        // 对于下标 i，左边的乘积为 answer[i]，右边的乘积为 R
        answer[i] = answer[i] * R;
        // R 需要包含右边所有的乘积，所以计算下一个结果时需要将当前值乘到 R 上
        R *= nums[i];
    }
    return answer;
}
int* productExceptSelf(int* nums, int numsSize, int* returnSize) {
    int* answer = (int*)calloc(numsSize, sizeof(int));
    *returnSize = numsSize;
    int sum = 1;
    for (int i = 0; i < numsSize; i++)
    {
        sum *= nums[i]; //求所有元素的乘积
    }
    for (int i = 0; i < numsSize; i++)
    {
        answer[i] = sum / nums[i];
    }
    return answer;
}

int main()
{
	int arr[] = { 1,2,3,4 };
	int sz = sizeof(arr) / sizeof(arr[0]);
    int count = 0;
    int* ret = productExceptSelf(arr, sz, &count);
    for (int i = 0; i < count; i++)
    {
        printf("%d", *(ret + i));
        if (i != count - 1)
        {
            printf(",");
        }
    }
    free(ret);
    ret = NULL;
	return 0;
}

//int* productExceptSelf(int* nums, int numsSize, int* returnSize) {
//    int* answer = (int*)calloc(numsSize, sizeof(int));
//    *returnSize = numsSize;
//    int Left[4];
//    int Right[4];
//    Left[0] = 1;
//    Right[3] = 1;
//    for (int i = 1; i < numsSize; i++)
//    {
//        Left[i] = Left[i - 1] * nums[i - 1];
//    }
//    for (int i = 2; i >= 0; i--)
//    {
//        Right[i] = Right[i + 1] * nums[i + 1];
//    }
//    for (int i = 0; i < numsSize; i++)
//    {
//        answer[i] = Left[i] * Right[i];
//    }
//    return answer;
//}

//int* productExceptSelf(int* nums, int numsSize, int* returnSize) {
//    int* answer = (int*)calloc(numsSize, sizeof(int));
//    *returnSize = numsSize;
//    int left[numsSize];
//    int right[numsSize];
//    answer[0] = 1;
//    int R = 1;
//    //先求前缀之积
//    for (int i = 1; i < numsSize; i++)
//    {
//        answer[i] = answer[i - 1] * nums[i - 1];
//    }
//    //求后缀之积与answer
//    for (int i = numsSize - 1; i >= 0; i--)
//    {
//        // 对于下标 i，左边的乘积为 answer[i]，右边的乘积为 R
//        answer[i] = answer[i] * R;
//        // R 需要包含右边所有的乘积，所以计算下一个结果时需要将当前值乘到 R 上
//        R *= nums[i];
//    }
//    return answer;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//    int count = 0;
//    int* ret = productExceptSelf(arr, sz, &count);
//    for (int i = 0; i < count; i++)
//    {
//        printf("%d", *(ret + i));
//        if (i != count - 1)
//        {
//            printf(",");
//        }
//    }
//    free(ret);
//    ret = NULL;
//	return 0;
//}

//#include<memory.h>
//#include<stdlib.h>
//int* findDisappearedNumbers(int* nums, int numsSize, int* returnSize) {
//    int* ret = (int*)malloc(sizeof(int) * numsSize);
//    int arr[numsSize + 1];
//    memset(arr, -1, (numsSize + 1) * sizeof(int));
//    for (int i = 0; i < numsSize; i++)
//    {
//        //将已经存在的数字的数量放在arr中
//        arr[nums[i]]++;
//    }
//    int k = 0;
//    for (int i = 1; i < numsSize + 1; i++)
//    {
//        //若该下标位置为0，则说明该下标对应的数字缺少
//        if (arr[i] == -1)
//        {
//            ret[k++] = i;
//        }
//    }
//    *returnSize = k;
//    return ret;
//}
//
//int main()
//{
//    int arr[] = { 1,1,2,3,4,5,5,6,7 };
//    int count = 0;
//    int* ret = findDisappearedNumbers(arr, 9, &count);
//    return 0;
//}

//int Add(int num1, int num2) {
//    // write code here
//    int sum = 0;
//    int tmp = 0;
//    do
//    {
//        sum = num1 ^ num2;
//        tmp = (num1 & num2) << 1;
//        num1 = sum;
//        num2 = tmp;
//    } while (num2 != 0);
//    return sum;
//}
// int Add(int num1, int num2 ) {
//     // write code here
//     int sum = num1;
//     int tmp = num2;
//     while(num2 != 0)
//     {
//         //先得到不进位的
//         sum = num1 ^ num2;
//         //得到进位
//         tmp = (num1 & num2) << 1;
//         //重复以上步骤，直到不进位
//         num1 = sum;
//         num2 = tmp;
//     }
//     return sum;
// }

//int main()
//{
//	int arr[][3] = { {1,18,16}, {2,1,18}, {4,7,22} };
//	int i = 0;
//	int j = 0;
//	int flag = 0;
//	int row = 0;
//	int col = 0;
//
//	for (i = 0; i < 3; i++)
//	{
//		flag = 1;//假设鞍点在该行中
//		int max_min = arr[i][0];
//		col = 0;
//		//找每行最大的元素
//		for (j = 0; j < 3; j++)
//		{
//			if (arr[i][j] > max_min)
//			{
//				max_min = arr[i][j];
//				row = i;
//				col = j;
//			}
//		}
//		//判断行最大，是不是列中最小的
//		for (int k = 0; k < 3; k++)
//		{
//			if (max_min > arr[k][col])
//			{
//				flag = 0;//不是列最小，不是鞍点
//				break;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//	if (flag == 1)
//		printf("%d %d\n", row, col);
//	else
//		printf("没有鞍点\n");
//	return 0;
//}

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int n = 0;
	scanf("%d", &n);
	int left = 0;
	int right = 10 - 1;
	while (left <= right)
	{
		int mid = (left + right) / 2;
		if (arr[mid] == n)
		{
			printf("找到了，下标为：%d\n", mid);
			break;
		}
		else if(arr[mid] < n)
		{
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
	}
	if (left > right)
	{
		printf("找不到\n");
	}
	return 0;
}