#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
int Step(int n)
{
    if (n == 1)
    {
        return 1;
    }
    else if (n == 2)
    {
        return 2;
    }
    else
    {
        return Step(n - 1) + Step(n - 2);
    }
}
int main()
{
    int stair = 0;
    scanf("%d", &stair);
    int ret = Step(stair);
    printf("%d\n", ret);
    return 0;
}

//#include<math.h>
//long long func(long long t)
//{
//    int tmp = 0;
//    long long  result = t;
//
//    t = t / 10; //去个位上的数
//    while (t > 0)
//    {
//        tmp = t % 10;  //得到个位
//        result = result * 10 + tmp;
//        t = t / 10;
//    }
//    return result;
//}
//int  Prime(int n)
//{
//    int i = 0;
//    for (i = 3; i <= sqrt(n); i += 2)
//    {
//        if (n % i == 0)
//        {
//            return 0;
//        }
//    }
//    return 1;
//}
//int main()
//{
//    long long t = 0;
//    scanf("%lld", &t);
//    t = func(t);
//    if (0 == Prime(t))
//    {
//        printf("noprime\n");
//    }
//    else
//    {
//        printf("prime\n");
//    }
//    return 0;
//}


//int func(int arr[])
//{
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	return sz;
//}
//void func(int* p)
//{
//	
//	return 0;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz1 = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz1);
//	int sz2 = 0;
//	sz2 = func(arr);
//	printf("%d\n", sz2);
//	return 0;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* pa = arr;
//	printf("&arr[0]=  %p\n", &arr[0]);
//	printf("&arr[0]+1=%p\n", &arr[0]+1);
//	printf("arr=      %p\n", arr);
//	printf("arr+1=    %p\n", arr+1);
//	printf("&arr=     %p\n", &arr);
//	printf("&arr+1=   %p\n", &arr+1);
//	return 0;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr);
//	printf("%d\n", sz);
//	return 0;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* pa = arr;
//	printf("%p\n", &arr[0]);
//	printf("%p\n", arr);
//
//	return 0;
//}

//#define NDEBUG
//#include<assert.h>
//int main()
//{
//	int* p = NULL;
//	assert(p != NULL);
//	printf("66666\n");
//	return 0;
//}

//int* test()
//{
//	int a = 10;
//	return &a;
//}
//int main()
//{
//	int* ret = test();
//	printf("%d\n", *ret);
//	printf("%d\n", *ret);
//	printf("%d\n", *ret);
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	int* pa = arr;
//	*(arr + 8) = 10;
//	printf("%d\n", *(arr + 8));
//	return 0;
//}

//int main()
//{
//	int* pa;
//	printf("%d\n", *pa);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* pa = &arr[0];
//	while (pa < arr + sz)
//	{
//		printf("%d ", *pa);
//		pa++;
//	}
//	return 0;
//}

//int my_strlen(char* p)
//{
//	char* cur = p;
//	while (*cur != '\0')
//	{
//		cur++;
//	}
//	return cur - p;
//}
//int main()
//{
//	int ret = my_strlen("abcdef");
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* pa = &arr[0];
//	int i = 0; 
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(pa + i)); //这里是指针+整数
//	}
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int b = 20;
//	//int const* pa = &a;  //写成这样也可以
//	const int*  pa = &a;
//	*pa = 20;
//	pa = &b;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	a = 20;
//	const int b = 10;
//	b = 20;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	char b = 'c';
//	void* pi = &a;
//	pi = &b;
//	*pi = 'a';
//	pi + 1;
//	return 0;
//}

//int main()
//{
//	int a = 0x11223344;
//	int* pa = &a;
//	char* pc = (char*)&a;
//	printf("&a   = %p\n", &a);
//	printf("pa   = %p\n", pa);
//	printf("pa+1 = %p\n", pa +1);
//	printf("pc   = %p\n", pc);
//	printf("pc+1 = %p\n", pc+1);
//	return 0;
//}

//int main()
//{
//	//x86
//	int* a;
//	char* b;
//	double* c;
//	short* d;
//	printf("%d\n", sizeof(a));
//	printf("%d\n", sizeof(b));
//	printf("%d\n", sizeof(c));
//	printf("%d\n", sizeof(d));
//	return 0;
//}

//int main()
//{
//	int a = 0x11223344;
//	int* pa = &a;//取出a的地址放在指针变量pa中
//	*pa = 0;
//	printf("%d\n", a);
//	return 0;
//}