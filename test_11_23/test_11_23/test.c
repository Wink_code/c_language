#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Stu
{
	int age;
	char name[20];
};
//使用者自己实现两个元素的比较函数
int cmp(const void* p1, const void* p2)
{
	return   *((int*)p1) - *((int*)p2);
	//此处是将p1、p2变量强制转换为 整型指针变量 然后解引用
}

int cmp_struct_name(const void* p1, const void* p2)
{
	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
}

int cmp_struct_age(const void* p1, const void* p2)
{
	return (*(struct Stu*)p1).age - (*(struct Stu*)p2).age;
}

void print1(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void print2(struct Stu arr[],int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d %s\n", arr[i].age, arr[i].name);
	}
	printf("\n");
}

//由于被交换的数据的类型不是固定的，但是数据类型的大小是知道的
//因此我们可以交换数据的每一个字节的数据
void _Swap(const void* p1, const void* p2, int sz)
{
	int i = 0;
	//交换数据的每一个字节
	for (i = 0; i < sz; i++)
	{
		char tmp = *((char*)p1 + i);
		*((char*)p1 + i) = *((char*)p2 + i);
		*((char*)p2 + i) = tmp;
	}
}

void my_qsort(void* base, int num, int size, int (*compar)(const void*, const void*))
{
	int i = 0;
	for (i = 0; i < num - 1; i++)
	{
		int j = 0;
		for (j = 0; j < num - 1 - i; j++)
		{
			//此处应该是传给compar()两个参数arr[j]与arr[j+1]，让其进行比较
			//但是怎么拿到要传的数呢？
			//我qsort函数只有这个数组首元素的地址，和数组元素类型的大小
			//因此我可以让指针加上 j个类型的大小 找到某个元素的首地址，具体比较多大内容的数据，看比较什么类型的数据,由使用者决定
			//但是我得到的数组首元素的地址也是void* 类型的，所以我们可以将base转换位char*类型的指针，一次访问 j*size大小
			if (compar((char*)base + j * size, (char*)base + (j + 1) * size) > 0)
			{
				//交换
				_Swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}

//int main()
//{
//	//比较整型的数据
//	int arr1[] = { 2,1,8,5,6,3,4,9,7,0 };
//	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
//	printf("排序整型\n");
//	print1(arr1, sz1);
//	my_qsort(arr1, sz1, sizeof(arr1[0]), cmp);
//	print1(arr1, sz1); 
//
//	//比较结构体类型的数据
//	struct Stu arr2[] = { {38, "lisi"}, {18, "zhangsan"}, {25, "wangwu"} };
//	int sz2 = sizeof(arr2) / sizeof(arr2[0]);
//	printf("排序结构体型-按姓名\n");
//	print2(arr2, sz2);
//	my_qsort(arr2, sz2, sizeof(arr2[0]), cmp_struct_name);
//	print2(arr2, sz2);
//
//	printf("排序结构体型-按年龄\n");
//	print2(arr2, sz2);
//	my_qsort(arr2, sz2, sizeof(arr2[0]), cmp_struct_age);
//	print2(arr2, sz2);
//	return 0;
//}

//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//void menu()
//{
//	printf("****************************\n");
//	printf("******1.Add       2.Sub*****\n");
//	printf("******3.Mul       4.Div*****\n");
//	printf("******0.exit           *****\n");
//	printf("****************************\n");
//}
//
//void Calculate(int (*pfunc)(int, int))
//{
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	printf("请输入两个操作数：");
//	scanf("%d %d", &x, &y);
//	ret = pfunc(x, y);
//	printf("%d\n", ret);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择：\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calculate(Add);
//			break;
//		case 2:
//			Calculate(Sub);
//			break;
//		case 3:
//			Calculate(Mul);
//			break;
//		case 4:
//			Calculate(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误，请重新选择!\n");
//		}
//	} while (input);
//	return 0;
//}

//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	int (*pArr[])(int, int) = { NULL, Add, Sub, Mul ,Div };
//	//                           0     1    2    3    4   使用NULL巧妙地与选择相对应
//	do
//	{
//		menu();
//		printf("请选择：\n");
//		scanf("%d", &input);
//		if (input == 0)
//		{
//			printf("退出计算器\n");
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = pArr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("选择错误，请重新选择!\n");
//		}
//	} while (input);
//	return 0;
//}

//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do
//	{
//		menu();
//		printf("请选择：\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Add(x, y);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("%d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误，请重新选择!\n");
//		}
//	} while (input);
//	return 0;
//}
//int main()
//{
//	int (*p)(int, int) = Add;//函数指针
//	int (*pArr[])(int, int) = { Add, Sub, Mul, Div };  //函数指针数组
//	//                            0   1    2    3
//	int sz = sizeof(pArr) / sizeof(pArr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int ret = pArr[i](2, 3);//pArr[0] 就找到了Add函数的函数名，通过函数名调用该函数
//		printf("%d\n", ret);
//	}
//	
//	return 0;
//}

//void digit(int x, int y)
//{
//	if (y > 1)
//	{
//		digit(x / 10, --y);
//	}
//	printf("%d", x % 10);
//}
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	digit(x, y);
//	return 0;
//}

//int h(int n, int x)
//{
//	if (n == 0)
//	{
//		return 1;
//	}
//	else if (n == 1)
//	{
//		return 2 * n;
//	}
//	else
//	{
//		return 2 * x * h(n - 1, x) - 2 * (n - 1) * h(n - 2, x);
//	}
//}
//int main()
//{
//	int n = 0;
//	int x = 0;
//	scanf("%d %d", &n, &x);
//	int ret = h(n, x);
//	printf("%d\n", ret);
//	return 0;
//}