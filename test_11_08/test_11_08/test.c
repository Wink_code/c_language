#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
int bin_search(char a[], char n)
{
	int left = 0;
	int right = strlen(a);
	while (left <= right)
	{
		int mid = (left + right) / 2;
		if (a[mid] == n)
		{
			return  mid;
		}
		else if (a[mid] < n)
		{
			left = mid + 1;
		}
		else if (a[mid] > n)
		{
			right = mid - 1;
		}
		else
		{
			return -1;
		}
	}
}
int main()
{
	char arr[] = "ABCDEFGHIJKLMNO";
	char n = 0;
	scanf("%c", &n);
	int ret = bin_search(arr, n);
	printf("%d\n", ret);
	return 0;
}
//int main()
//{
//	int a[10] = { 0 };
//	int b[9] = { 0 };
//	int i = 0; 
//	for (i = 0; i < 10; i++)
//	{
//		a[i] = i+1;
//	}
//	for (i = 0; i < 9; i++)
//	{
//		b[i] = a[i+1] / a[i];
//		printf("%d ", b[i]);
//		if ((i + 1) % 3 == 0)
//		{
//			printf("\n");
//		}
//	}
//	return 0;
//}
//int main()
//{
//	int arr[11][11] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	int count = 1;
//	int left = 0;
//	int right = n - 1; 
//	int up = 0;
//	int down = n - 1;
//	int i = 0;
//	while (count <= n * n)
//	{
//		//最上面一行,行不变，列变
//		for (i = left; i <= right; i++)
//		{
//			arr[up][i] = count;
//		}
//		up++;
//		//最右侧一列，列不变，行变
//		for (i = up; i <= down; i++)
//		{
//			arr[i][right] = count;
//		}
//		right--;
//		//最下面一行，行不变，列变
//		for (i = right; i >= left; i--)
//		{
//			arr[down][i] = count;
//		}
//		down--;
//		//最左侧一列，列不变，行变
//		for (i = down; i >= up; i--)
//		{
//			arr[i][left] = count;
//		}
//		left++;
//		count++;
//	}
//	for (i = 0; i < n; i++)
//	{
//		int j = 0; 
//		for (j = 0; j < n; j++)
//		{
//			printf("%3d", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[5][5] = { 0 };
//	int i = 0; 
//	int j = 0;
//	int n = 1;
//	for (i = 0; i < 5; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			arr[i][j] = n++;
//		}
//	}
//	for (i = 0; i < 5; i++)
//	{
//		for (j = 0; j <=i ; j++)
//		{
//			printf("%3d",arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//void my_print(int a[2][3], int n, int m)
//{
//	for (int i = 0; i < m; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			printf("%d ", a[j][i]);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[][3] = { 2,4,6,8,10,12 };
//	my_print(arr, 2, 3);
//	return 0;
//}
//int main()
//{
//	int arr[30] = { 0 };
//	int ave[6] = { 0 };
//	int n = 0;
//	int tmp = 0;
//	int sum = 0;
//	int i = 0;
//	while (n < 30)
//	{
//		tmp = tmp + 2;
//		arr[n] = tmp;
//		sum += arr[n];
//		if ((n + 1) % 5 == 0)
//		{
//			ave[i] = sum / 5;
//			i++;
//			sum = 0;
//		}
//		n++;
//	}
//	for (i = 0; i < 6; i++)
//	{
//		printf("%d ", ave[i]);
//	}
//	return 0;
//}
//int main()
//{
//	int arr1[2][3] = { {1, 2, 3} ,{ 4, 5, 6} };
//	int arr2[3][2] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 2; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			arr2[j][i] = arr1[i][j];
//		}
//	}
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 2; j++)
//		{
//			printf("%d ", arr2[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//#define M 5
//int main()
//{
//	int arr[M] = { 0 };
//	int count[5] = { 0 };
//	int n = 0;
//	int x = 0;
//	scanf("%d", &x);
//	while (x != -1 && n <=M)
//	{
//		if (x >= 0 && x <= 4)
//		{
//			arr[n] = x;
//			n++;
//			scanf("%d", &x);
//		}
//	}
//	int i = 0;
//	//i的范围是小于n，数组初始化的全0，
//	for (i = 0; i < n; i++)
//	{
//		//arr[i] 的范围是0-4
//		count[arr[i]]++;  
//	}
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d的个数：%d\n", i, count[i]);
//	}
//	return 0;
//}
//int fun()
//{
//	int n = 0;
//	int i, j, k;
//	for (i = 1; i <= 9; i++)
//	{
//		for (j = 0; j <= 8; j += 2)
//		{
//			for (k = 0; k <= 0; k++)
//			{
//				if ((i * 100 + j * 10 + k) % 2 == 0)
//				{
//					printf("%d  ", i * 100 + j * 10 + k);
//					n++;
//				}
//			}
//		}
//	}
//	return n;
//}
//int main()
//{
//	int n = 0;
//	n = fun();
//	printf("%d\n", n);
//	return 0;
//}
//整型提升
//int main()
//{
//	char a = 125;
//	//00000000000000000000000001111101
//	char b = 10;
//	//00000000000000000000000000001010
//	// 
//	//00000000000000000000000010000111-----135
//	char c = 0;
//	c = a + b;
//	//11111111111111111111111110000111  --补码
//	//10000000000000000000000001111000  --反码
//	//10000000000000000000000001111001  --原码  -121
//
//	printf("c=%d\n", c);
//	printf("a+b=%d\n", a+b);
//	return 0;
//}


//数组中有：1 2 3 4 5 1 2 3 4，只有5出现一次，其他数字都出现2次，找出5
//0^a = a
//a^a = 0
//int main()
//{
//	int arr[9] = { 1,2,3,4,5,1,2,3,4 };
//	int i = 0;
//	int ret = 0;
//	for (i = 0; i < 9; i++)
//	{
//		ret = ret ^ arr[i];
//	}
//	printf("%d\n", ret);
//	return 0;
//}
//int main()
//{
//	char a = 'c';
//	char* pc = &a;
//	*pc = 'n';
//	printf("%c\n", a);
//	return 0;
//}