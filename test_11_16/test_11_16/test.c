#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#define ROW 2
#define COL 5

float ave_person[ROW] = { 0 };  //存放每人平均分
float ave_lesson[ROW] = { 0 };  //存放课程平均分
int x = 0;
int y = 0;

int main()
{
	//函数声明
	void per_average_score(float a[][5]);
	void per_lesson_average(float arr[][5]);
	float find_max_score(float arr[][COL]);

	float arr[ROW][COL] = { 0 };
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			scanf("%f", &arr[i][j]);
		}
	}
	per_average_score(arr);

	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			printf("%.2f ", arr[i][j]);
		}
		printf(" average = %.2f", ave_person[i]);
		printf("\n");
	}
	float ret = find_max_score(arr);
	printf("最高分为：%f， 第%d位学生的%d课程\n", ret, x, y);
	return 0;
}

void per_average_score(float a[][COL])
{
	int i = 0;
	int j = 0;
	for (i = 0; i < ROW; i++)
	{
		float sum = 0;
		for (j = 0; j < COL; j++)
		{
			sum += a[i][j];
		}
		ave_person[i] = sum / COL;
	}
}

void per_lesson_average(float arr[][COL])
{
	
	for (int i = 0;  i < ROW; i++)
	{
		float sum = 0;
		for (int j = 0; j < COL; j++)
		{
			sum += arr[j][i];
		}
		ave_person[i] = sum / ROW;
	}
}

float find_max_score(float arr[][COL])
{
	float max = arr[0][0];
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			if (arr[i][j] > max)
			{
				max = arr[i][j];
				x = i + 1; 
				y = j + 1;
			}
		}
	}
	return max;
}
