#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
int fib(int n)
{
	if (n == 1 || n == 2)
	{
		return 1;
	}
	else
	{
		return fib(n - 1) + fib(n - 2);
	}
}

int main()
{
	int num1 = 1;
	int num2 = 1;
	printf("%d %d ", num1, num2);
	for (int i = 3; i <= 40; i++)
	{
		int ret = fib(i);
		printf("%d ", ret);
	}
	return 0;
}

//int main()
//{
//	int num1 = 1;
//	int num2 = 1;
//	for (int i = 1; i <= 20; i++)
//	{
//		printf("%d %d ", num1, num2);
//		num1 = num1 + num2;
//		num2 = num2 + num1;
//	}
//
//	//printf("%d %d ", num1, num2);
//	/*for (int i = 3; i <= 40; i++)
//	{
//		int num3 = num1 + num2;
//		printf("%d ", num3);
//		num1 = num2; 
//		num2 = num3;
//	}*/
//
//	/*for (int i = 1; i <= 19; i++)
//	{
//		int num3 = num1 + num2;
//		int num4 = num2 + num3;
//		printf("%d %d ", num3, num4);
//		num1 = num3;
//		num2 = num4;
//	}*/
//	return 0;
//}

//#include<math.h>
//int main()
//{
//	double pi = 0;
//	double flag = 1;
//	int i = 1;
//	double ret = flag / i;
//	while (fabs(ret) >= 1e-6)
//	{
//		pi += ret;  //每一项相加的结果
//		i = i + 2;
//		flag = -flag;
//		ret = flag / i;//一项
//	}
//	pi = 4 * pi;
//	printf("%lf\n", pi);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int j = 0;
//	/*for (i = 1; i <= 4; i++)
//	{
//		for (j = 1; j <= 5; j++)
//		{
//			printf("%d ", i * j);
//		}
//		printf("\n");
//	}*/
//	for (i = 1; i <= 4; i++)
//	{
//		int count = 0;
//		for (j = i; count!= 5; j++)
//		{
//			count++;
//			printf("%d ", j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (i % 3 != 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	double sum = 0;
//	double money = 0;
//	int count = 0;
//	while (sum < 100000 && count< 1000)
//	{
//		scanf("%lf", &money);
//		sum += money;
//		count++;
//	}
//	printf("%lf %d\n", sum, count);
//	return 0;
//}

//int main()
//{
//	int sum = 0;
//	int i = 1;
//	for (i = 1; i <= 100; i++)
//	{
//		sum += i;
//	}
//	printf("%d\n", sum);
//	sum = 0;
//	i = 1;
//	while (i <= 100)
//	{
//		sum += i;
//		i++;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//void print(int num)
//{
//	if (num > 9)
//	{
//		print(num / 10);//先递归到只有第一位数
//	}
//	printf("%d ", num % 10);//每次仅输出一位
//}
//int main()
//{
//	int num = 0;
//	do
//	{
//		scanf("%d", &num);
//	} while (num >= 100000 || num <= 0);  //不是五位数内的正整数就重新输入
//	//1. 求他是一个几位数
//	int count = 1;
//	int tmp = num;
//	while (tmp > 9)
//	{
//		count++;
//		tmp = tmp / 10;
//	}
//	printf("%d是一个%d位数\n", num, count);
//	//2.输出每一位
//	print(num);
//	printf("\n");
//	//3.逆序输出
//	while (num)
//	{
//		printf("%d ", num % 10);
//		num = num / 10;
//	}
//	return 0;
//}

//int main()
//{
//	double score = 0.0;
//	scanf("%lf", &score);
//	char grade = 0;
//	switch ((int)(score / 10)) //由于switch语句中只能是整型表达式，所以应该强制类型转换
//	{
//	case 10:
//	case 9:
//		grade = 'A';
//		break;
//	case 8:
//		grade = 'B';
//		break;
//	case 7:
//		grade = 'C';
//		break;
//	case 6:
//		grade = 'D';
//		break;
//	default: 
//		grade = 'E';
//		break;
//	}
//	printf("%c\n", grade);
//	return 0;
//}