#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    int is_leap_year(int year);

    int year = 0;
    int month = 0;
    int day = 0;
    int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
    scanf("%d %d %d", &year, &month, &day);
    int day_sum = day;
    int i = 0;
    for (i = 1; i < month; i++)
    {
        day_sum += arr[i];
    }
    if (is_leap_year(year) && month > 2)
    {
        day_sum += 1;
    }
    printf("%d\n", day_sum);
    return 0;
}

int is_leap_year(int year)
{
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
    {
        return 1;
    }
    return 0;
}

//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= 9; i++)
//	{
//		for (int k = 0; k < 9 - i; k++)
//		{
//			printf("\t");
//		}
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%d\t", i, j, j * i);
//		}
//		printf("\n");
//	}

	/*for (i = 9; i >= 1; i--)
	{
		for (int k = 0; k < 9 - i; k++)
		{
			printf("\t");
		}
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%d\t", j, i, j * i);
		}
		printf("\n");
	}*/
	/*for (i = 1; i<=9; i++)
	{
		for (j = 1; j <= 9; j++)
		{
			if (j < i)
			{
				printf("\t");
			}
			else
			{
				printf("%d*%d=%d\t", j, i, j * i);
			}
		}
		printf("\n");
	}*/

	/*for (i = 9; i >= 1; i--)
	{
		for (j = i; j>=1; j--)
		{
			printf("%d*%d=%d\t", i, j, i * j);
		}
		printf("\n");
	}*/

	/*for (i = 1; i <= 9; i++)
	{
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%d\t", i, j, i * j);
		}
		printf("\n");
	}*/
//	return 0;
//}

//int CalCount(int num)
//{
//	int count = 1;
//	while (num / 10)
//	{
//		count++;
//		num /= 10;
//	}
//	return count;
//}
//
////void print(num)
////{
////	if (num > 9)
////	{
////		print(num / 10);
////	}
////	printf("%d ", num % 10);
////}
//
//int print(num)
//{
//	int sum = num;
//	if (num > 9)
//	{
//		sum = print(num / 10) * 10 + num % 10;
//	}
//	return sum;
//}
//
//void RevPrint(num)
//{
//	while (num)
//	{
//		printf("%d ", num % 10);
//		num /= 10;
//	}
//}
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	/*int count = CalCount(num);
//	printf("%d\n", count);*/
//	int ret = print(num);
//	printf("%d\n", ret);
//
//	/*printf("\n");
//	RevPrint(num);*/
//	return 0;
//}

//int main()
//{
//	int score = 0;
//	scanf("%d", &score);
//	switch (score / 10)
//	{
//	case 10:
//	case 9:
//		printf("A\n");
//		break;
//	case 8:
//		printf("B\n");
//		break;
//	case 7:
//		printf("C\n");
//		break;
//	case 6:
//		printf("D\n");
//		break;
//	default:
//		printf("E\n");
//		break;
//	}
//	return 0;
//}

//#include<math.h>
//static int arr[100000] = { 0 };
//int* printNumbers(int n, int* returnSize) {
//    // write code here
//    int count = pow(10, n);
//
//    int i = 0;
//    for (i = 1; i < count; i++)
//    {
//        arr[i - 1] = i;
//    }
//    *returnSize = i - 1;
//    return arr;
//
//}

//int x = 5, y = 7;
//void swap()
//{
//	int z;
//	z = x;
//	x = y;
//	y = z;
//} 
//int main()
//{
//	int x = 3, y = 8;
//	swap();
//	printf("%d,%d\n",x, y);
//	return 0;
//}