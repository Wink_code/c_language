#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void menu()
{
	printf("*******************************\n");
	printf("*********   1.play    *********\n");
	printf("*********   0.exit    *********\n");
	printf("*******************************\n");
}
void game()
{
	char mine[ROWS][COLS]; //用来存放布置雷的信息
	char show[ROWS][COLS]; //用来存放排查雷的信息
	//初始化两个棋盘
	InitBoard(mine, ROWS, COLS ,'0');
	InitBoard(show, ROWS, COLS, '*');
	//打印棋盘
	//DisPlay(mine, ROW, COL);
	DisPlay(show, ROW, COL);
	//布雷
	SetMine(mine);
	//DisPlay(mine, ROW, COL);
	//排查雷
	FindMine(show, mine, ROW, COL);
}

int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	//打印菜单
	do
	{
		menu();
		printf("请选择：");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			printf("玩游戏\n");
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误，请重新选择!\n");
			break;
		}
	} while (input);
	return 0;
}