#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"


void InitBoard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	for (i = 0; i < rows; i++)
	{
		int j = 0;
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

void DisPlay(char board[ROWS][COLS], int row, int col)
{
	printf("----------扫雷----------\n");
	int i = 0;
	//打印行号
	for (i = 0; i <= row; i++)
	{
		printf("%d ", i);
		if (i == 0)
		{
			printf("|");
		}
	}
	printf("\n");
	//打印行分割线
	for (i = 0; i <= row; i++)
	{
		printf("--");
		if (i == 0)
		{
			printf("|");
		}
	}
	printf("\n");
	for (i = 1; i <= row;i++)
	{
		//打印列分割线
		printf("%d |", i);
		int j = 0;
		//打印列号
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("----------扫雷----------\n");
	printf("\n");
}

void SetMine(char board[ROWS][COLS])
{
	//需要布置几个雷，Count_mine = 10
	int count = Count_mine;
	while (count)
	{
		//对9取余，余0-8  加1正好为1-9
		int x = rand() % 9 + 1;
		int y = rand() % 9 + 1;
		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}


int Around_Mine(char board[ROWS][COLS], int x, int y)
{
	int count = 0;
	int i = 0;
	for (i = x - 1; i <= x + 1; i++)
	{
		int j = 0;
		for (j = y - 1; j <= y + 1; j++)
		{
			if (board[i][j] == '1')
			{
				count++;
			}
		}
	}
	return count;
}
//排查完的个数
int win_count = 0;


void OpenBoard(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y)
{
	int count = 0;
	int i = 0;
	int j = 0;
	for (i = x - 1; i <= x + 1; i++)
	{
		for (j = y - 1; j <= y + 1; j++)
		{
			//该坐标周围雷的个数
			if (mine[i][j] == '1')
			{
				count++;
			}
		}
	}
	if (count == 0)
	{
		show[x][y] = ' ';
		win_count++;
	}
	else
	{
		show[x][y] = count + '0';
		win_count++;
	}
	if (show[x][y] == ' ')
	{
		for (i = x - 1; i <= x + 1; i++)
		{
			for (j = y - 1; j <= y + 1; j++)
			{
				//该坐标不是雷，并且没有被排查过，下标也不能越界
				if (mine[i][i] != '1' && show[i][j] == '*' && (i >= 1 && i <= ROW) && (j >= 1 && j <= COL))
				{
					OpenBoard(mine, show, i, j);
				}
			}
		}
	}
}
void FindMine(char show[ROWS][COLS], char mine[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	while (win_count <row * col - Count_mine)
	{
		printf("请输入要排查的坐标：");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾，你踩到雷了，游戏结束\n下面为雷阵图\n");
				DisPlay(mine, row, col);
				break;
			}
			else
			{
				////该位置不是雷，统计周围有几个雷
				//int count = Around_Mine(mine ,x, y);
				//show[x][y] =  count + '0';
				//win_count++;
				OpenBoard(mine, show, x, y);
				DisPlay(show, ROW, COL);
			}
		}
		else
		{
			printf("坐标错误，请重新输入\n");
		}
	}
	if (win_count == row * col - Count_mine)
	{
		printf("恭喜你，所有的雷都被你排除了！\n");
	}
}



