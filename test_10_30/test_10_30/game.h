#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>

//可操作的数组大小
#define ROW 9
#define COL 9

//实际大小
#define ROWS ROW+2
#define COLS COL+2
//雷的数量
#define Count_mine 3
//初始化棋盘
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set);
//打印棋盘
void DisPlay(char board[ROWS][COLS], int row, int col);
//布雷
void SetMine(char board[ROWS][COLS]);
//计算周围有多少雷
int Around_Mine(char board[ROWS][COLS], int x, int y);
//排查雷
void FindMine(char show[ROWS][COLS], char mine[ROWS][COLS], int row, int col);
//展开周围
void OpenBoard(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y);




