#define _CRT_SECURE_NO_WARNINGS 1

//牛客题目

#include<stdio.h>
//输出"Hello Nowcoder!"。开始你的编程之旅吧
//int main()
//{
//	printf("Hello Nowcoder!");
//	return 0;
//}

//请帮他编写程序输出这架小飞机
//int main()
//{
//	printf("     **\n");
//	printf("     **\n");
//	printf("************\n");
//	printf("************\n");
//	printf("    *  *\n");
//	printf("    *  *\n");
//	return 0;
//}

//输入一个整数，输出这个整数。
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	printf("%d\n", a);
//	return 0;
//}

//输入一个浮点数，输出这个浮点数(保留3位小数）
//int main()
//{
//	float a = 0;
//	scanf("%f", &a);
//	printf("%.3f\n",a);
//	return 0;
//}

//输入一个字符，输出这个字符
//int main()
//{
//	char c = '0';
//	scanf("%c", &c);
//	printf("%c\n", c);
//	return 0;
//}

//牛牛从键盘上输入三个整数，并尝试在屏幕上显示第二个整数。输入描述：一行输入 3 个整数，用空格隔开
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d%d%d", &a, &b, &c);
//	printf("%d\n", b);
//	return 0;
//}

//用键盘读入一个字符，然后在屏幕上显示用这个字符组成的 3*3 的矩形

//int main()
//{
	/*char c = '0';
	int i = 0;
	int j = 0;
	scanf("%c", &c);
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			printf("%c", c);
		}
		printf("\n");
	}*/

	//int temp = 0;
	//while (i)
	//{
	//	temp = 3;
	//	while (temp)
	//	{
	//		printf("%c", c);
	//		temp--;
	//	}
	//	i--;
	//	printf("\n");
	//}
	
	//return 0;
//}

//用键盘读入一个字符，然后在屏幕上显示一个用这个字符填充的对角线长5个字符，倾斜放置的菱形。
//  #
// ###
//#####
// ###
//  #
//int main()
//{
//	char c = 0;
//	scanf("%c", &c);
//	int i = 3;
//	//1.打印上半部分
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		//打印空格
//		for (j = 0; j <3-1-i ; j++)
//		{
//			printf(" ");
//		}
//		//打印字符
//		for (j = 0; j <2*i+1 ; j++)
//		{
//			printf("%c", c);
//		}
//		printf("\n");
//	}
//	//2.打印下半部分
//	for (i = 0; i < 3-1; i++)
//	{
//		int j = 0;
//		//打印空格
//		for (j = 0; j < i + 1; j++)
//		{
//			printf(" ");
//		}
//		//打印字符
//		for (j = 0; j <2*(3-1-i)-1 ; j++)
//		{
//			printf("%c",c);
//		}
//		printf("\n");
//	}
//	return 0;
//}
// 
// //输入一个字符，输出该字符相应的ASCII码。
//int main()
//{
//	char ch = '0';
//	scanf("%c", &ch);
//	printf("%d", ch);
//	return 0;
//}
// 
// 

//int main()
//{
//	int line = 0;
//	int i = 0;
//	scanf("%d", &line);
//	//1.打印上半部分
//	for (int i = 0; i < line; i++)
//	{
//		int j = 0;
//		for (j = 0; i < line - 1 - j; j++)
//		{
//		//1.打印空格
//			printf(" ");
//		}
//		//2.打印*
//		for (j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//2.打印下半部分
//	for (i = 0; i < line - 1; i++)
//	{
//		int j = 0;
//		//1.打印空格
//		for (j = 0; j <i+1 ; j++)
//		{
//			printf(" ");
//		}
//		//2.打印*
//		for (j = 0; j < 2*(line-1-i)-1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//四舍五入一个浮点数
int main()
{
	float f = 0;
	scanf("%f", &f);
	//正的
	if (f > 0)
	{
		//小数部分大于0.5则进一
		if (f >= (int)f + 0.5)
		{
			printf("%d", (int)f + 1);
		}
		//小数部分小于0.5
		else
		{
			printf("%d", (int)f);
		}
	}
	//负的
	else
	{
		//小数部分大于0.5
		if (f <= (int)f - 0.5)
		{
			printf("%d", (int)f - 1);
		}
		else
		{
			printf("%d", (int)f);
		}
	}
	return 0;
}
