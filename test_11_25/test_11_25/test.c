﻿#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include<assert.h>

void* my_memmove(void* dest, const void* src, int n)
{
	assert(dest && src);
	void* ret = dest;
	//1.前->后  后->前
	if(dest < src)
	{
		//从前向后拷
		while (n--)
		{
			*((char*)dest) = *((char*)src);
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		//从后向前拷
		while (n--)
		{
			*((char*)dest + n) = *((char*)src + n);
		}
	}

	//2.前->后  后->前  前->后
	//if (dest < src)
	//{
	//	//从前向后拷
	//	while (n--)
	//	{
	//		*((char*)dest) = *((char*)src);
	//		dest = (char*)dest + 1;
	//		src = (char*)src + 1;
	//	}
	//}
	//else if(((char*)dest - (char*)src) > 0 && ((char*)dest - (char*)src) < n)
	//{
	//	//从后向前拷
	//	while (n--)
	//	{
	//		*((char*)dest + n) = *((char*)src + n);
	//	}
	//}
	//else
	//{
	//	//从前向后拷
	//	while (n--)
	//	{
	//		*((char*)dest) = *((char*)src);
	//		dest = (char*)dest + 1;
	//		src = (char*)src + 1;
	//	}
	//}
	return ret;
}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	my_memmove(arr+4, arr, 20);
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}

//void* my_memcpy(void* dest, const void* src, int sz)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (sz--)
//	{
//		*((char*)dest) = *((char*)src);
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[10] = { 0 };
//	int arr2[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memcpy(arr1, arr2, 12);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

//char* my_strncat(char* dest, char* src, int n)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (n-- && (*dest++ = *src++))
//	{
//		;
//	}
//	*dest = '\0';
//	return ret;
//}
//
//int main()
//{
//	char str1[20] = "hello-";
//	char str2[] = "world";
//	printf("%s\n", my_strncat(str1, str2, 6));
//	return 0;
//}


//char* my_strncpy(char* dest, const char* src, int n)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (n-- && (*dest++ = *src++))
//	{
//		;
//	}
//	/*while (n-- && ((*src) != '\0'))
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}*/
//	while (n--)
//	{
//		*dest = '\0';
//		dest++;
//	}
//	return ret;
//}
//
//int main()
//{
//	char str1[] = "hello-world";
//	char str2[] = "XXXXX";
//	printf("%s\n", my_strncpy(str1, str2, 8));
//	return 0;
//}

//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d,b=%d,c=%d", a, b, c);
//	return 0;
//}

//int main()
//{
////	/*unsigned int a = 0x1234;
////	unsigned char b = *(unsigned char*)&a;*/
////	//char a = 255;
//	int arr[] = { 1,11,12,13 };
//	printf("%p\n", arr);
//	printf("%p\n", arr+1);
//	char ch[] = { 255,2,3,4 };
//	printf("%p\n", ch);
//	printf("%p\n", ch+1);
////	int a = 0x11223344;
////	char* p = (char*)&a;
////	printf("%x\n", *p);
//	return 0;
//}

//int check()
//{
//	int i = 1;
//	char* p = (char*)&i;
//	return *p;
//}
//
//int main()
//{
//	if (check())
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}

//int main()
//{
//	char arr[5] = { 1,'0',2,3,'\0'};
//	printf("%d\n", strlen(arr));
//	return 0;
//}

//int main()
//{
//	unsigned char a = 100;
//	unsigned char b = 200;
//	unsigned char c = a + b;
//	printf("%d\n", a + b);
//	return 0;
//}

//#include <errno.h>

//我们打印⼀下0~10这些错误码对应的信息
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 10; i++) 
//	{
//		printf("%d:%s\n", i, strerror(i));
//	}
//	return 0;
//}

//int main()
//{
//	char arr[] = "192.168.6.111";
//	char* sep = ".";
//	char* str = NULL;
//	for (str = strtok(arr, sep); str != NULL; str = strtok(NULL, sep))
//	{
//		printf("%s\n", str);
//	}
//	return 0;
//}

//int main()
//{
//	int num = 10;
//	while (num--)
//	{
//		printf("%d\n", num);
//	}
//	return 0;
//}

//int main()
//{
//	char arr[] = { 'a', 'b', 'c', '\0' };
//	char* s = "abcdef";
//	printf("%s\n", arr);
//	printf("%s\n", s);
//	return 0;
//}