#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//leetcode88.合并两个有序数组
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    int index1 = m - 1;
    int index2 = n - 1;
    int index3 = nums1Size - 1;
    //两数组下标均有效
    while (index1 >= 0 && index2 >= 0)
    {
        if (nums1[index1] > nums2[index2])
        {
            nums1[index3--] = nums1[index1--];
        }
        else
        {
            nums1[index3--] = nums2[index2--];
        }
    }
    //若index2<0,则数组已有序
    //若index1<0,则将num2中的剩余元素存储
    while (index2 >= 0)
    {
        nums1[index3--] = nums2[index2--];
    }
}

//leetcode27.移除元素
int removeElement2(int* nums, int numsSize, int val) {
    int i = 0;
    int j = 0;
    while (i < numsSize)
    {
        if (nums[i] == val)
        {
            i++;
        }
        else
        {
            nums[j] = nums[i];
            i++;
            j++;
        }
    }
    return j;
}

int removeElement3(int* nums, int numsSize, int val) {
    int i = 0;
    int j = 0;
    while (i < numsSize)
    {
        if (nums[i] == val)
        {
            //若相等，后面的将前面的覆盖
            for (j = i; j < numsSize - 1; j++)
            {
                nums[j] = nums[j + 1];
                
            }
            numsSize--;//删除一个后，数组大小-1
        }
        else
        {
            i++;
        }
    }
    return i;
}

int removeElement(int* nums, int numsSize, int val) {
    int left = 0;
    int right = numsSize - 1;
    while (left <= right)
    {
        if (nums[left] == val)
        {
            //如果最左侧的值==val,使用最右侧的将其覆盖
            //同时right--
            nums[left] = nums[right];
            right--;
        }
        else
        {
            left++;
        }
    }
    return left;
}

int main()
{
    int arr[] = { 3,2,2,3 };
    int len = removeElement(arr, 4, 3);
    for (int i = 0; i < len; i++)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}