#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<math.h>
int main()
{
	int num = 0;
	do
	{
		scanf("%d", &num);
		if (num > 1000 || num <=0)
		{
			printf("输入错误，请重新输入\n");
		}
	} while (num > 1000 || num <= 0);  //不在1-1000范围内就重新输入
	int ret = (int)sqrt(num);
	printf("%d\n", ret);
	return 0;
}
//计算三角形的面积

//int main()
//{
//	double a, b, c;
//	scanf("%lf %lf %lf", &a, &b, &c);
//	double area = 0.0;
//	double s = (a + b + c) / 2;
//	if (a + b > c || a + c > b || b + c > a)
//	{
//		area = sqrt(s * (s - a) * (s - b) * (s - c));
//		printf("area = %lf\n", area);
//	}
//	else
//	{
//		printf("不能构成三角形\n");
//	}
//	return 0;
//}

//int main()
//{
//	char ch1 = '0';
//	scanf("%c", &ch1);
//	if (ch1 >= 'a' && ch1 <= 'z')
//	{
//		ch1 = ch1 - 32;
//	}
//	printf("%c\n", ch1);
//	return 0;
//}

//enum sex
//{
//	male,
//	female,
//	secret
//};
//int main()
//{
//	enum sex s1 = male;
//	//enum sex s2 = 59;
//	enum sex s2 = (enum sex)59;
//	int a = male;
//	printf("%d\n", s1);
//	printf("%d\n", s2);
//	printf("%d\n", a);
//	return 0;
//}

//struct stu
//{
//	int age;
//	char name[20];
//	double score;
//};
//int main()
//{
//	struct stu s1 = { 12,"zhansan", 88.8 };
//	struct stu s2;
//	s2 = { 15,"lisi",68.8 };
//	return 0;
//}