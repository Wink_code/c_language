#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

struct Stu
{
	int age;
	char name[20];
};

int int_cmp(const void* value1, const void* value2)
{
	return *(int*)value1 - *(int*)value2;
}

int cmp_by_age(const void* value1, const void* value2)
{
	return ((struct Stu*)value1)->age - ((struct Stu*)value2)->age;
}

int cmp_by_name(const void* value1, const void* value2)
{
	return strcmp( (*(struct Stu*)value1).name , (*(struct Stu*)value2).name);
}

void Swap(void* p1, void* p2, int size)
{
	int i = 0;
	//交换两个数的每一个字节
	for (i = 0; i < size; i++)
	{
		char tmp = *((char*)p1 + i);
		*((char*)p1 + i) = *((char*)p2 + i);
		*((char*)p2 + i) = tmp;
	}
}

My_sort(void* base, int count, size_t size, int (*cmp)(const void* value1, const void* value2))
{
	int i = 0;
	for (i = 0; i < count - 1; i++)
	{
		int j = 0;
		for (j = 0; j < count - 1 - i; j++)
		{
			//arr[j] arr[j+1]
			//用char型指针最为合适
			if (cmp((char*)base + size * j, (char*)base + size * (j+1)) > 0)
			{
				//交换,将被交换数的地址传过去
				Swap((char*)base + size * j, (char*)base + size * (j + 1), size);
			}
		}
	}
}

void test1()
{
	int arr1[] = { 3,1,5,2,4,9,7,8,6,10 };
	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
	My_sort(arr1, sz1, sizeof(arr1[0]), int_cmp);
	int i = 0;
	for (i = 0; i < sz1; i++)
	{
		printf("%d ", arr1[i]);
	}
}

void test2()
{
	struct Stu arr2[] = { {18,"zhangsan"}, {37, "lisi"}, {23, "wangwu"} };
	int sz2 = sizeof(arr2) / sizeof(arr2[0]);
	//My_sort(arr2, sz2, sizeof(arr2[0]), cmp_by_age);
	My_sort(arr2, sz2, sizeof(arr2[0]), cmp_by_name);
	int i = 0;
	for (i = 0; i < sz2; i++)
	{
		printf("%d %s", arr2[i].age, arr2[i].name);
		printf("\n");
	}
}

int main()
{
	//test1();
	test2();
	return 0;
}

//#include<string.h>
//int main()
//{
//	char arr1[] = { '0', '0', '0' };
//	char arr2[4] = { '0', '0', '0' }; 
//
//	printf("%d\n", strlen(arr1));
//	printf("%d\n", sizeof(arr1));
//	printf("%d\n", strlen(arr2));
//	printf("%d\n", sizeof(arr2));
//	return 0;
//}