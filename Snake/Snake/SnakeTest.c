#define _CRT_SECURE_NO_WARNINGS 1
#include"Snake.h"

int main()
{
	srand((unsigned int)time(NULL));
	setlocale(LC_ALL, "");//适应本地中文环境

	int ch = 0;
	do
	{
		Snake snake = { 0 };//创建贪吃蛇
		//游戏开始前的初始化
		GameStart(&snake);

		////玩游戏
		GameRun(&snake);

		////游戏结束，善后工作
		GameEnd(&snake);
		SetPos(20, 15);
		printf("还要再来一局吗？（Y/N）");
		ch = getchar();
		getchar();//吸收换行
	} while (ch == 'Y' || ch == 'y');

	SetPos(0, 30);
	return 0;
}