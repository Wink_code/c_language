#define _CRT_SECURE_NO_WARNINGS 1

#include<locale.h>
#include<stdio.h>
#include<Windows.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>


#define KEY_PRESS(VK) ((GetAsyncKeyState(VK)&0x1) ? 1 : 0)

#define WALL L'□'//墙体
#define BODY L'●'//蛇身
#define FOOD L'★'//食物

//蛇初始位置
#define POS_X 24
#define POS_Y 12

//蛇的方向
//上、下、左、右
enum Direction
{
	
	UP = 1,
	DOWN,
	LEFT,
	RIGHT
};

//游戏的状态
enum GameState
{
	OK,//正常
	KILL_BY_WALL,//撞墙
	KILL_BY_SELF,//撞到自己
	END//结束
};

//蛇节点的定义
typedef struct SnakeNode
{
	int x;
	int y;
	struct SnakeNode* next;//指向下一个节点
}SnakeNode,*pSnakeNode;//同时定义一个指向该节点的指针

//整条蛇
typedef struct Snake
{
	pSnakeNode pSnake;  //指向整条蛇的指针
	pSnakeNode pFood;   //指向食物的指针

	enum Direction Dire;  //蛇的方向
	enum GameState State; //蛇的状态

	int SleepTime; //蛇的休眠时间；休眠越短，蛇移动的越快
	int Score;//游戏得分
	int FoodScore;//一个食物的分数
}Snake,* pSnake;

//设置光标位置
void SetPos(int x, int y);

//设置颜色
void SetColor(int HeadColor);

//游戏准备
void GameStart(pSnake psnake);

//初始化蛇
void InitSnake(pSnake psnake);

//创建食物
void CreatFood(pSnake ps);

//打印蛇
void PrintSnake(pSnake ps);

//玩游戏
void GameRun(pSnake ps);

//移动蛇
void SnakeMove(pSnake ps);

//判断节点是否是食物
int JudgeNext(pSnake ps, pSnakeNode pNext);

//是食物，吃掉，长度增加
void EatFood(pSnake ps, pSnakeNode pNext);

//不是食物
void NoFood(pSnake ps, pSnakeNode pNext);

//撞墙
void KillByWall(pSnake ps);

//撞到自己
void KillBySelf(pSnake ps);

//游戏结束
void GameEnd(pSnake ps);
