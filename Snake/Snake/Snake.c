#define _CRT_SECURE_NO_WARNINGS 1

#include"Snake.h"

//隐藏光标
void HideCurSor()
{
	CONSOLE_CURSOR_INFO cursor = { 0 };
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleCursorInfo(handle, &cursor);
	cursor.bVisible = false;
	SetConsoleCursorInfo(handle, &cursor);
}

//设置光标位置
void SetPos(int x, int y)
{
	COORD pos = { x,y }; //要设置的位置
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);//哪个设备
	SetConsoleCursorPosition(handle, pos);//设置
}

void Welcome()
{
	//显示欢迎语一
	SetPos(38, 15);
	printf("欢迎来到贪吃蛇小游戏！");
	SetPos(40, 24);
	system("pause");
	system("cls");//清屏
	//提示语
	SetPos(25, 15);
	printf("按 ↑、↓、←、→ 控制蛇蛇的移动，F1为加速，F2为减速，");
	SetPos(25, 16);
	printf("加速将能得到更高的分数");
	SetPos(40, 24);
	system("pause");
}

//打印地图
void DrawMap()
{
	system("cls");
	SetColor(6);
	SetPos(0, 0);
	//上
	for (int i = 0; i <= 56; i += 2)
		wprintf(L"%lc", WALL);
	//左、右
	for (int i = 1; i <= 25; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc", WALL);
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (int i = 0; i <= 56; i += 2)
		wprintf(L"%lc", WALL);  
	SetColor(10);
}

//显示提示信息
void HelpInfo()
{
	SetPos(65, 18);
	printf("不能穿墙，不能碰到自己,");
	SetPos(65, 19);
	printf("按 ↑、↓、←、→ 控制蛇蛇的移动,");
	SetPos(65, 21);
	printf("F1为加速，F2为减速，");
	SetPos(65, 22);
	printf("ESC：退出游戏 SPACE：暂停");
}

//窗口相关设置
void ScreenPrepare()
{
	//设置窗体大小
	system("mode con cols=100 lines=40");
	system("title 贪吃蛇小游戏");
	//隐藏光标
	HideCurSor();
	//打印欢迎语
	Welcome();
	//打印地图
	DrawMap();
	//显示提示信息
	HelpInfo();
}

//设置颜色
void SetColor(int HeadColor)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), HeadColor);
}

//打印蛇
void PrintSnake(pSnake ps)
{
	pSnakeNode cur = ps->pSnake;
	int flag = 1;
	while (cur)
	{
		if (flag == 1)
		{
			//为蛇头设置颜色
			SetColor(12);
		}
		SetPos(cur->x, cur->y);//设置每个节点的位置
		wprintf(L"%lc", BODY);
		cur = cur->next;
		flag = 0;
		SetColor(10);
	}
}

//初始化蛇
void InitSnake(pSnake ps)
{
	int i = 0;
	pSnakeNode cur = NULL;
	//蛇的长度开始为3
	for (i = 0; i < 5; i++)
	{
		cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("InitSnake():malloc");
			return;
		}
		cur->next = NULL;
		cur->x = POS_X + i * 2;  //节点横坐标依次增加2
		cur->y = POS_Y;

		//头插法将节点相连
		if (ps->pSnake == NULL)
		{
			ps->pSnake = cur;
		}
		else
		{
			cur->next = ps->pSnake;
			ps->pSnake = cur;
		}
	}
	//打印蛇
	PrintSnake(ps);
	//初始化蛇的其它信息
	ps->Dire = RIGHT;
	ps->FoodScore = 10;
	ps->SleepTime = 200;
	ps->State = OK;
	ps->Score = 0;
}

//创建食物
void CreatFood(pSnake ps)
{
	int x = 0;
	int y = 0;
	//食物坐标应该在墙体内
again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 25 + 1;
	} while (x % 2 != 0);

	//食物坐标不与蛇重叠
	pSnakeNode cur = ps->pSnake;
	while (cur)
	{
		if (x == cur->x && y == cur->y)
		{
			goto again;
		}
		cur = cur->next;
	}
	//根据坐标创建食物
	pSnakeNode pFood = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pFood == NULL)
	{
		perror("CreatFood():malloc()");
		return;
	}
	//打印食物
	pFood->x = x;
	pFood->y = y;
	SetColor(14);
	SetPos(pFood->x, pFood->y);
	wprintf(L"%lc", FOOD);
	SetColor(10);
	ps->pFood = pFood;
}

//游戏准备
void GameStart(pSnake psnake)
{
	//设置好窗口、地图
	ScreenPrepare();
	//初始化蛇
	InitSnake(psnake);
	//创建食物
	CreatFood(psnake);
}

//暂停
void pause()
{
	while (1)
	{
		//一直休眠，直到再次按空格键
		Sleep(200);
		if (KEY_PRESS(VK_SPACE))
		{
			break;
		}
	}
}

//玩游戏
void GameRun(pSnake ps)
{
	do
	{
		//显示分数
		SetColor(12);
		SetPos(65, 12);
		printf("目前得分：%-5d",ps->Score);
		SetColor(10);
		SetPos(65, 13);
		printf("每个食物：%2d分",ps->FoodScore);
		//检测按键
		if (KEY_PRESS(VK_UP) && ps->Dire != DOWN)
		{
			//按上键，且蛇的方向不能向下
			ps->Dire = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->Dire != UP)
		{
			//按下键，且蛇的方向不能向上
			ps->Dire = DOWN;
		}
		else if (KEY_PRESS(VK_LEFT) && ps->Dire != RIGHT)
		{
			//按左键，且蛇的方向不能向右
			ps->Dire = LEFT;
		}
		else if (KEY_PRESS(VK_RIGHT) && ps->Dire != LEFT)
		{
			//按右键，且蛇的方向不能向左
			ps->Dire = RIGHT;
		}
		else if (KEY_PRESS(VK_ESCAPE))
		{
			//按ESC,退出游戏
			ps->State = END;
		}
		else if (KEY_PRESS(VK_SPACE))
		{
			//按空格，暂停
			pause();
		}
		else if (KEY_PRESS(VK_F1))
		{
			//按F1加速,即睡眠时间变短
			
			//休眠时间不能是负数,最快就是休眠30ms
			if (ps->SleepTime >= 50)
			{
				ps->SleepTime -= 20;
				//速度变快，食物的分数变高
				ps->FoodScore += 2;
			}
		}
		else if (KEY_PRESS(VK_F2))
		{
			//F2减速，睡眠时间变长

			//食物的分数不能减到负数,最多减为1分
			if (ps->FoodScore >= 3)
			{
				ps->SleepTime += 20;
				ps->FoodScore -= 2;
			}
		}
		//按照蛇的睡眠时间，真正实现休眠
		Sleep(ps->SleepTime);

		//休眠后，蛇要移动
		SnakeMove(ps);

	} while (ps->State == OK);//只有蛇的状态为OK，才能继续检测按键
}

//移动蛇
void SnakeMove(pSnake ps)
{
	//先产生节点
	pSnakeNode pNext = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pNext == NULL)
	{
		perror("SnakeMove():malloc()");
		return;
	}
	//根据蛇的方向，设定节点的位置
	switch (ps->Dire)
	{
	case UP:
		//蛇的方向向上，节点应该在当前蛇头的上方;横坐标不变，纵坐标减1
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y - 1;
		break;
	case DOWN:
		//蛇的方向向下，节点应该在当前蛇头的下方;横坐标不变，纵坐标加1
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y + 1;
		break;
	case LEFT:
		//蛇的方向向左，节点应该在当前蛇头的左边;纵坐标不变，横坐标减2
		pNext->x = ps->pSnake->x - 2;
		pNext->y = ps->pSnake->y;
		break;
	case RIGHT:
		//蛇的方向向右，节点应该在当前蛇头的右方;纵坐标不变，横坐标加2
		pNext->x = ps->pSnake->x + 2;
		pNext->y = ps->pSnake->y;
		break;
	}

	//判断下一个节点是否是食物
	if (JudgeNext(ps, pNext))
	{
		//是食物，吃掉，长度增加
		EatFood(ps, pNext);
	}
	else
	{
		//不是食物，吃掉，长度不增加
		NoFood(ps, pNext);
	}

	//撞墙
	KillByWall(ps);

	//撞自己
	KillBySelf(ps);
}

//判断节点是否是食物
int JudgeNext(pSnake ps, pSnakeNode pNext)
{
	return ((ps->pFood->x == pNext->x) && (ps->pFood->y == pNext->y));
}

//是食物，吃掉，长度增加
void EatFood(pSnake ps, pSnakeNode pNext)
{
	//吃掉食物，头插法将节点插入
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;
	//打印蛇
	PrintSnake(ps);
	//加分
	ps->Score +=  ps->FoodScore;
	//释放食物节点
	free(ps->pFood);
	//再次创建食物
	CreatFood(ps);
}

//不是食物
void NoFood(pSnake ps, pSnakeNode pNext)
{
	//头插法连接
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;
	//删除蛇尾
	pSnakeNode cur = ps->pSnake;
	//找到蛇尾的前一个节点
	while (cur->next->next)
	{
		cur = cur->next;
	}
	pSnakeNode del = cur->next;
	//将蛇尾打印为空格
	SetPos(del->x, del->y);
	printf("  ");
	//释放蛇尾节点
	free(del);
	cur->next = NULL;
	//打印蛇
	PrintSnake(ps);
}

//撞墙
void KillByWall(pSnake ps)
{
	if (ps->pSnake->x == 0 ||
		ps->pSnake->x == 56 ||
		ps->pSnake->y == 0 ||
		ps->pSnake->y == 26)
	{
		ps->State = KILL_BY_WALL;
	}
}

//撞到自己
void KillBySelf(pSnake ps)
{
	pSnakeNode cur = ps->pSnake->next;
	while (cur)
	{
		if (cur->x == ps->pSnake->x && cur->y == ps->pSnake->y)
		{
			ps->State = KILL_BY_SELF;
		}
		cur = cur->next;
	}
}

//游戏结束
void GameEnd(pSnake ps)
{
	SetPos(20, 13);
	SetColor(12);

	switch (ps->State)
	{
	case END:
		printf("您结束了游戏");
		break;
	case KILL_BY_SELF:
		printf("很遗憾！您撞到了自己");
		break;
	case KILL_BY_WALL:
		printf("很遗憾！您撞墙了");
		break;
	}
	pSnakeNode cur = ps->pSnake;
	while (cur)
	{
		pSnakeNode del = cur;
		cur = cur->next;
		free(del);
	}
	ps = NULL;
	SetColor(10);

}