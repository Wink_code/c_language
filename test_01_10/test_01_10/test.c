#define _CRT_SECURE_NO_WARNINGS 1

//牛客 JZ53 数字在升序数组中出现的次数
int GetNumberOfK(int* nums, int numsLen, int k) {
    // write code here
    int left = 0;
    int right = numsLen - 1;
    int mid = 0;
    while (left <= right)
    {
        mid = (left + right) / 2;
        if (nums[mid] < k)
        {
            left = mid + 1;
        }
        else if (nums[mid] > k)
        {
            right = mid - 1;
        }
        else
        {
            right--;
        }
    }
    int start = left;
    left = 0;
    right = numsLen - 1;
    while (left <= right)
    {
        mid = (left + right) / 2;
        if (nums[mid] < k)
        {
            left = mid + 1;
        }
        else  if (nums[mid] > k)
        {
            right = mid - 1;
        }
        else
        {
            left++;
        }
    }
    int end = left;
    return end - start;
}

//leetcode 349. 两个数组的交集
#include<stdlib.h>
//void bubble_sort(int* arr, int size)
//{
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < size - 1; i++)
//    {
//        for (j = 0; j < size - 1 - i; j++)
//        {
//            if (arr[j] > arr[j + 1])
//            {
//                int tmp = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tmp;
//            }
//        }
//    }
//}
//
int cmp(const int* a, const int* b)
{
    return *a - *b;
}

int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) {
    int max = nums1Size > nums2Size ? nums1Size : nums2Size;
    int* ret = (int*)malloc(sizeof(int) * max);
    if (ret == NULL)
    {
        return NULL;
    }
    //先将两数组排序；
    // bubble_sort(nums1,nums1Size);
    // bubble_sort(nums2, nums2Size);
    qsort(nums1, nums1Size, sizeof(int), cmp);
    qsort(nums2, nums2Size, sizeof(int), cmp);
    //使用双指针比较数组元素
    int* num1 = nums1;
    int* num2 = nums2;
    int i = 0;
    int j = 0;
    int k = 0;
    int prev = -1;//由于 0 <= num <= 1000
    while (i < nums1Size && j < nums2Size)
    {
        if ((*num1 == *num2) && (*num1 != prev))
        {
            prev = *num1;
            ret[k++] = *num1;
        }
        if (*num1 < *num2)
        {
            num1++;
            i++;
        }
        else if (*num1 > *num2)
        {
            num2++;
            j++;
        }
        else
        {
            num1++;
            num2++;
            j++;
            i++;
        }
    }
    *returnSize = k;
    return ret;
}

//leetcode 747. 至少是其他数字两倍的最大数
int dominantIndex(int* nums, int numsSize) {
    int max = *nums;
    int index = 0;
    for (int i = 1; i < numsSize; i++)
    {
        if (max < nums[i])
        {
            max = nums[i];
            index = i;
        }
    }
    for (int i = 0; i < numsSize; i++)
    {
        if (i == index)  //最大数不和自己比较
        {
            continue;
        }
        if (max < (2 * nums[i]))  //不是每个数的 至少 两倍
        {
            return -1;
        }
    }
    return index;
}

#include<stdio.h>

//int Max(int a, int b)
//{
//    return a > b ? a : b;
//}
//int main()
//{
//    int a = 12;
//    int b = 21;
//    int c = 4;
//    int max = Max(a, b);
//    max = Max(max, c);
//    printf("%d\n", max);
//    return 0;
//}

//int func(int n)
//{
//    if (n == 0)
//    {
//        return 1;
//    }
//    else
//    {
//        return n * func(n - 1);
//    }
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int sum = 0;
//    /*for (int i = 1; i <= n; i++)
//    {
//        sum *= i;
//    }*/
//    int ret = 1;
//    for (int i = 1; i <= n; i++)
//    {
//        ret *= i;
//        sum += ret;
//    }
//    printf("%d\n", sum);
//    
//    return 0;
//}
//#define M 4
//int main()
//{
//    int arr[M][2] = { 0 };
//    int count = 0;
//    for (int i = 0; i < M; i++)
//    {
//        scanf("%d %d", &arr[i][0], &arr[i][1]);
//    }
//    for (int i = 0; i < M; i++)
//    {
//        if (arr[i][1] > 80)
//        {
//            count++;
//            printf("学号为：%d,分数是：%d\n", arr[i][0], arr[i][1]);
//        }
//    }
//    printf("人数：%d\n", count);
//
//    return 0;
//}

//int main()
//{
//    for (int i = 2000; i <= 2500; i++)
//    {
//        //if ((i % 4 != 0 || i % 100 == 0 && i % 400 != 0))
//        if (!(i % 4 == 0 && i % 100 != 0 || i % 400 == 0))
//        {
//            printf("%d ",i);
//        }
//    }
//    return 0;
//}

//int main()
//{
//    /*int i = 0;
//    double flag = 1;
//    double sum = 0;
//    for (i = 1; i <= 100; i++)
//    {
//        sum += flag / i;
//        flag = -flag;
//    }
//    printf("%lf\n", sum);*/
//
//    int i = 0;
//    int j = 0;
//    double sum = 0;
//    double flag = 1;
//    int tmp = 0;
//    for (i = 1; i <= 5; i++)
//    {
//        tmp = i;
//        for (j = 1; j < i; j++)
//        {
//            tmp = tmp * 10 + i;
//        }
//        printf("%d\n", tmp);
//        sum += flag / tmp;
//        flag = -flag;
//    }
//    printf("%lf\n", sum);
//    return 0;
//}

//int main()
//{
//    int i = 0;
//    int j = 0;
//    for (i = 101; i <= 1000; i+=2)
//    {
//        for (j = 2; j*j <= i; j++)
//        {
//            if (i % j == 0)
//            {
//                break;
//            }
//        }
//        if (j * j> i)
//        {
//            printf("%d ", i);
//        }
//    }
//    return 0;
//}

//int isPrime(int n)
//{
//    int i = 0;
//    for (i = 2; i < n; i++)
//    {
//        if (n % i == 0)
//        {
//            return 0;
//        }
//    }
//    return 1;
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    for (i = 2; i <= n; i++)
//    {
//        if (isPrime(i) && n % i == 0)
//        {
//            printf("%d ", i);
//            n /= i;
//            /*while (n % i == 0)
//            {
//                printf("%d ", i);
//                n /= i;
//            }*/
//            if (n == 1)
//            {
//                break;
//            }
//        }
//    }
//    return 0;
//}

//int main()
//{
//    int arr[] = { 43,67,111,87,23,56,69,27 };
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    int max = arr[0];
//    for (int i = 1; i < sz; i++)
//    {
//        if (max < arr[i])
//        {
//            max = arr[i];
//        }
//    }
//    printf("%d\n", max);
//    return 0;
//}

//void swap(int* n, int* m)
//{
//    int tmp = *n;
//    *n = *m;
//    *m = tmp;
//}

//void Max(int* a, int* b, int* c)
//{
//    if (*a < *b)
//    {
//        swap(a, b);
//    }
//    if (*a < *c)
//    {
//        swap(a, c);
//    }
//    if (*b < *c)
//    {
//        swap(b, c);
//    }
//    
//}
//
//void Max2(int a, int b, int c)
//{
//    if (a < b)
//    {
//        swap(&a, &b);
//    }
//    if (a < c)
//    {
//        swap(&a, &c);
//    }
//    if (b < c)
//    {
//        swap(&b, &c);
//    }
//    printf("%d %d %d\n", a, b, c);
//
//}
//
//int main()
//{
//    int a = 12;
//    int b = 78; 
//    int c = 99;
//    Max(&a, &b, &c);
//    Max2(a, b, c);
//    printf("%d %d %d\n", a, b, c);
//    return 0;
//}

//int judge(int n)
//{
//    if (n % 3 == 0 && n % 5 == 0)
//    {
//        return 1;
//    }
//    return 0;
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    if (judge(n))
//    {
//        printf("YES\n");
//    }
//    else
//    {
//        printf("NO\n");
//    }
//    return 0;
//}

//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int min = n < m ? n : m;
//    int mul = n * m;
//    int i = 0;
//    for ( i = min; i > 0; i--)
//    {
//        if (n % i == 0 && m % i == 0)
//        {
//            printf("%d\n", i);
//            break;
//        }
//    }
//    printf("%d\n", mul / i);
//    return 0;
//}

//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int mul = n * m;
//    int i = 0;
//    while (n % m != 0)
//    {
//        int tmp = n % m;
//        n = m;
//        m = tmp;
//    }
//    printf("%d\n", m);
//    printf("%d\n", mul / m);
//    return 0;
//}

//int func(int n, int m)
//{
//    if (n < m)
//    {
//        return func(n, m - n);
//    }
//    else if (n > m)
//    {
//        return func(m, n - m);
//    }
//    else
//    {
//        return n;
//    }
//}

//int func(int n, int m)
//{
//    if (m == 0)
//    {
//        return n;
//    }
//    else
//    {
//        return func(m, n % m);
//    }
//}
//
//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int mul = n * m;
//    int ret = func(n, m);
//    printf("%d\n",  ret);
//    printf("%d\n", mul / ret);
//    return 0;
//}

#include<math.h>
//int main()
//{
//    double a = 0;
//    double b = 0;
//    double c = 0;
//    scanf("%lf %lf %lf", &a, &b, &c);
//    double flag = b * b - 4 * a * c;
//    double a1 = 0;
//    double a2 = 0;
//    if (flag > 0)
//    {
//        a1 = (-b + sqrt(flag)) / 2 * a;
//        a2 = (-b - sqrt(flag)) / 2 * a;
//        printf("a1=%lf a2=%lf\n", a1, a2);
//    }
//    else if (flag == 0)
//    {
//        a1 = a2 = -b / 2 * a;
//        printf("a1=a2=%lf\n", a1);
//    }
//    else
//    {
//        printf("方程无解\n");
//    }
//
//    return 0;
//}