#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

//leetcode-1929
int* getConcatenation(int* nums, int numsSize, int* returnSize)
{
    int* tmp = NULL;
    tmp = realloc(nums, 2 * numsSize * sizeof(int));
    if (tmp != NULL)
    {
        nums = tmp;
        tmp = NULL;
    }
    else
    {
        perror("realloc");
        return NULL;
    }
    for (int i = 0; i < numsSize; i++)
    {
        nums[i] = nums[i];
        nums[i + numsSize] = nums[i];
    }
    *returnSize = 2 * numsSize;
    return nums;
}

//使用mallco模拟实现二维数组
//int main()
//{
//	int** p =(int**)malloc(3 * sizeof(int*));
//	if (p == NULL)
//	{
//		perror("malloc-1");
//		return 1;
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		*(p + i) = malloc(5 * sizeof(int));
//		if ((p + i) == NULL)
//		{
//			perror("malloc-2");
//			return 1;
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			p[i][j] = i * j;
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		free(p[i]);
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//4.realloc malloc
//int main()
//{
//	int* p = realloc(NULL, 5 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("realloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//3.realloc
//int main()
//{
//	int* p = (int*)malloc(10 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	//reallloc
//	int* ptr = (int*)realloc(p, sizeof(int) * 20);
//	if (ptr != NULL)
//	{
//		p = ptr;
//		ptr = NULL;
//	}   
//	else 
//	{
//		perror("realloc");
//		return 1;
//	}
//	for (i = 10; i < 20; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//2.calloc
//int main()
//{
//	int* p = (int*)calloc(10 , sizeof(int));
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//1.malloc
//int main()
//{
//	int*p = (int*)malloc(10 * sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//void convert(int n)
//{
//    if (n / 10 != 0)
//    {
//        convert(n / 10);
//    }
//    putchar(n % 10 + '0');
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    if (n < 0)
//    {
//        putchar('-');
//        n = -n;
//    }
//    convert(n);
//    return 0;
//}

//int sum_day(int year, int month, int day)
//{
//    int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//    int sum = day;
//
//    for (int i = 1; i < month; i++)
//    {
//        sum += arr[i];
//    }
//    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//    {
//        sum++;
//    }
//    return sum;
//}
//
//int main()
//{
//    int year, month, day;
//    scanf("%d %d %d", &year, &month, &day);
//    int ret = sum_day(year, month, day);
//    printf("%d\n", ret);
//    return 0;
//}
