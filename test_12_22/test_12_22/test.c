#define _CRT_SECURE_NO_WARNINGS 1
#define SETBIT(x,n) (x |= 1<<(n-1)) //将指定位置为1
#define CLEBIT(x,n) (x &= ~(1<<(n-1)))//将指定位置为0
#include<stdio.h>
void ShowBit(int x)
{
	int count = sizeof(x) * 8 - 1;
	while (count>=0)
	{
		if (x &(1<<count))
		{
			printf("1 ");
		}
		else
		{
			printf("0 ");
		}
		count--;
	}
	printf("\n");
}
int main()
{
	int x = 5;
	int y = 0xFFFFFFFF;
	ShowBit(x);
	SETBIT(x, 5);
	ShowBit(x);

	ShowBit(y);
	CLEBIT(y, 5);
	ShowBit(y);
	return 0;
}

//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	double sum = 0;
//	double ret = 1;
//	for (i = 1; i <= n; i++)
//	{
//		ret = ret * i;
//		sum += ret; 
//	}
//	//n个数
//	//for (i = 1; i <= n; i++)
//	//{
//	//	int j = 0;
//	//	int ret = 1;
//	//	//求每个数的阶乘
//	//	for (j = 1; j <= i; j++)
//	//	{
//	//		ret = ret * j;
//	//	}
//	//	sum += ret;
//	//}
//	printf("%lf\n", sum);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		int j = 0;
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//		}
//		if (j >= i)
//		{
//			printf("%d ", i);
//			count++;
//		}
//		if (count % 10 == 0)
//		{
//			printf("\n");
//		}
//	}
//	return 0;
//}
//#include<math.h>
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 101; i <= 200; i+=2)
//	{
//		int j = 0;
//		for (j = 2; j <=sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//		}
//		if (j > sqrt(i))
//		{
//			printf("%d ", i);
//			count++;
//		}
//		if (count % 10 == 0)
//		{
//			printf("\n");
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int a, b, c;
//	int sum = 0;
//	for (i = 100; i < 1000; i++)
//	{
//		a = i % 10;
//		b = i / 10 % 10;
//		c = i / 100;
//		sum = a * a * a + b * b * b + c * c * c;
//		if (sum == i)
//		{
//			printf("%d ", sum);
//		}
//	}
//	return 0;
//}