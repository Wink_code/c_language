#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int Max2(int x, int y)
//{
//	return x > y ? x : y;
//}
//
//int Max_1(int a, int b, int c, int d)
//{
//	int m = 0;
//	m = Max2(a, b);
//	m = Max2(m, c);
//	m = Max2(m, d);
//	return m;
//}
//int main()
//{
//	int a, b, c, d, max;
//	scanf("%d %d %d %d", &a, &b, &c, &d);
//	max = Max_1(a,b,c,d);
//	printf("%d\n",max);
//	return 0;
//}

//#include<stdio.h>
//
//void move(char x, char y)
//{
//	printf("%c--->%c\n", x, y);
//}
//void hanoi(int n, char A, char B, char C)
//{
//	//A盘只剩一个，直接移动到C盘
//	if (n == 1)
//	{
//		move(A, C);
//	}
//	else
//	{
//		//n-1个，从A盘借助C移到B盘
//		hanoi(n - 1, A, C, B);
//		//第n个从A盘移动到C盘
//		move(A, C);
//		//n-1个，从B盘借助A移到C盘
//		hanoi(n - 1, B, A, C);
//	}
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	hanoi(n, 'A', 'B', 'C');
//	return 0;
//}

////全局变量
//int Max = 0;
//int Min = 0;
//
//float average(int arr[], int n)
//{
//	int i = 0;
//	float sum = 0;
//	Max = Min = arr[0];
//	for (i = 0; i < n; i++)
//	{
//		if (arr[i] > Max)
//		{
//			Max = arr[i];
//		}
//		else if (arr[i] < Min)
//		{
//			Min = arr[i];
//		}
//		sum += arr[i];
//	}
//	return sum / n;
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	float ave = average(arr, 10);
//	printf("max=%d min=%d ave=%f\n", Max, Min, ave);
//	return 0;
//}

//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int arr[n * m];
//    int arr2[n][m];
//    for (int i = 0; i < n * m; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int count = 0;
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            arr2[i][j] = arr[count++];
//        }
//    }
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            printf("%d ", arr2[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n][n];
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            if (j == 0 || i == j)
//            {
//                arr[i][j] = 1;
//            }
//            else
//            {
//                arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//            }
//        }
//    }
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            if (i >= j)
//            {
//                printf("%5d", arr[i][j]);
//            }
//        }
//        printf("\n");
//    }
//}