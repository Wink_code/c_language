#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>


void (*signal(int, void(*)(int)))(int);

typedef int* int_p;
typedef int(*parr_t)[5];


typedef void (*pf_t)();
pf_t signal(int, pf_t);


int main()
{
	(* (void (*)()) 0)();
	return 0;
}
//int add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int (*p1)(int, int) = &add;
//	int ret1 = add(3, 5);
//	printf("%d\n", ret1);  //8
//
//	int ret2 = (*p1)(3, 5);
//	printf("%d\n", ret2);  //8
//
//	int (*p2)(int, int) = add;
//	int ret3 = (*p2)(4, 6);
//	printf("%d\n", ret3);  //10
//
//	int ret4 = p2(4, 6);
//	printf("%d\n", ret4);   //10
//	return 0;
//}

//void func(int (*arr)[5], int row, int col)
//{
//	int i = 0;
//	for (i = 0; i < row; i++)
//	{
//		int j = 0;
//		for (j = 0; j < col; j++)
//		{
//			printf("%d ",*(*(arr + i) + j));
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5}, {2,3,4,5,6}, {3,4,5,6,7} };
//	func(arr, 3, 5);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int(*parr)[10] = &arr;  //数组指针变量parr
//	return  0;
//}

//int main()
//{
//	char str1[] = "hello world.";
//	char str2[] = "hello world.";
//	const char* str3 = "hello world.";
//	const char* str4 = "hello world.";
//	if (str1 == str2)
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 are not same\n");
//
//	if (str3 == str4)
//		printf("str3 and str4 are same\n");
//	else
//		printf("str3 and str4 are not same\n");
//
//	return 0;
//}


//int main()
//{
//	char* pc = "abcdef";
//	printf("%c\n", "abcdef"[0]);   //a
//	printf("%c\n", pc[0]);		   //a
//	return 0;
//}

//int main()
//{
//	char ch = 'c';
//	char* pc = &ch;
//	*pc = 'a';
//	printf("%c\n", ch);
//	return 0;
//}

//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//	//数组名是数组首元素的地址，类型是int*，可以存放在数组指针arr中
//	int* arr[3] = { arr1, arr2, arr3 };
//
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//			//printf("%d ", *(*(arr + i) + j));
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	int** pp = &p;
//	**pp = 20;
//	printf("%d\n", a);
//	return 0;
//}


//int main()
//{
//	int arr[3][2] = { {1,2}, {3,4}, {5,6} };
//	int(*p)[2] = arr;
//	//printf("%d\n", *arr);
//	printf("%d\n", *(*p));
//	printf("%d\n", *(*(p + 1)));
//	printf("%d\n", *(p[1]));
//	printf("%d\n", *(p[0] + 1));
//	return 0;
//}
