#define _CRT_SECURE_NO_WARNINGS 1


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
//void qsort(void* base, 
//			 size_t num, 
//			 size_t size,
//			 int (*compar)(const void*, const void*));

//使用qsort 排序结构体类型的数据
struct Stu
{
	int age;
	char name[20];
};
int cmp_by_age(const void* p1, const void* p2)
{
	//结构体类型的数据，直接访问
	return (*(struct Stu*)p1).age - (*(struct Stu*)p2).age;
}
int cmp_by_name(const void* name1, const void* name2)
{
	//结构体类型的数据，间接访问
	return strcmp(((struct Stu*)name1)->name, ((struct Stu*)name2)->name);
}
int main()
{
	struct Stu arr[] = { {12,"zhangsan"}, {34, "lisi"}, { 22, "wangwu"} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	printf("按年龄排序：\n");
	qsort(arr, sz, sizeof(arr[0]), cmp_by_age);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d--%d %s\n", i + 1, arr[i].age, arr[i].name);
	}

	printf("\n按姓名排序：\n");
	qsort(arr, sz, sizeof(arr[0]), cmp_by_name);
	for (i = 0; i < sz; i++)
	{
		printf("%d--%d %s\n", i + 1, arr[i].age, arr[i].name);
	}
	return 0;
}

//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = 0;
//				tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//void Find_sole_dog(int arr[], int sz, int dog[])
//{
//	int i = 0;
//	int j = 0;
//	while (i < sz)
//	{
//		//两数相等，走两步
//		if (arr[i] == arr[i + 1])
//		{
//			i += 2;
//		}
//		else
//		{
//			//两数不等，记录，走一步
//			dog[j] = arr[i];
//			j++;
//			i++;
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int dog[2] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	Find_sole_dog(arr, sz, dog);
//	for (int i = 0; i < 2; i++)
//	{
//		printf("%d ", dog[i]);
//	}
//	return 0;
//}

//使用qsort 排序整型数据
//int int_cmp(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *((int*)p2);
//}
//
//int main()
//{
//	int arr[] = { 2, 4, 5, 1, 12, 23, -1, 34, -12, 78 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), int_cmp);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
////void calc(int (*p)(int, int))
////{
////	int x = 0;
////	int y = 0;
////	int ret = 0;
////	printf("请输入两个操作数：\n");
////	scanf("%d %d", &x, &y);
////	ret = p(x, y);
////	printf("ret = %d\n", ret);
//// }
//int main()
//{
//	int input = 0;
//	int (*arr[])(int, int) = { NULL, Add, Sub, Mul, Div };
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do
//	{
//		printf("**** 1. add  ***** 2. sub *****\n");
//		printf("**** 3. mul  ***** 4. div *****\n");
//		printf("************ 0. exit **********\n");
//		printf("请选择：\n");
//		scanf("%d", &input);
//		if (input <= 4 && input > 0)
//		{
//			printf("请输入两个操作数：\n");
//			scanf("%d %d", &x, &y);
//			ret = (*arr[input])(x, y);
//			printf("ret = %d\n", ret);
//		}
//		else if (input == 0)
//		{
//			printf("退出计算器\n");
//		}
//		else
//		{
//			printf("选择错误，请重新选择\n");
//
//		}
//	} while (input);
//	return 0;
//}



//int main()
//{
//	
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do
//	{
//		printf("**** 1. add  ***** 2. sub *****\n");
//		printf("**** 3. mul  ***** 4. div *****\n");
//		printf("************ 0. exit **********\n");
//		printf("请选择：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Add(x,y);
//			printf("ret = %d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default :
//			printf("选择错误，请重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//int main()
//{
//	int (*(*p)[10])(int*);
//	return 0;
//}


//int main()
//{
//    int year = 0;
//    int month = 0;
//    int per_month[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//    int day = 0;
//    while (scanf("%d %d", &year, &month) != EOF)
//    {
//        day = per_month[month];
//        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//        {
//            if (month == 2)
//            {
//                day++;
//            }
//        }
//        printf("%d\n", day);
//    }
//    return 0;
//}