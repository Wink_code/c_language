#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void func(int n)
{
	if (n >= 10)
	{
		func(n / 10);
	}
	printf("%d ", n % 10);
}

int main()
{
	int n = 1234;
	func(n);
	return 0;
}

//int func(int n)
//{
//	if (n >= 10)
//	{
//		return func(n / 10) + n % 10;
//	}
//	return n ;
//}
//int main()
//{
//	int n = 1342;
//	int ret =func(n);
//	printf("%d\n", ret);
//	return 0;
//}

//int func(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	return n * func(n, k - 1);
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int ret = func(n, k);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//    for (int i = 10000; i < 100000; i++)
//    {
//        int sum = 0;
//        for (int j = 10; j < 100000; j *= 10)
//        {
//            sum += (i / j) * (i % j);
//        }
//        if (sum == i)
//        {
//            printf("%d ", i);
//        }
//    }
//}

//int main()
//{
//	int a = 0;
//	int n = 0;
//	scanf("%d %d", &a, &n);
//	int sum = 0;
//	int tmp = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		tmp = tmp * 10 + a;
//		sum += tmp;
//		printf("%d  ", tmp);
//	}
//
//	return 0;
//}

//int func(int num)
//{
//	if (num == 1 || num == 2)
//	{
//		return 1;
//	}
//	return func(num - 1) + func(num - 2);
//}
//
//int main()
//{
//	int f1 = 1;
//	int f2 = 1;
//	for (int i = 1; i <= 20; i++)
//	{
//		printf("%d %d ", f1, f2);
//		f1 = f1 + f2;
//		f2 = f2 + f1;
//	}
//
//	/*for (int i = 1; i <= 40; i++)
//	{
//		int ret = func(i);
//		printf("%d ", ret);
//	}*/
//
//	/*int f1 = 1;
//	int f2 = 1;
//	int f3 = 0;
//	printf("%d %d ", f1, f2);
//	for (int i = 2; i < 40; i++)
//	{
//		f3 = f1 + f2;
//		printf("%d ", f3);
//		f1 = f2;
//		f2 = f3;
//	}*/
//
//	/*int arr[40] = { 0 };
//	arr[0] = 1;
//	arr[1] = 1;
//	for (int i = 2; i < 40; i++)
//	{
//		arr[i] = arr[i - 1] + arr[i - 2];
//	}
//	for (int i = 0; i < 40; i++)
//	{
//		printf("%d ", arr[i]);
//	}*/
//	return 0;
//}

//int main()
//{
//	for (int i = 1; i <= 5; i++)
//	{
//		for (int j = 1; j <= 5; j++)
//		{
//			printf("%d ", i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	double sum = 0;
//	double flag = 1;
//	int n = 5;
//	for (int i = 1; i <= n; i++)
//	{
//		int ret = i;
//		for (int j = 1; j < i; j++)
//		{
//			ret = ret * 10 + i;
//		}
//		printf("%d\n", ret);
//		sum += flag / ret;
//		flag = -flag;
//	}
//	printf("%lf\n", sum);
//	return 0;
//}

//int main()
//{
//	double sum = 0;
//	double flag = 1;
//	for (int i = 1; i <= 100; i++)
//	{
//		sum += flag / i;
//		flag = -flag;
//	}
//	printf("%lf\n", sum);
//	return 0;
//}

//int is_leap_year(int year)
//{
//	if (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//	//if (((year % 4 != 0 || year % 100 == 0) && (year % 400 != 0)))
//	{
//		return 1;
//	}
//	return 0;
//}
//
//int main()
//{
//	int count = 0;
//	for (int i = 2000; i < 2500; i++)
//	{
//		if (is_leap_year(i))
//		{
//			count++;
//			printf("%d ", i);
//			if (count % 15 == 0)
//			{
//				printf("\n");
//			}
//		}
//	}
//	return 0;
//}

//int func(int num)
//{
//	if (num == 0)
//	{
//		return 1;
//	}
//	return num * func(num - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int sum = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		sum *= i;
//	}
//	printf("%d\n", sum);
//	printf("%d\n", func(n));
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[10][10] = { 0 };
//	int m = n / 2;
//	if (n % 2 != 0)
//	{
//		m = m + 1;
//	}
//	for (int i = 0; i < m; i++)
//	{
//		for (int j = i; j < n - i; j++)
//		{
//			arr[i][j] = i + 1;
//			arr[n - 1 - i][j] = i + 1;
//			arr[j][i] = i + 1;
//			arr[j][n - 1 - i] = i + 1;
//		}
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}